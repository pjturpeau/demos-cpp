
# initialize debug flag
string( TOLOWER "${CMAKE_BUILD_TYPE}" CMK_DEBUG_TYPE_LOWER )
if (CMK_DEBUG_TYPE_LOWER STREQUAL "debug")
    set(DEBUG_ACTIVATED true)
endif ()

# to avoid problems with paths containing spaces under MS Windows
set(CMAKE_RC_COMPILER "windres")

# construct the name of the executable based on the compilation platform
math( EXPR BITS "8*${CMAKE_SIZEOF_VOID_P}" )
set( EXECUTABLE_NAME ${PROJECT_NAME}-x${BITS} )

if(CMAKE_COMPILER_IS_GNUCXX AND DEBUG_ACTIVATED)
    message("++ DrMemory options added!")
    # Options for DrMemory. Must be set before add_executable/add_library
    add_compile_options("$<$<CONFIG:DEBUG>:-O0;-g3;-ggdb;-m32;-fno-inline;-fno-omit-frame-pointer>")
endif()

if(WIN32)
    link_directories( $ENV{SDL2_HOME}/lib )
endif()

add_executable( ${EXECUTABLE_NAME} ${SOURCES} ${HEADERS} ${RESOURCES} )

# check for SDL2 environment variable
if(WIN32)
    if(NOT DEFINED ENV{SDL2_HOME})
        message(FATAL_ERROR "SDL2_HOME environment variable not defined (to allow SDL2_HOME/lib & SDL2_HOME/include)")
    endif()

    target_include_directories( ${EXECUTABLE_NAME}
        PUBLIC $ENV{SDL2_HOME}/include
    )
endif()

target_compile_features( ${EXECUTABLE_NAME} PRIVATE cxx_std_11 )

target_include_directories( ${EXECUTABLE_NAME}
    PUBLIC .
    PUBLIC .. 
    PUBLIC ../externals
)

if(CMAKE_COMPILER_IS_GNUCXX)
    target_compile_options( ${EXECUTABLE_NAME}
        PUBLIC -fexceptions
        PUBLIC -Wall
        PUBLIC -m64

        #optimize for size
        # PUBLIC -fno-rtti # remove RTTI
        # PUBLIC -fno-stack-protector
        # PUBLIC -fomit-frame-pointer
        # PUBLIC -ffunction-sections -fdata-sections -Wl,--gc-sections

        # if you like pain
#        PUBLIC -Wpedantic
#        PUBLIC -Wextra
    )
endif()

# optimize for size
#set_property(TARGET ${EXECUTABLE_NAME} APPEND_STRING PROPERTY LINK_FLAGS "-s")

if(WIN32)
    if(DEBUG_ACTIVATED)
        message("++ Output console enabled!")
    else()
        message("++ Output console disabled!")
        # set_property(TARGET ${EXECUTABLE_NAME} APPEND_STRING PROPERTY LINK_FLAGS "-mwindows -Wl,--gc-sections -Wl,-Bdynamic -Wl,--as-needed -Wl,--strip-all")

        # this disable cout/cerr in Release mode
        set_property(TARGET ${EXECUTABLE_NAME} APPEND_STRING PROPERTY LINK_FLAGS "-mwindows")
    endif()

    target_compile_definitions( ${EXECUTABLE_NAME}
        PUBLIC WIN32
    )

    target_link_libraries(
        ${EXECUTABLE_NAME}
        mingw32
        SDL2main
        SDL2
        ws2_32
    )
else()
    # non win32 platforms are not tested
    target_link_libraries(
        ${EXECUTABLE_NAME}
        SDL2main
        SDL2
    )
endif()
