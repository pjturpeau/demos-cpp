/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION
#include <x/x.hh>

#include <x/sg/sg.hh>
#include <x/sg/anim.hh>

#include <x/fx/PlasmaFx.hh>

#define W 320
#define H 240

struct PlasmaNode : x::sg::Node {

    int version = 0;
    x::PlasmaFx plasma;

    bool update_node(x::PixelSurface& surface, float delta_time_ms) {
        switch( version ) {
            case 0:
                plasma.update(delta_time_ms);
                plasma.render_sine_only(surface);
                break;
            case 1:
                plasma.update_fixed_point(delta_time_ms);
                plasma.render_fixed_point(surface);
                break;
            case 2:
                plasma.update(delta_time_ms);
                plasma.render_complexe(surface);
                break;
        }
        return true;
    }

    void cycle_version() {
        version++;
        version %= 3;
    }
};

void extra_usage(const char *basename) {
    SDL_Log("\n%s key actions:", basename);
    SDL_Log("\t[d]                to test various algorithms");
}

int main(int argc, char **argv)
{
    x::sg::SceneGraph app;
    x::sg::SequentialAnimation seq1, seq2;
    x::sg::PauseAnimation pause;
    x::sg::PaletteAnimation anim1, anim2, anim3, anim4;
    x::ColorPalette p1, p2, p3, p4;
    PlasmaNode plasma;

    // build the scene graph tree
    
    app.resolution(W, H);
    app.fullscreen(false);
    app.zoom(2);
    app.read_command_line(argc, argv, extra_usage);
    app.title(".:[ bbl / plasma ]:.");
    app.set_root(plasma);
    app.on_key_down(
        [&plasma] (SDL_Keysym& key) {
            if( key.sym == SDLK_d ) {
                plasma.cycle_version();
            }
        });
  
    plasma.add_child(seq1);
    seq1.add_child(pause);
    seq1.add_child(seq2);
    seq1.start();
    
    seq2.add_child(anim1);
    seq2.add_child(anim2);
    seq2.add_child(anim3);
    seq2.add_child(anim4);
    seq2.loop(true);

    // configure tree elements

    p1.linear_gradient(0, 0xf64f59, 30, 0xc471ed);
    p1.linear_gradient(30, 0xc471ed, 130, 0x12c2e9);
    p1.linear_gradient(130, 0x12c2e9, 255, 0xf64f59);
    p1.activate();

    p2.linear_gradient(0, 0xFEAC5E, 30, 0xC779D0);
    p2.linear_gradient(30, 0xC779D0, 130, 0x4BC0C8);
    p2.linear_gradient(130, 0x4BC0C8, 255, 0xFEAC5E);

    p3.linear_gradient(0, 0x833ab4, 80, 0xfd1d1d);
    p3.linear_gradient(80, 0xfd1d1d, 190, 0x4BC0C8);
    p3.linear_gradient(190, 0x4BC0C8, 255, 0x833ab4);

    p4.linear_gradient(0, 0x40e0d0, 85, 0xff8c00);
    p4.linear_gradient(85, 0xff8c00, 170, 0xff0080);
    p4.linear_gradient(178, 0xff0080, 255, 0x40e0d0);

    anim1.destination_palette(x::Video().palette);
    anim1.from_palette(p1);
    anim1.to_palette(p2);
    anim1.duration(2000);
    
    anim2.destination_palette(x::Video().palette);
    anim2.from_palette(p2);
    anim2.to_palette(p3);
    anim2.duration(2000);

    anim3.destination_palette(x::Video().palette);
    anim3.from_palette(p3);
    anim3.to_palette(p4);
    anim3.duration(2000);

    anim4.destination_palette(x::Video().palette);
    anim4.from_palette(p4);
    anim4.to_palette(p1);
    anim4.duration(2000);

    pause.duration(3000);

    float fps = app.execute();

    SDL_Log("fps = %f", fps);
    return 0;
}
