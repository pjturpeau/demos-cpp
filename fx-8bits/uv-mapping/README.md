# 2D Tunnel

Based on precalc tables
([excellent explanation](http://lodev.org/cgtutor/tunnel.html) / [mirror](Tunnel_Effect.zip))

![tunnel](images/tunnel-2d-screenshot.png)

## Generated maps

Distance map  
![distance map](images/distance_map.png)

Angle map  
![angle map](images/angle_map.png)
