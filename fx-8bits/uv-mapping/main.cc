/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION
#include <x/x.hh>
#include <x/gfx/BitmapManager.hh>
#include <x/fx/UVMapFx.hh>
#include <x/sg/sg.hh>
#include <x/sg/gfx.hh>

#define W 320
#define H 240

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN(texture, "images/tunnel-2d-texture.bmp");

int main(int argc, char **argv)
{
    x::sg::SceneGraph app;
    x::sg::CyclicAction uvmap_node;
    x::UVMapFx<W,H> uvmap_fx;
    x::ColorPalette uvmap_palette;

    SDL_Log("key actions:");
    SDL_Log("\t[D]\tto display the UV maps");
    SDL_Log("\t[SPACE]\tto generate new maps");

    app.resolution(W, H);
    app.fullscreen(false);
    app.zoom(2);
    app.read_command_line(argc, argv);
    app.title(".:[ bbl / UV 2D Mapping ]:.");
    app.set_root(uvmap_node);

    int map_display = 0;

    // cycle visible nodes and set the palette accordingly 
    app.on_key_down(
        [&uvmap_palette, &uvmap_fx, &map_display] (SDL_Keysym& key) {
            if( key.sym == SDLK_d ) { // [D] key to display maps
                map_display++;
                map_display %= 4;
                switch(map_display) {
                    case 0: uvmap_palette.activate(); break;
                    case 1: x::ColorPalette::black_to_white().activate(); break;
                    case 3: x::Video().palette.linear_gradient(0, 0, uvmap_fx.shadow_levels(), 0xFFFFFF); break;
                }
            } else if( key.sym == SDLK_SPACE ) { // [SPACE] to generate a new map
                static int map_version = 0;
                map_version++;
                map_version %= 5;
                switch(map_version) {
                case 0:
                    uvmap_fx.generate_tunnel();
                    break;
                case 1:
                    uvmap_fx.generate_waves();
                    break;
                case 2:
                    uvmap_fx.generate_warping();
                    break;
                case 3:
                    uvmap_fx.generate_warping_near();
                    break;
                case 4:
                    uvmap_fx.generate_warping_far();
                    break;
                }
            }
        });

    auto texture = x::Bitmaps().load_bmp("texture", texture_data, texture_size);
    
constexpr int USEFUL_COLORS = 16; // the number of useful colors in the texture (for shadows)
    uvmap_palette = texture.palette;
    uvmap_palette.darken_to_black(USEFUL_COLORS); // number of useful colors in the texture
    uvmap_palette.activate();

    uvmap_fx.texture(texture.surface);
    uvmap_fx.shadow_colors(USEFUL_COLORS);
    uvmap_fx.generate_tunnel();

    // it's one way to do it...
    uvmap_node.action([&uvmap_fx, &map_display](x::PixelSurface& s, float dt) {
        uvmap_fx.update_tunnel(dt); // hopefully, it works for all my maps...

        switch(map_display) {
            case 0: uvmap_fx.draw(s); break;
            case 1: uvmap_fx.render_u_map(s); break;
            case 2: uvmap_fx.render_v_map(s); break;
            case 3: uvmap_fx.render_w_map(s); break;
        }
    });

    float fps = app.execute();

    SDL_Log("fps = %f", fps);
    return 0;
}
