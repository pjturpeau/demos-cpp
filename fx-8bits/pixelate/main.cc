/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 02/2018
 * <pierrejean AT turpeau DOT net>
 *
 */
#define X_IMPLEMENTATION
#include <x/x.hh>
#include <x/sg/sg.hh>
#include <x/sg/anim.hh>

#include <x/gfx/ChunkyMode.hh>
#include <x/gfx/BitmapManager.hh>
#include <x/fx/SurfaceFx.hh>

#include <x/sg/anim/NumberAnimation.hh>

// size of the texture to be mapped
#define TEXTURE_SIZE 256

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN(texture1, "images/minitel.bmp");

#define W 320
#define H 240

SDL_Surface *tex1;

class PixelatedLoop : public x::ChunkyModeLoop
{
public:
    PixelatedLoop() : reference_palette(0) {
        animation.from(0.0f);
        animation.to(1.0f);
        animation.duration(2000);
     
        animation.start();
    }

    void source(const x::Bitmap& bmp) {
        source_image = &bmp;
        fx.from(source_image->surface);
    }

    void loop_callback(x::PixelSurface& surface, float delta_time_ms) {
        animation.update_node(surface, delta_time_ms);

        float factor = 20.0f - 19.0f * animation.value;

         x::Video().palette.interpolate(reference_palette, source_image->palette, animation.value);
        
        fx.pixelate(surface, 0, 0, factor);
    }

    const x::Bitmap *source_image = nullptr;
    x::SurfaceFx fx;
    x::ColorPalette reference_palette;
    x::sg::FloatAnimation animation;
};

int main(int argc, char **argv)
{
    // initialize video
    x::ChunkyModeAttributes attributes; 

    attributes.resolution(W, H);
    attributes.title(".:[ bbl / pixelize ]:.");
    attributes.read_command_line(argc, argv);

    x::Video().initialize(attributes);

    // configure fx
    const x::Bitmap img = x::Bitmaps().load_bmp("bm0", texture1_data, texture1_size);
    x::Bitmaps("bm0").palette.activate();
    x::Video().palette.clear(0);

    PixelatedLoop main_loop;
    main_loop.source(x::Bitmaps("bm0"));

    // execute
    float fps = x::Video().execute(main_loop);

    SDL_Log("FPS=%f", fps);
    return 0;
}
