/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 03/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION

#include <x/x.hh>
#include <x/sg/sg.hh>
#include <x/sg/anim.hh>
#include <x/fx/MoireFx.hh>

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN(light, "../bump/images/light.bmp");

#define W 320
#define H 240

int main(int argc, char **argv)
{
    x::sg::SceneGraph app;
    x::sg::CyclicAction moire_node;
    x::MoireFx moire;
    bool flat = true;

    SDL_Log("key actions:");
    SDL_Log("\t[space]\tdisplay gradient version");

    app.resolution(W, H);
    app.fullscreen(false);
    app.zoom(2);
    app.read_command_line(argc, argv);
    app.title(".:[ bbl / moiré ]:.");
    app.set_root(moire_node);

    app.on_key_down(
        [&flat, &moire] (SDL_Keysym& key) {
            if( key.sym == SDLK_SPACE) { // press [spc] key
                flat = !flat;
                x::Video().palette = moire.palette(flat);
            }
        });

    moire_node.action([&moire, &flat](x::PixelSurface& s, float dt) {
        moire.update(s, dt);
        moire.draw(s, flat);
    });
 
    x::Video().palette = moire.palette();

    float fps = app.execute();

    SDL_Log("fps = %f", fps);
    return 0;
}
