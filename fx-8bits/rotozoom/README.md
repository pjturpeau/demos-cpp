# Rotozoom

The following pictures give an insight of what we want to achieve when the
framebuffer is centered at the origin of the texture.

<img src="images/rotozoom_0.png" alt="Rotozoom" style="width: 340px;"/>
<img src="images/rotozoom_1.png" alt="Rotozoom" style="width: 340px;"/>

## Framebuffer rectangle in the texture coordinate system

As the projection is orthogonal (rectangle), we can consider only three (3)
point of the framebuffer rectangle (here, the top left, top right and bottom
left points).

```
A = ( -framebuffer.width / 2 ; -framebuffer.height / 2 )
B = (  framebuffer.width / 2 ; -framebuffer.height / 2 )
C = ( -framebuffer.width / 2 ;  framebuffer.height / 2 )

P =  (P.x , P.y) = initial position in the texture coordinate system
```

The 2D rotation matrix around the Z axis is:

```
            cos(angle)   -sin(angle)
Rz(angle) = sin(angle)    cos(angle)
```

Once zoomed, translated and rotated we have

```
Ap = (scale * (A + P)) * Rz(angle)
Bp = (scale * (B + P)) * Rz(angle)
Cp = (scale * (C + P)) * Rz(angle)
```

Which gives

```
Ap.x =  (scale * (A.x + P.x)) * cos(angle) + (P.y + scale * (A.y + P.y)) * sin(angle)
Ap.y = -(scale * (A.x + P.x)) * sin(angle) + (P.y + scale * (A.y + P.y)) * cos(angle)

Bp.x =  (scale * (B.x + P.x)) * cos(angle) + (P.y + scale * (B.y + P.y)) * sin(angle)
Bp.y = -(scale * (B.x + P.x)) * sin(angle) + (P.y + scale * (B.y + P.y)) * cos(angle)

Cp.x =  (scale * (C.x + P.x)) * cos(angle) + (P.y + scale * (C.y + P.y)) * sin(angle)
Cp.y = -(scale * (C.x + P.x)) * sin(angle) + (P.y + scale * (C.y + P.y)) * cos(angle)
```

## dx & dy computation for the slope along the X axis of the framebuffer

The framebuffer horizontal scanline is going from point _Ap_ to point _Bp_ in
the texture coordinate system.

Thus, to compute the _dx & dy_ for the slope along the X axis of the
framebuffer we need:

```
dX = Bp - Ap = (Bp.x - Ap.x ; Bp.y - Ap.y)

dX.x =   (scale * (B.x + P.x)) * cos(angle) + (P.y + scale * (B.y + P.y)) * sin(angle)
       - (scale * (A.x + P.x)) * cos(angle) - (P.y + scale * (A.y + P.y)) * sin(angle)

dX.y = - (scale * (B.x + P.x)) * sin(angle) + (P.y + scale * (B.y + P.y)) * cos(angle)
       + (scale * (A.x + P.x)) * sin(angle) - (P.y + scale * (A.y + P.y)) * cos(angle)
```

As we have an orthogonal projection _A.y = B.y_, we can simplify:

```
dX.x =  (scale * (B.x + P.x)) * cos(angle) - (scale * (A.x + P.x)) * cos(angle)
dX.y = -(scale * (B.x + P.x)) * sin(angle) + (scale * (A.x + P.x)) * sin(angle)
```

Since _A.x = -B.x_, we can still simplify (and eliminate P.x):

```
dX.x =  2 * scale * B.x * cos(angle)
dX.y = -2 * scale * B.x * sin(angle)
```

As _B.x = framebuffer.width / 2_

```
dX.x =  scale * framebuffer.width * cos(angle)
dX.y = -scale * framebuffer.width * sin(angle)
```

In the end, we can now compute _dx_ and _dy_ according to the horizontal
scanline size:

```
dx_dX = dX.x / framebuffer.width;
dy_dX = dX.y / framebuffer.width;
```

## dx & dy computation for the slope along the Y axis of the framebuffer

After each drawn scanline, we need to go from point _Ap_ to point _Cp_ in the
texture coordinate system.

Thus, to compute the _dx & dy_ for the slope along the Y axis of the
framebuffer we need:

```
dY = Cp - Ap = (Cp.x - Ap.x ; Cp.y - Ap.y)

dY.x =   (scale * (C.x + P.x)) * cos(angle) + (P.y + scale * (C.y + P.y)) * sin(angle)
       - (scale * (A.x + P.x)) * cos(angle) - (P.y + scale * (A.y + P.y)) * sin(angle)

dY.y = - (scale * (C.x + P.x)) * sin(angle) + (P.y + scale * (C.y + P.y)) * cos(angle)
       + (scale * (A.x + P.x)) * sin(angle) - (P.y + scale * (A.y + P.y)) * cos(angle)
```

As we have an orthogonal projection _A.x = C.x_, we can simplify:

```
dY.x = -(P.y + scale * (C.y + P.y)) * sin(angle) - (P.y + scale * (A.y + P.y)) * sin(angle)

dY.y =  (P.y + scale * (C.y + P.y)) * cos(angle) - (P.y + scale * (A.y + P.y)) * cos(angle)
```

Since _A.y = -C.x_, we can still simplify (and eliminate P.y):

```
dY.x = 2 * scale * C.y * sin(angle)
dY.y = 2 * scale * C.y * cos(angle)
```

As _C.y = framebuffer.height / 2_

```
dY.x = scale * framebuffer.height * sin(angle)
dY.y = scale * framebuffer.height * cos(angle)
```

In the end, we can now compute _dx_ and _dy_ according to the number of lines
in the framebuffer:

```
dx_dY = dY.x / framebuffer.height;
dy_dY = dY.y / framebuffer.height;
```
