/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION
#include <x/x.hh>
#include <x/gfx/BitmapManager.hh>

#include <x/sg/sg.hh>
#include <x/sg/anim.hh>

#include <x/fx/RotozoomFx.hh>

#define W 320
#define H 240

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN(texture1, "images/bomberman1.bmp");
INCBIN(texture0, "images/bomberman0.bmp");
INCBIN(texture2, "images/bomberman2.bmp");

int main(int argc, char **argv)
{
    x::sg::SceneGraph app;
    x::sg::SequentialAnimation seq;
    x::sg::LambdaAction action;
    x::sg::PauseAnimation pause;
    x::sg::CyclicAction rotozoom_node;
    x::RotozoomFx rotozoom;
  
    const x::Bitmap tex0 = x::Bitmaps()
        .load_bmp("bm0", texture0_data, texture0_size);

    const x::Bitmap tex1 = x::Bitmaps()
        .load_bmp("bm1", texture1_data, texture1_size);

    const x::Bitmap tex2 = x::Bitmaps()
        .load_bmp("bm2", texture2_data, texture2_size);

    tex0.palette.activate();

    // change the texture every 500ms
    pause.duration(500);
    seq.add_child(pause);
    seq.add_child(action);
    seq.loop(true);
    seq.start();

    action.lambda([&rotozoom, tex0, tex1, tex2]() {
        static int tex_num = 0;
        tex_num = (tex_num + 1) & 3;
        switch(tex_num) {
            case 0: rotozoom.set_texture(tex0.surface); break;
            case 1:
            case 3: rotozoom.set_texture(tex1.surface); break;
            case 2: rotozoom.set_texture(tex2.surface); break;
        }
    });

    rotozoom.set_texture(tex0.surface);
    
    // update the rotozoom at each execution cycle
    rotozoom_node.action([&rotozoom](x::PixelSurface& surface, float delta_time_ms) {
        rotozoom.default_update(delta_time_ms);
        rotozoom.draw(surface);
    });
    rotozoom_node.add_child(seq);
//    rotozoom.add_child(cyclic);

    app.resolution(W, H);
    app.fullscreen(false);
    app.zoom(2);
    app.read_command_line(argc, argv);
    app.title(".:[ bbl / rotozoom ]:.");
    app.set_root(rotozoom_node);

    float fps = app.execute();

    SDL_Log("fps = %f", fps);
    return 0;
}
