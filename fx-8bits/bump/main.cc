/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION

#include <x/x.hh>
#include <x/gfx/BitmapManager.hh>

#include <x/sg/sg.hh>
#include <x/sg/gfx.hh>
#include <x/sg/anim.hh>

#include <x/fx/BumpFx.hh>

constexpr int W = 320;
constexpr int H = 240;

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN(map,   "images/stone.bmp");
INCBIN(light, "images/light.bmp");
INCBIN(logo,  "images/bbl.bmp");

int main(int argc, char **argv)
{
    x::sg::SceneGraph app;
    x::sg::IntSineAnimation x_anim;
    x::sg::IntSineAnimation y_anim;

    x::sg::SequentialAnimation seq;
    x::sg::IntegerAnimation bounce_down;
    x::sg::IntegerAnimation bounce_up;
    x::sg::OpaqueSurface bouncing_logo;
    x::sg::CyclicAction bump_node;

    x::BumpFx bump;

    app.resolution(W, H);
    app.fullscreen(false);
    app.zoom(2);
    app.read_command_line(argc, argv);
    app.title(".:[ bbl / bump ]:.");
    app.set_root(bump_node);

    // TODO: generate specular light source texture
    // TODO: play with colormap for specular size
    // x::Procedurals.specular_map(size, from_index, to_index, hsv_color, spot_size);

    x::Bitmaps().load_bmp("logo", logo_data, logo_size);
    bouncing_logo.image(x::Bitmaps("logo"));
    bouncing_logo.position(15, 160);
    bouncing_logo.add_child(seq);

    bounce_down.from(185);
    bounce_down.to(205);
    bounce_down.duration(250);
    bounce_down.bind_to(bouncing_logo.y);
    bounce_down.easing(x::ease_in_quad);

    bounce_up.from(205);
    bounce_up.to(185);
    bounce_up.duration(250);
    bounce_up.bind_to(bouncing_logo.y);
    bounce_up.easing(x::ease_out_quad);

    seq.add_child(bounce_down);
    seq.add_child(bounce_up);
    seq.loop(true);
    seq.start();

    x::Bitmaps().load_bmp("map", map_data, map_size);
    x::Bitmaps().load_bmp("light", light_data, light_size);

    bump.background(x::Bitmaps("map").surface);
    bump.light(x::Bitmaps("light").surface);
    
    bump_node.add_child(x_anim);
    bump_node.add_child(y_anim);
    bump_node.add_child(bouncing_logo);

    x_anim.time_speed(1.0f);
    x_anim.bind_to(bump.light_x);
    x_anim.start();

    y_anim.time_speed(0.5f);
    y_anim.bind_to(bump.light_y);
    y_anim.start();

    bump_node.action([&bump](x::PixelSurface& s, float dt) {
        bump.draw(s);
    });

    // just play with possibilities as you like...

    x_anim.trigo.configure(W/2, W/2, 0.75f, 1.f, -0.25f, 0.250f, 2.f, 0.00f);
    //y_anim.trigo.configure(H/2, H/2, -0.5f, 1.f,  0.75f, 0.375f, 1.f, 0.25f);

    //x_anim.trigo.configure(W/2, W/2, 2);
    y_anim.trigo.configure(H/2, H/2, 0.5f);

    // x_anim.trigo.configure(W/2, W/2);
    // y_anim.trigo.configure(H/2, H/2);

    x::Bitmaps("light").palette.activate();

    float fps = app.execute();

    SDL_Log("fps = %f", fps);
    return 0;
}
