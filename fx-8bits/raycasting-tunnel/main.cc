/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 11/2017
 * <pierrejean AT turpeau DOT net>
 */
#include <iostream>
#include <windows.h>
#include <wingdi.h>
#include <assert.h>

using namespace std;

#define X_IMPLEMENTATION
#include <x/x.hh>
//#include <x/gfx/ChunkyMode.hh>

#include "config.h"

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN(texture, "images/texture256x256.bmp");

extern void tunnel_interpolation(x::PixelSurface& surface, float delta_time);
extern void tunnel_fast_maths(x::PixelSurface& surface, float delta_time);
extern void tunnel_cstd_maths(x::PixelSurface& surface, float delta_time);

class MainLoop : public x::ChunkyModeLoop
{
public:

    void loop_callback(x::PixelSurface& surface, float delta_time_ms) {
        switch(version) {
            case 0:
                tunnel_interpolation(surface, delta_time_ms);
                break;
            case 1:
                tunnel_fast_maths(surface, delta_time_ms);
                break;
            case 2:
                tunnel_cstd_maths(surface, delta_time_ms);
                break;
        }
    }

    void key_down(SDL_Keysym& key) {
        if( key.sym == SDLK_d ) { // press [d] key
            version ++;
            version %= 3;
        }
    }

    int version = 0;
};

int main(int argc, char **argv)
{
    x::ChunkyModeAttributes attributes;
    
    attributes.resolution(WIDTH, HEIGHT);
    attributes.title(".:[ bbl / raycasting tunnel ]:.");
    attributes.read_command_line(argc, argv, 
        [](cstrptr_t basename) {
            SDL_Log("\n%s usage:\n", basename);
            SDL_Log("\t-1  version using C++ std maths\n");
            SDL_Log("\t-2  version using fasts maths\n");
            SDL_Log("\tthe default version is using interpolations\n");
        });

    x::Video().initialize(attributes);

    MainLoop main_loop;

    while(--argc)
    {
        std::string str(argc[argv]);
        if( ! str.compare("-1") )
        {
            SDL_Log("C++ std math version");
            main_loop.version = 2;
        }
        else if( ! str.compare("-2") )
        {
            SDL_Log("Fasts math version");
            main_loop.version = 1;
        }
    }

    x::Bitmaps().load_bmp("texture", texture_data, texture_size);
    x::Bitmaps("texture").palette.activate();

    float fps = x::Video().execute(main_loop);

    SDL_Log("fps = %f", fps);
    return 0;
}
