/*
 * fast.cc
 * Copyleft (c) Pierre-Jean Turpeau 03/2018
 * <pierrejean AT turpeau DOT net>
 *
 * The one using interpolation over a grid
 */
#include <math.h>

#include <x/gfx/BitmapManager.hh>
#include <x/math/fastmaths.hh>
#include <x/math/Matrix.hh>
#include <x/math/Trigo.hh>

#include "config.h"

using namespace std;

#define GRID_SIZE   8
#define FP_SIZE     16 // fixed point precision

typedef struct
{
    long u;
    long v;
}
uv32;

static x::Mat3f rotation_matrix;
static x::Vec3f origin;

static x::FloatTrigo trigo;

static float Radius = 256.0f;

static uv32 _grid[HEIGHT/GRID_SIZE+1][WIDTH/GRID_SIZE+1];

static void _GetUV(int x,int y, int *u, int *v)
{
    x::Vec3f initial_direction(x - WIDTH/2, y - HEIGHT/2, 256.f);
    x::Vec3f direction;

    direction = rotation_matrix * initial_direction;
    vec_normalize(direction);

    // It's a conic problem. We have to calculate the intersection between
    // a cylinder centered on (0,0) along the Z axis with the following
    // equation:   x^2 + y^2 = r^2 (equation of a circle for all z)

    // We need to find the intersection between this cylinder and a ray that
    // has an origin and a direction vectors: ray = origin + t * direction

    // Here we know the origin, we know the direction, and we know the radius
    // of our cylinder, thus we need to find t such as if we substitute (x,y)
    // by the ray coordinates in the cylinder equation, the result is equal
    // to radius^2

    // The full expanded equation is:
    //
    //     t^2*(direction.x^2 + direction.y^2)
    //     + t*2*(origin.x*direction.x + origin.y*direction.y)
    //     + origin.x^2 + origin.y^2 - r^2 = 0;

    // it's a quadric that we can resolve using standard maths.

    float a = direction.x * direction.x + direction.y * direction.y;
    float b = 2*(origin.x * direction.x + origin.y * direction.y);
    float c = origin.x * origin.x + origin.y * origin.y - Radius * Radius;

    // calculate discriminent delta
    float delta = b*b - 4*a*c;

    float t;

    // if there's no real solution
    if (delta < 0)
    {
        *u = 128;
        *v = 128;
        return;
    }
    else if ( delta == 0.0f )
    {
        t  = -b/(2*a);
    }
    else
    {
        // there are 2 solutions, get the nearest
        delta = x::sqrt(delta);
        float t1 = -b + delta;
        float t2 = -b - delta;
        t = t1 <= t2 ? t1 : t2;
        t /= 2*a;
    }

    // finally compute the intersection
    //      intersection = direction * t + origin
    direction *= t;
    direction += origin;

    // do the mapping within the texture
    *u = (int)(fabs(direction.z)*0.10f);
    *v = (int)(fabs(x::atan2(direction.y, direction.x) * 256 / M_PI));
}

// Could certainly be faster (btw, it is HW dependent) if all floating point
// numbers are changed for fixed point ones while using the SinInt utility
// class.
void tunnel_interpolation(x::PixelSurface& surface, float delta_time)
{
    static float posangle = 0;
    static float rotangle;
    static float zrotangle;

    const uint8_t* pixs = x::Bitmaps("texture").surface.pixels;

    int u = 0;
    int v = 0;

    posangle += delta_time * 0.0005f;

    origin.x = 30 * trigo.cos(posangle-0.25f) +  90 * trigo.sin(posangle*2.f);
	origin.y = 60 * trigo.sin(posangle+0.25f) - 110 * trigo.cos(posangle+0.75f);
    origin.z -= delta_time * 0.5f; // run along the tunnel

    rotangle = trigo.sin(posangle*2)*0.5f;
    zrotangle = trigo.cos(posangle*0.5f);

    mat_rotation(rotation_matrix, rotangle*0.5f, rotangle, -zrotangle);

    for(int j = 0; j <= HEIGHT/GRID_SIZE; j++)
    {
        for(int i = 0; i <= WIDTH/GRID_SIZE; i++)
        {
            _GetUV(i*GRID_SIZE, j*GRID_SIZE, &u, &v);
            _grid[j][i].u = u;
            _grid[j][i].v = v;
            //video_mode.set_pixel(i, j, pixs[(u + 256 * v) & 0xFFFF]);
        }
    }

    // now computing left and right du/dv to interpolate within the texture
    // along the y axis on the screen, so that we're able to compute dx/dx and
    // interpolate onto each horizontal scanline to draw the pixels.

    // very naive and simple version, there's room for optimization everywhere

    long left_u, left_v, left_du, left_dv;
    long right_u, right_v, right_du, right_dv;

    long scanline_u, scanline_v, scanline_du, scanline_dv;

    for (int j = 0; j < HEIGHT/GRID_SIZE; j++)
    {
        for (int i = 0; i < WIDTH/GRID_SIZE; i++)
        {
            left_du = ((_grid[j+1][i].u - _grid[j][i].u) << FP_SIZE) / GRID_SIZE;
            left_dv = ((_grid[j+1][i].v - _grid[j][i].v) << FP_SIZE) / GRID_SIZE;

            right_du = ((_grid[j+1][i+1].u - _grid[j][i+1].u) << FP_SIZE) / GRID_SIZE;
            right_dv = ((_grid[j+1][i+1].v - _grid[j][i+1].v) << FP_SIZE) / GRID_SIZE;

            left_u = _grid[j][i].u << FP_SIZE;
            left_v = _grid[j][i].v << FP_SIZE;

            right_u = _grid[j][i+1].u << FP_SIZE;
            right_v = _grid[j][i+1].v << FP_SIZE;

            for (int n = 0; n < GRID_SIZE; n++)
            {
                scanline_u = left_u;
                scanline_v = left_v;

                scanline_du = (right_u - left_u) / GRID_SIZE;
                scanline_dv = (right_v - left_v) / GRID_SIZE;

                for (int m = 0; m < GRID_SIZE; m++ )
                {
                    surface.set(i*GRID_SIZE + m, j*GRID_SIZE + n, pixs[((scanline_u >> FP_SIZE) + ((scanline_v >> FP_SIZE) <<8))  & 0xFFFF]);

                    scanline_u += scanline_du;
                    scanline_v += scanline_dv;
                }

                left_u += left_du;
                left_v += left_dv;

                right_u += right_du;
                right_v += right_dv;
            }
        }
    }
}
