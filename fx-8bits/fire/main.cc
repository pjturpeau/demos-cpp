/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION

#include <x/x.hh>
#include <x/sg/sg.hh>
#include <x/fx/FireFx.hh>

#define W 320
#define H 240

struct FireNode : x::sg::Node {
    
    bool update_node(x::PixelSurface& surface, float dt) {
        fx.draw(surface);
        return true;
    }

    x::FireFx<255> fx;
};

int main(int argc, char **argv)
{
    x::sg::SceneGraph app;
    FireNode fire;
    
    app.resolution(W, H);
    app.fullscreen(false);
    app.zoom(2);
    app.read_command_line(argc, argv);
    app.title(".:[ bbl / fire ]:.");
    app.set_root(fire);

    fire.fx.yellow_to_blue_fire().activate();

    float fps = app.execute();

    SDL_Log("fps = %f", fps);
    return 0;
}
