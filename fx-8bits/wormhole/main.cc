/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION
#include <x/x.hh>
#include <x/gfx/BitmapManager.hh>
#include <x/sg/sg.hh>
#include <x/sg/anim.hh>
#include <x/fx/WormholeFx.hh>

#include <memory>

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN(blue, "images/blue.bmp");
INCBIN(green, "images/green.bmp");
INCBIN(pink, "images/pink.bmp");
INCBIN(map1, "images/map-320x240-4800x10-40000x128-15-v.bmp");
INCBIN(map2, "images/map-320x240-12000x30-24000x100-25-h.bmp");

#define W 320
#define H 240

class MainLoop : public x::ChunkyModeLoop
{
public:
    explicit MainLoop(x::ChunkyMode& video_mode) :
        map1(bitmaps.load_bmp("map1", map1_data, map1_size)),
        map2(bitmaps.load_bmp("map2", map2_data, map2_size)),
        tex1(bitmaps.load_bmp("pink", pink_data, pink_size)),
        tex2(bitmaps.load_bmp("green", green_data, green_size)),
        tex3(bitmaps.load_bmp("blue", blue_data, blue_size)),
        wormhole(map1, tex1),
        black_palette(0),
        tex_palette(wormhole.texture->palette),
        action_change_map([this]() {
                map_num = (map_num + 1) % 2;
                tex_num = (tex_num + 1) % 3;

                if( map_num )
                    wormhole.map = &map2;
                else
                    wormhole.map = &map1;

                switch( tex_num ) {
                    case 0: wormhole.texture = &tex1; break;
                    case 1: wormhole.texture = &tex2; break;
                    case 2: wormhole.texture = &tex3; break;
                }

                tex_palette = wormhole.texture->palette;
            })
    {
        fade_to_black_animation.destination_palette(x::Video().palette);
        fade_to_black_animation.from_palette(tex_palette);
        fade_to_black_animation.to_palette(black_palette);
        fade_to_black_animation.duration(250);
  
        fade_to_texture_animation.destination_palette(x::Video().palette);
        fade_to_texture_animation.from_palette(black_palette);
        fade_to_texture_animation.to_palette(tex_palette);
        fade_to_texture_animation.duration(250);

        wormhole.texture->palette.activate();

        sequential_animation.add_child(fade_to_black_animation);
        sequential_animation.add_child(action_change_map);
        sequential_animation.add_child(fade_to_texture_animation);
    }

    void loop_callback(x::PixelSurface& surface, float delta_time_ms) {
        wormhole.update(delta_time_ms);

        static float elapsed_time = 0;

        if( display_map )
            wormhole.draw_map(surface);
        else {
            wormhole.draw(surface);
            elapsed_time += delta_time_ms;
        }

        if( elapsed_time> 5000 /*ms*/ ) {
            elapsed_time = 0;
            sequential_animation.start();
        }

        sequential_animation.execute(surface, delta_time_ms);
    }

    void key_down(SDL_Keysym& keysym) 
    {
        switch (keysym.sym)
        {
        case SDLK_d:
            display_map = !display_map;
            if( display_map )
                x::ColorPalette::black_to_white().activate();
            else
                wormhole.texture->palette.activate();
            break;
        }
    }

    bool display_map = false;
    int map_num = 0;
    int tex_num = 0;

    x::ColorPalette pal;
    x::BitmapManager bitmaps;
    const x::Bitmap& map1;
    const x::Bitmap& map2;
    const x::Bitmap& tex1;
    const x::Bitmap& tex2;
    const x::Bitmap& tex3;

    x::WormholeFx<W,H> wormhole;
    
    x::ColorPalette black_palette;
    x::ColorPalette tex_palette;

    x::sg::PaletteAnimation fade_to_black_animation;
    x::sg::PaletteAnimation fade_to_texture_animation;
    x::sg::LambdaAction action_change_map;
    x::sg::SequentialAnimation sequential_animation;
};

int main(int argc, char **argv)
{
    x::ChunkyModeAttributes attributes;

    attributes.width  = W;
    attributes.height = H;
    attributes.title(".:[ bbl / wormhole ]:.");
    attributes.read_command_line(argc, argv,
        [](cstrptr_t basename) {
            SDL_Log("%s usage:", basename);
            SDL_Log("\t-genmaps  generate wormhole maps\n\n");
            SDL_Log("%s key actions:", basename);
            SDL_Log("\t[d]       display current map\n\n");
        }    
    );

    // Check for maps generation
    while(--argc)
    {
        std::string str(argc[argv]);
        if( ! str.compare("-genmaps") )
        {
            SDL_Log("Generating maps");

            // x::fx::Wormhole<W, H>::generate_map(
            //     W/2,
            //     H/2,
            //     1200*10, 24000, 25, 3*10, 100, false);

            x::WormholeFx<W, H>::generate_and_save_map(
                W/2,
                H/2,
                4800, 40000, 15, 10, 128, true);

            SDL_Quit();
            exit(1);
        }
    }

    x::Video().initialize(attributes);

    MainLoop main_loop(x::Video());

    float fps = x::Video().execute(main_loop);

    SDL_Log("FPS=%f", fps);
    return 0;
}
