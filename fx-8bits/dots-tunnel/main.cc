/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION
#include <x/x.hh>
#include <x/sg/sg.hh>
#include <x/fx/DotTunnelFx.hh>

#define W 320
#define H 240

int main(int argc, char **argv)
{
    x::sg::SceneGraph app;
    x::sg::CyclicAction tunnel_node;
    x::DotTunnelFx<W,H> tunnel;

    app.resolution(W, H);
    app.fullscreen(false);
    app.zoom(2);
    app.read_command_line(argc, argv);
    app.title(".:[ bbl / dot tunnel ]:.");
    app.set_root(tunnel_node);

    tunnel_node.action([&tunnel](x::PixelSurface& s, float dt) {
        tunnel.update(dt);
        s.clear();
        tunnel.draw(s);
    });

    tunnel.yellow_and_blue_palette().activate();

    float fps = app.execute();

    SDL_Log("fps = %f", fps);
    return 0;
}
