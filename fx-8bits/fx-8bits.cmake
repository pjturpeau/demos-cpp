list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../..")

include(common)

target_include_directories( ${EXECUTABLE_NAME}
    PUBLIC ../..
    PUBLIC ../../externals
    )
