/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION

#include <x/sg/sg.hh>
#include <x/sg/gfx.hh>

#include <x/gfx/BitmapManager.hh>
#include <x/fx/WhiteNoiseFx.hh>

#define W 320
#define H 240

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN(topleft, "images/top-left.bmp");

struct Noise : x::sg::Node {
    bool update_node(x::PixelSurface& s, float dt) {
        fx.draw(s);
        return true;
    }
    x::WhiteNoiseFx fx;
};

int main(int argc, char **argv)
{
    Noise noise;
    x::sg::SceneGraph app;
    x::sg::TransparentSurface top_left;
    x::sg::TransparentSurface top_right;
    x::sg::TransparentSurface bottom_left;
    x::sg::TransparentSurface bottom_right;

    app.resolution(W, H);
    app.fullscreen(false);
    app.zoom(2);
    app.read_command_line(argc, argv);
    app.title(".:[ bbl / noise ]:.");
    app.set_root(noise);

    x::Bitmaps().load_bmp("top_left", topleft_data, topleft_size);

    top_left.image(x::Bitmaps("top_left").surface);
    top_left.position(0, 0);
    top_left.transparent_color(8);

    top_right.image(x::Bitmaps("top_left").surface);
    top_right.surface.vertical_mirror();
    top_right.position(app.width() - top_right.width(), 0);
    top_right.transparent_color(8);
 
    bottom_right.image(x::Bitmaps("top_left").surface);
    bottom_right.surface.vertical_mirror();
    bottom_right.surface.horizontal_mirror();
    bottom_right.position( app.width()  - bottom_right.width(),
                           app.height() - bottom_right.height());
    bottom_right.transparent_color(8);

    bottom_left.image(x::Bitmaps("top_left").surface);
    bottom_left.surface.horizontal_mirror();
    bottom_left.position(0, app.height() - bottom_left.height());    
    bottom_left.transparent_color(8);

    x::ColorPalette::black_to_white().activate();

    noise.fx.seed_noise(x::Video().backbuffer);

    noise.add_child(top_left);
    noise.add_child(top_right);
    noise.add_child(bottom_right);
    noise.add_child(bottom_left);

    float fps = app.execute();

    SDL_Log("fps = %f", fps);
    return 0;
}
