# Simple MOD Player

A very simple 4-CHAN MOD player in SDL.

File format overview:
 - http://manual.freeshell.org/tracker/Html/The_Protracker_file_format.html
 - http://coppershade.org/articles/More!/Topics/Protracker_File_Format/

Main source of understanding
 - [fmoddoc2.zip](docs/fmoddoc2.zip) by FireLight / Brett Paterson
 - [modformat.html](docs/modformat.html) (https://greg-kennedy.com/tracker/modformat.html)
 - [mod.txt](docs/mod.txt) by ByteRaver annotated by Thunder can help.
 - Sound mixing for the [GBA](http://deku.gbadev.org/program/sound1.html) ([Archive](docs/gba_mod.zip))

# Status

Highly incomplete and unoptimized, but it's a great pleasure to achieve the
replay of a single bit of sound after more than 20 years for having thought
about it ;)

Nothing very difficult, it's just about fun...