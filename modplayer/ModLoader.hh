/*
 * ModLoader.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2017
 * <pierrejean AT turpeau DOT net>
 *
 * Initialize and mix protracker data structures.
 *
 * Inspired by:
 *  - http://manual.freeshell.org/tracker/Html/The_Protracker_file_format.html
 *  - https://greg-kennedy.com/tracker/modformat.html
 *  - http://coppershade.org/articles/More!/Topics/Protracker_File_Format/
 */
#ifndef MODLOADER_H
#define MODLOADER_H

// TODO: switch to C++ limits... how ?
// #define __STDC_LIMIT_MACROS 1
#include <stdint.h>

#include "protracker.hh"
#include <x/tools/ByteReader.hh>

namespace x
{
    class ModLoader
    {
        public:
            ModLoader( uint32_t mixing_frequency, uint32_t mixing_chunk_count,
                uint8_t  mixing_channels );

            virtual ~ModLoader();

            void load(ByteReader& reader);

            void mix_audio(int16_t *output, uint32_t output_size);

            // public members
            // --------------

            mod_song_t _mod;

            // buffers for data visualization
            // TODO: temporary approach
            uint32_t _mixing_audio_chunks;
            int16_t *_channel_audio_buf[MOD_CHANNELS_MAXCOUNT];
            int16_t *_mono_audio_buf;

        protected:

        private:
            void _play_mod();
            void _play_pattern_row();

            void _mix_channels_stereo(int16_t *output, int index);
            void _mix_channels_mono(int16_t *output, int index);

            void _set_tempo(uint32_t bpm);
            void _set_speed(uint32_t speed);

            uint32_t _period_index(uint32_t period);

            // private data
            // ------------

            //ByteReader& _byteReader;
    };
}
#endif // MODLOADER_H
