/*
 * ModLoader.cc
 * Copyleft (c) Pierre-Jean Turpeau 11/2017
 * <pierrejean AT turpeau DOT net>
 *
 * Initialize and mix protracker data structures.
 *
 * Inspired by:
 *  - http://manual.freeshell.org/tracker/Html/The_Protracker_file_format.html
 *  - https://greg-kennedy.com/tracker/modformat.html
 *  - http://coppershade.org/articles/More!/Topics/Protracker_File_Format/
 */
#include "ModLoader.hh"

#include <algorithm>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

namespace x {

    ModLoader::ModLoader(
        uint32_t mixing_frequency,
        uint32_t mixing_chunk_count,
        uint8_t  mixing_channels )
    {

        //ctor
        _mod.mixing_frequency = mixing_frequency;
        _mod.mixing_channels  = mixing_channels;

        _mixing_audio_chunks  = mixing_chunk_count;

        for(int i = 0; i < MOD_CHANNELS_MAXCOUNT; i++)
        {
            _channel_audio_buf[i] = new int16_t[_mixing_audio_chunks];
        }

        _mono_audio_buf = new int16_t[_mixing_audio_chunks];

        for(int i = 0; i < 16; i++)
            _mod.unknown_fx[i] = 0;
    }

    ModLoader::~ModLoader()
    {
        for(int i = 0; i < MOD_CHANNELS_MAXCOUNT; i++)
        {
            if( _channel_audio_buf[i] )    delete _channel_audio_buf[i];
        }
        if( _mono_audio_buf ) delete _mono_audio_buf;
    }

    void ModLoader::load(ByteReader& reader)
    {
        uint32_t signature;

        _set_tempo(MOD_DEFAULT_BPM);
        _set_speed(MOD_DEFAULT_SPEED);

        _mod.replay_pattern_index = 0;
        _mod.replay_pattern_id = 0;
        _mod.replay_position_in_pattern = 0;
        _mod.mixing_count_remaining = _mod.mixing_count_before_tick;

        _mod.sample_pos_incr
            = ((MOD_AMIGA_C2_FREQ * MOD_AMIGA_C2_PERIOD)
                / _mod.mixing_frequency)
                    << MOD_SAMPLE_FP_SHIFT;

        printf("C2 Freq: %d - Sample Incr: %u - Shifted Incr: %u\n",
            MOD_AMIGA_C2_FREQ,
            ((MOD_AMIGA_C2_FREQ * 428) ) / _mod.mixing_frequency,
            _mod.sample_pos_incr);

        memset(&(_mod.channels), 0, sizeof(_mod.channels));

        reader.seek(0);
        reader.read_string(_mod.title, MOD_SONG_TITLE_LEN);

        // Check MOD type
        // --------------

        _mod.number_of_samples = 15;
        _mod.number_of_channels = 4;

        reader.seek(MOD_SIGNATURE_OFFSET);
        signature = reader.read_u32();

        switch(signature)
        {
        case MOD_SIGNATURE_MK1:
        case MOD_SIGNATURE_MK2:
        case MOD_SIGNATURE_FLT4:
        case MOD_SIGNATURE_4CHN:
            _mod.number_of_samples = 31;
            break;

        case MOD_SIGNATURE_6CHN:
            _mod.number_of_samples = 31;
            _mod.number_of_channels = 6;
            break;

        case MOD_SIGNATURE_FLT8:
        case MOD_SIGNATURE_8CHN:
            _mod.number_of_samples = 31;
            _mod.number_of_channels = 8;
            break;
        }

        printf("Title: %s\n", _mod.title);
        printf("Samples: %d\n", _mod.number_of_samples);

        // Read samples data
        // -----------------

        reader.seek(MOD_SAMPLE_OFFSET);

        for(uint32_t i = 0; i < _mod.number_of_samples; i++)
        {
            mod_sample_t *sample = &(_mod.samples[i]);

            reader.read_string(sample->name, MOD_SAMPLE_NAME_LEN);

            // stored as number of 16 bits words in MOD file
            sample->length = reader.read_u16() * 2;

            sample->data = new int8_t[sample->length]; // TODO: store MOD data address?

            sample->finetune = reader.read_u8();

            sample->volume = reader.read_u8();

            // dunno if its the right way to normalize.
            // seems it's needed however, according to tech docs.
            sample->volume = sample->volume > 64 ? 64 : sample->volume;

            sample->loop_offset = reader.read_u16() * 2;
            sample->loop_length = reader.read_u16() * 2;

            // Patch for old MOD with offset in bytes not words
            if( sample->loop_offset + sample->loop_length > sample->length )
            {
                sample->loop_length = sample->length - sample->loop_offset;
            }

            printf("   Sample %2u - len %5u - finetune %x - volume %2d - repeat(%4x,%4x) - name='%s'\n",
                i,
                sample->length,
                sample->finetune,
                sample->volume,
                sample->loop_offset,
                sample->loop_length,
                sample->name);
        }

        _mod.song_length = reader.read_u8();

        printf("Song length : %02d\n", _mod.song_length);

        reader.skip(1); // ignore protracker byte #951
        reader.read_bytes(_mod.song_patterns, MOD_PATTERNS_MAXCOUNT);

        if(_mod.number_of_samples > 15)
        {
            reader.skip(4); // ignore already read signature
        }

        // Lookup for the highest pattern id in the pattern sequence
        // ---------------------------------------------------------

        _mod.number_of_patterns = 0;

        for(uint32_t j = 0; j < MOD_PATTERNS_MAXCOUNT; j++)
        {
            if( _mod.song_patterns[j] > _mod.number_of_patterns )
            {
                _mod.number_of_patterns = _mod.song_patterns[j];
            }
        }
        _mod.number_of_patterns++;

        printf("Number of patterns: %02d\n", _mod.number_of_patterns);

        // Read patterns data
        // ------------------

        for(uint32_t k = 0; k < _mod.number_of_patterns; k++)
        {
            for( uint32_t p = 0; p < MOD_PATTERN_DIVIONS; p++ )
            {
                for(uint32_t m = 0; m < _mod.number_of_channels; m++)
                {
                    mod_note_t *note;
                    uint8_t a,b,c,d;

                    note = &(_mod.patterns_notes[k][p][m]);

                    a = reader.read_u8();
                    b = reader.read_u8();
                    c = reader.read_u8();
                    d = reader.read_u8();

                    // sample number (8 bits)
                    note->sample_id = (a & 0xF0) + ((c >> 4) & 0x0F);

                    // period (12 bits)
                    note->period = ((a & 0x0F) << 8) + b;

                    // effect command (4 bits)
                    note->fx = c & 0x0F;

                    // effect param (8 bits)
                    note->fx_param = d;

                    // store the note real name
                    note->name = mod_note_names[_period_index(note->period)];
                }
            }
        }

        // Load sample data
        // ----------------

        // TODO: store loaded MOD address only...
        for(uint32_t n = 0; n < _mod.number_of_samples; n++)
        {
            mod_sample_t *sample = &(_mod.samples[n]);
            if( sample->length > 1)
            {
                reader.read_bytes((uint8_t *) sample->data, sample->length);
            }
        }

        printf("Error_number: %d - reader pos: %d\n", reader.error_number, reader.tell());
    }

    // stream size is supposed to be in number of 16 bits words
    void ModLoader::mix_audio(int16_t *audio_stream, uint32_t stream_size)
    {
        int index = 0;

        // here we want to fill the output mixing buffer with the song audio data
        while( stream_size > 0)
        {
            if( _mod.mixing_count_remaining-- <= 0 )
            {
                _play_mod();
                _mod.mixing_count_remaining = _mod.mixing_count_before_tick;
            }

            switch( _mod.mixing_channels )
            {
            case 1:
                _mix_channels_mono(audio_stream, index);
                break;

            case 2:
                _mix_channels_stereo(audio_stream, index);
                break;
            }

            index++;
            audio_stream += _mod.mixing_channels;
            stream_size  -= _mod.mixing_channels;
        }
    }

    void ModLoader::_play_mod()
    {
        static uint32_t tick = _mod.speed; // so that @ first call we process the first row immediately

        // process a new row in pattern?
        if( ++tick >= _mod.speed )
        {
            // reset tick to wait before processing the next row
            tick = 0;

            _play_pattern_row();

            // change position, then pattern and loop the song in the end...
            if( ! _mod.jump_fx && ++_mod.replay_position_in_pattern >= MOD_PATTERN_DIVIONS )
            {
                printf("\n");
                _mod.replay_position_in_pattern = 0;
                if( ++_mod.replay_pattern_index >= _mod.song_length )
                {
                    _mod.replay_pattern_index = 0;
                }
            }
        }
        else
        {
            // In between two notes, channels effects are updated.

            //TODO: _update_effects();

            // TBC : update effect even at tick 0?
        }
    }

    void ModLoader::_play_pattern_row()
    {
        mod_note_t *note;
        mod_sample_t *sample;
        mod_channel_t *channel;

    _mod.replay_pattern_id = _mod.song_patterns[_mod.replay_pattern_index];

        printf("%02d/%02d - %02d/%02d", _mod.replay_pattern_index, _mod.song_length, _mod.replay_pattern_id, _mod.replay_position_in_pattern);

        _mod.jump_fx = 0;

        // prepare channels data with a new row
        for(int i = 0; i < _mod.number_of_channels; i++)
        {
            channel = &(_mod.channels[i]);

            note = &(_mod.patterns_notes[_mod.replay_pattern_id][_mod.replay_position_in_pattern][i]);

            if( note->period )
            {
                channel->period = note->period;
                printf(" | %s", note->name);
            }
            else
            {
                printf(" | ...");
            }

            if( note->sample_id > 0)
            {
                sample = &(_mod.samples[note->sample_id-1]); // 0 means no sample...

                channel->sample_data     = sample->data;
                channel->sample_pos      = 0;

                // due to hyper resolution of mixing (e.g. 44KHz vs 8KHz) we need
                // to scale sample length and position using fixed point.
                channel->sample_length   = sample->length << MOD_SAMPLE_FP_SHIFT;
                channel->sample_loop_pos = sample->loop_offset << MOD_SAMPLE_FP_SHIFT;
                channel->sample_loop_len = sample->loop_length << MOD_SAMPLE_FP_SHIFT;

                channel->finetune = sample->finetune;

                printf(" %02d", note->sample_id);

                // TODO: store note effect for long term management in _play_mod()
                if( note->fx == MOD_FX_SET_VOLUME )
                {
                    channel->volume = note->fx_param;
                    printf(" ...");
                }
                else
                {
                    channel->volume = sample->volume;
                    printf(" v%02d", sample->volume);
                }
            }
            else
            {
                channel->finetune = 0;
                printf(" .. ...");
            }

            if( note->fx || note->fx_param )
            {
                printf(" %1X%02X", note->fx, note->fx_param);
            }
            else
            {
                printf(" ...");
            }

            if( channel->finetune )
            {
                printf(" %1X", channel->finetune);
            }
            else
            {
                printf(" .");
            }

            switch( note->fx )
            {

            case MOD_FX_SET_SPEED:
                if( note->fx_param < 32 )
                {
                    if(note->fx_param == 0)
                        note->fx_param++;
                    _set_speed(note->fx_param);
                }
                else
                {
                    _set_tempo(note->fx_param);
                }
                break;

            case MOD_FX_PATTERN_BREAK:
                // switch to next pattern index and check for song overflow
                if( ++_mod.replay_pattern_index >= _mod.song_length )
                {
                    _mod.replay_pattern_index = 0;
                }

                // compute position in the new pattern
                _mod.replay_position_in_pattern = ((note->fx_param>>4)&0xF)*10 + (note->fx_param&0xF);

                _mod.jump_fx = true;
                break;

            case MOD_FX_SET_VOLUME:
                break;

            default:
                if(note->fx || note->fx_param)
                    _mod.unknown_fx[note->fx] = ((note->fx<<8)&0x0F00)+(note->fx_param&0xFF);
                break;
            }
        }

        printf("\n");
    }

    void ModLoader::_mix_channels_stereo(int16_t *output, int index)
    {
        int32_t channel_chunk;
        int32_t left_audio_chunk = 0;
        int32_t right_audio_chunk = 0;

        for(uint32_t i = 0; i < _mod.number_of_channels; i++)
        {
            mod_channel_t *channel = (&_mod.channels[i]);

            if( channel->period && channel->sample_data != NULL )
            {
                channel_chunk =
                    channel->sample_data[channel->sample_pos >> MOD_SAMPLE_FP_SHIFT]
                        * channel->volume;

                channel->sample_pos += (_mod.sample_pos_incr / channel->period);

                // according to one of the doc, you should loop only if the
                // repeat length is greater than 1 (16bits word so => 2 bytes)
                if( channel->sample_loop_len > (2 << MOD_SAMPLE_FP_SHIFT) )
                {
                    if( channel->sample_pos >= channel->sample_loop_pos + channel->sample_loop_len)
                    {
                        channel->sample_pos =
                            channel->sample_loop_pos
                                + (channel->sample_pos % (channel->sample_loop_pos + channel->sample_loop_len));
                    }
                }
                else
                {
                    if( channel->sample_pos >= channel->sample_length )
                    {
                        channel->sample_length = 0;
                        channel->sample_pos = 0;
                    }
                }

                // For stereo output:
                //  - MOD channels : 1 2 3 4 5 6 7 8
                //  - audio output : L R R L L R R L
                switch( i & 3 )
                {
                case 0:
                case 3:
                    left_audio_chunk += channel_chunk;
                    break;

                case 1:
                case 2:
                    right_audio_chunk += channel_chunk;
                    break;
                }
            }
            else
            {
                channel_chunk = 0;
            }

            if( _channel_audio_buf[i] )
            {
                _channel_audio_buf[i][index] = channel_chunk;
            }
        }

        // TODO: global volume

        left_audio_chunk  = ((left_audio_chunk + _mod.last_left_chunk) >> 2) <<1;
        right_audio_chunk = ((right_audio_chunk + _mod.last_right_chunk) >> 2) <<1;

        // crop to 16 bits replay limitations
        if( left_audio_chunk  > INT16_MAX ) left_audio_chunk  = INT16_MAX;
        if( left_audio_chunk  < INT16_MIN ) left_audio_chunk  = INT16_MIN;
        if( right_audio_chunk > INT16_MAX ) right_audio_chunk = INT16_MAX;
        if( right_audio_chunk < INT16_MIN ) right_audio_chunk = INT16_MIN;

        _mod.last_left_chunk  = left_audio_chunk;
        _mod.last_right_chunk = right_audio_chunk;

        *output     = left_audio_chunk;
        *(++output) = right_audio_chunk;

        _mono_audio_buf[index] = (left_audio_chunk + right_audio_chunk) >> 1;
    }

    void ModLoader::_mix_channels_mono(int16_t *output, int index)
    {
        int32_t channel_chunk;
        int32_t audio_chunk = 0;

        for(uint32_t i = 0; i < _mod.number_of_channels; i++)
        {
            mod_channel_t *channel = (&_mod.channels[i]);

            if( channel->period != 0 && channel->sample_data != NULL )
            {
                if( channel->period )
                {
                    channel->sample_pos += (_mod.sample_pos_incr / channel->period);
                }

                // according to one of the doc, you should loop only if the
                // repeat length is greater than 1 (16bits word so => 2 bytes)
                if( channel->sample_loop_len > 2)
                {
                    if( channel->sample_pos >= channel->sample_loop_pos + channel->sample_loop_len)
                    {
                        channel->sample_pos =
                            channel->sample_loop_pos
                                + (channel->sample_pos % (channel->sample_loop_pos + channel->sample_loop_len));
                    }
                }
                else
                {
                    if( channel->sample_pos >= channel->sample_length )
                    {
                        channel->sample_data = NULL;
                        channel->sample_pos = 0;
                    }
                }

                channel_chunk =
                    channel->sample_data[channel->sample_pos >> MOD_SAMPLE_FP_SHIFT]
                        * channel->volume;

                // 127 (max audio chunk) * 64 (max vol) * 8 (max channels) < 64K
                audio_chunk += channel_chunk;
            }
            else
            {
                channel_chunk = 0;
            }

            if( _channel_audio_buf[i] )
            {
                _channel_audio_buf[i][index] = channel_chunk;
            }
        }

        // average volume according to the number of channels
        audio_chunk /= _mod.number_of_channels;
        //audio_chunk *= 64; // TODO : replace by global volume?

        // crop to 16 bits replay limitations
        if( audio_chunk  > INT16_MAX ) audio_chunk  = INT16_MAX;
        if( audio_chunk  < INT16_MIN ) audio_chunk  = INT16_MIN;

        *output = audio_chunk;

        _mono_audio_buf[index] = audio_chunk;
    }

    void ModLoader::_set_tempo(uint32_t bpm)
    {
        _mod.bpm = bpm;
        _mod.mixing_count_before_tick  = (_mod.mixing_frequency * 5 >> 1) / bpm;
    }

    void ModLoader::_set_speed(uint32_t speed)
    {
        _mod.speed = speed;
    }

    // TODO: binary search would be faster, but in the loading phase we don't care
    uint32_t ModLoader::_period_index(uint32_t period)
    {
        int num_periods = sizeof(mod_period_table) / sizeof(mod_period_table[0]);

        for( int i = 0; i < num_periods; i++ )
        {
            if( period == mod_period_table[i] )
            {
                return i;
            }
            else if( period > mod_period_table[i] )
            {
                if( i == 0)
                {
                    return i;
                }
                else
                {
                    if ( mod_period_table[i] - period < period - mod_period_table[i-1] )
                    {
                        return i-1;
                    }
                    else
                    {
                        return i;
                    }
                }
            }
        }
        return num_periods-1;
    }
}