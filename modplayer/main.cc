/*
 * main.cc
 * Copyleft (c) Pierre-Jean Turpeau 11/2017
 * <pierrejean AT turpeau DOT net>
 *
 */
#include <iostream>
#include <cstdio>
#include <assert.h>
#include <exception>

#include <SDL2/SDL_audio.h>
#include <ThrowAssert/ThrowAssert.hpp>

#define X_IMPLEMENTATION
#include <x/x.hh>

#include "ModLoader.hh"

using namespace std;
using namespace x;

#define WIDTH  320
#define HEIGHT 240

class ModLoop : public x::ChunkyModeLoop
{
public:

    ModLoop() : data(nullptr), datasize(0), mod_loader(44100,2048,2), audio_device(0)
    {
        SDL_memset(&fmt, 0, sizeof(fmt));

        fmt.freq     = 44100;     // number of sample frames per second
        fmt.format   = AUDIO_S16; // each channel is 16 bits
        fmt.channels = 2;         // 1 = mono, 2 = stereo

        // fmt.samples is the number of audio chunks. According to the SDL doc
        // the size in bytes of the buffer sent to the callback depends on both
        // the format and the number of channels
        fmt.samples  = 2048; // seems to be a good value
        fmt.userdata = &mod_loader;
        fmt.callback = ModLoop::mixaudio_callback;
    }

    ~ModLoop() {
        if( data != nullptr ) delete [] data;
        SDL_CloseAudioDevice(audio_device);
    }

    // callback audio
    static void mixaudio_callback(void *user_data, uint8_t *audio_stream, int32_t stream_len)
    {
        ModLoader *loader = static_cast<ModLoader*>(user_data);
        loader->mix_audio((int16_t *) audio_stream, stream_len / sizeof(int16_t));
    }


    void load(const std::string& name) {
        _load_file(name);
        ByteReader byte_reader(data, datasize);
        mod_loader.load(byte_reader);
    }

    void start_replay() {
        audio_device = SDL_OpenAudioDevice(NULL, 0, &fmt, NULL, 0);

        if ( audio_device == 0 ) {
            std::string const err = SDL_GetError();
            throw std::runtime_error("Failed to open audio device: " + err);
        } else
            SDL_PauseAudioDevice(audio_device, 0);
    }

    // TODO: paste into a FileByteReader implementation.
    void _load_file(const std::string& filename)
    {
        FILE *f = fopen(filename.c_str(), "rb");
        if( f != NULL)
        {
            // obtain file size
            fseek(f, 0, SEEK_END);
            datasize = ftell(f);
            rewind(f);
            assert( datasize > 0);

            data = new uint8_t[datasize];
            assert(data);

            size_t result = fread(data, 1, datasize, f);
            assert( result == datasize );

            SDL_Log("bytes read: %u / %ul", result, datasize);

            fclose(f);

            SDL_Log("file: %s - %ul bytes", filename.c_str(), datasize);
        }
        else
        {
            throw std::runtime_error("unable to open file: " + filename);
        }
    }

    void loop_callback(x::PixelSurface& surface, float delta_time_ms)
    {
        int32_t  y;

        uint32_t channel_display_height = HEIGHT>>2;

        surface.clear();

        for(int i = 0; i < WIDTH; i++)
        {
            uint32_t audio_chunk_index = (i * mod_loader._mixing_audio_chunks ) / WIDTH;

            for( int cc = 0; cc < 4; cc++)
            {
                y = -mod_loader._channel_audio_buf[cc][audio_chunk_index];
                y *= (channel_display_height>>1);
                y /= (127*64);
                y += channel_display_height*cc;
                y += (channel_display_height>>1);

                surface.set_safe(i, y, 200 + cc);
            }
        }
    }

    uint8_t *data;
    size_t datasize;
    ModLoader mod_loader;

    SDL_AudioSpec fmt;
    SDL_AudioDeviceID audio_device;
};


int main(int argc, char **argv)
{
    auto usage = [](cstrptr_t basename)->void {
        SDL_Log("%s usage:", basename);
        SDL_Log("\t-f <filename>   mod file to play");
    };

    x::ChunkyModeAttributes attributes;
    attributes.read_command_line(argc, argv, usage);
    
    std::string mod_name;

    for(int i = 0; i < argc; i++)
    {
        string str(argv[i]);
        if( ! str.compare("-f") ) {
            if( i < argc-1 )
                mod_name = string(argv[i+1]);
            else {
                SDL_Log("missing argument\n\n");
                usage(*argv);
                exit(EXIT_FAILURE);
            }
        } 
    }

    if(mod_name.length() == 0) {
        SDL_Log("missing argument\n\n");
        usage(*argv);
        exit(EXIT_FAILURE);
    }

    x::ChunkyMode video_mode(WIDTH, HEIGHT, ".:[ bbl / mod player ]:.", attributes);

    video_mode.palette.set_color(0, 0, 0, 0);
    video_mode.palette.set_color(200, 0xFF, 0, 0);
    video_mode.palette.set_color(201, 0, 0xFF, 0);
    video_mode.palette.set_color(202, 0, 0, 0xFF);
    video_mode.palette.set_color(203, 0xFF, 0, 0xFF);

    ModLoop main_loop;

    main_loop.load(mod_name);

    main_loop.start_replay();

    fclose(stdout); // to prevent printf() disturb the replay routine

    float fps = video_mode.execute(main_loop);

    for(int i = 0; i < 16; i++)
    {
        uint16_t v = main_loop.mod_loader._mod.unknown_fx[i];
        SDL_Log("fx = %X : %X %X", i, (v>>8)&0xF, v&0xFF);
    }

    SDL_Log("fps = %f", fps);
    return(EXIT_SUCCESS);
}
