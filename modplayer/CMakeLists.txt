cmake_minimum_required(VERSION 3.0)

project(bbl-modplayer VERSION 1.0 LANGUAGES CXX)

set(SOURCES
    main.cc
    ModLoader.cc
    ../x/tools/ByteReader.cc
)   

set(HEADERS
    protracker.hh
    ModLoader.hh
)

if(WIN32)
    set(RESOURCES resources.rc)
endif()


list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/..")
include(common)
