# The TODO list

## fx

_(to be organized, prioritized, completed, ...)_

- [ ] Caustics / god rays like in milkshake/blueblue
- [ ] Crayon lines rendering using generated brush strokes patterns
- [ ] Gradient moiré patterns with bump mapping (play with thickness) like in
[Back to the roots / Haujobb](https://youtu.be/XzWNOV4W2ME?t=137)
- [ ] Boucing Ball as in [Virutal Escape / Equinox](https://youtu.be/i_7ZQBkobY4?t=117)
- [ ] Vector balls with sprite generation using super-sampling (as I did in J2ME)
- [ ] Metaballs/2d blobs using simplified iso surfaces (with precalc bitmaps?)
    - with background smoothing (divide by 2) to have a shade/smear effect
    - play on thresholds
    - use an octree to have a huge number of them?
    - uses power of 2 sizes to optimize distance calculation for smaller balls?
    - try with multiple colors as in [this sdl version](https://gynvael.coldwind.pl/?id=21)
    - see [Exploring Metaballs and Isosurfaces in 2D](https://www.gamedev.net/articles/programming/graphics/exploring-metaballs-and-isosurfaces-in-2d-r2556/)
    - see [mad team](http://madteam.atari8.info/index.php?prod=fx#metaballs)
    - see [furrtek](http://furrtek.free.fr/index.php?a=demofx&ss=2)
- [ ] Dynamic 2d water effect with splashes [gamedev](https://gamedevelopment.tutsplus.com/tutorials/make-a-splash-with-dynamic-2d-water-effects--gamedev-236)
- [ ] Glenz on a moving checker-board like in [Cristal Dream/Triton](https://youtu.be/HW1TlCrC2Qo?t=330)
- [ ] Work on better plasma palette like in [Cristal Dream 2 / Triton](https://youtu.be/BLMUfBikxTY?t=182)
- [ ] Generated 3d torus switching from shaded wireframe, backface culling, 
  particles, flat shaded, gouraud shaded, env mapping.
- [ ] Grid rendering pattern using different technics: discs, animated/blured
stuffs, squares, particles/shade bobs ??

- plasma sur des gros pixels séparés les uns des autres sur fond de couleur
  avec un zoom progressif façon drapeau / sinusoide...

- sinus scroll : cette fonte est sympa http://www.stashofcode.fr/wp-content/uploads/figure1-3.png

- http://madteam.atari8.info/index.php?prod=fx   <<!!!!!>>

- http://maettig.com/media/dos/demo-graphics-effects/stars-3d-animation.gif
  Generate a terrain, draw only vertex points first.
  Then replace points by balls (opage and transparent?)
  Then move to voxel drawing.

- [ ] Twister bars : http://furrtek.free.fr/index.php?a=demofx&ss=8

- [ ] Fluid (or dust) simulation : http://www.dgp.toronto.edu/~stam/reality/Research/pub.html
https://www.youtube.com/watch?v=alhpH6ECFvQ

## Development environment

- [ ] Use http://astyle.sourceforge.net/
- [X] Hierarchical cmake : http://techminded.net/blog/modular-c-projects-with-cmake.html
- [X] Switch to CMake for build system
- [X] Remove CodeBlocks projects
- [X] Move to Visual Studio Code

## Common lib features devs

- [ ] font display utilities
- [ ] texture mapping
- [ ] perspective texture mapping
- [ ] Average FPS based on http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement
- [x] look at https://github.com/nothings/stb (e.g., image library) : not so exciting after all
- [x] Decide when to use SDL_Log or cout/cerr (see style.md)
- [x] try minifb to challenge SDL against frame glitches on office laptop (see tests/minifb)
- [x] identify and fix memory leak in BitmapManager (thanks to DrMemory)
- [x] Scene-graph base demo framework with a kind of declarative approach (low priority)
- [x] Standardize the notion of FX with update() and render()
- [X] Raycasting tunnel: code interpolation version
- [x] Second step ChunkyMode sanitization (BMPLoading, palette rendering in 8bpp)
- [x] First step ChunkyMode sanitization (externalize surface, palette, etc.)
- [x] Use of SDL_WINDOW_FULLSCREEN rather than SDL_WINDOW_FULLSCREEN_DESKTOP

## External inspirations (to be sorted)

- noises function to generate texture  
   http://catlikecoding.com/unity/tutorials/simplex-noise/  
   https://github.com/jobtalle/CubicNoise

- Learn from  
   http://casual-effects.com/  
   https://www.3dgep.com/

- Porting  
    https://r3dux.org/2011/05/simple-opengl-keyboard-and-mouse-fps-controls/  
    https://r3dux.org/2011/02/how-to-perform-manual-frustum-culling/

- bisqwit:  
    https://bisqwit.iki.fi/  
    https://www.youtube.com/watch?v=xW8skO7MFYw  
    https://www.youtube.com/watch?v=VL0oGct1S4Q  
    https://www.youtube.com/watch?v=vkUwT9U1GzA  
    https://www.youtube.com/watch?v=MvDtr3cNaLU  

- fx
    - 3D transforms: http://furrtek.free.fr/index.php?a=demofx&ss=6
    - Smoke: https://github.com/npiegdon/immediate2d/blob/master/example8_smoke.cpp
    - Interpolation examples (for animations) http://sol.gfxile.net/interpolation/
