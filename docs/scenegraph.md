# 2D Scenegraph for demo fx

Incremental construction of a simple 2D scenegraph approach.
A first example of usage is available in `fx-8bits/noise/`.
~~It is designed with chained methods~ in mind as most as possible when
it make sense.~~

## Improvements

Certainly several, non exhaustive list is:

1. switch the `ChunkyMode` parameter for something more like a `GraphicContext`,
allowing to manage transformation in a cleaner way (translation at first)

2. The GraphicContext concept shall help in managing offscreen rendering (e.g.,
for later use as a texture)

3. Put the current BitmapManager within the GraphicContext?
How to retrieve the right BitmapManager during the initialization phase?
Is a singleton BitmapManager the right thing to do ?

4. Dynamic allocation / destruction of the scenegraph and associated bitmaps
Once a scene have been processed and replaced by a new scene, how is it possible
to release a full sub-graph?

5. Introduce the concept of Scene or View to manage the transition in-between?

## Roadmap

a possible futur content of `x/sg/` is

- core
    - SceneGraph
    - Node
- animation (all SGAnimation sub-classes)
    - SineAnimation
    - LissajouAnimation ?
    - SplineAnimation ?
- fx
    - fire
    - noise
    - rotozoom
    - tunnel_2d
    - tunnel_raycast
    - voxel
    - wormhole
    - plasma
    - unlimited_bobs (multiple image trick)
    - meta-balls
    - infinite triangles
        - render 4 triangles offscreen
        - use them as a quad texture in the middle of the screen
- graphics
    - ansi
    - bitmap
    - metaball (render one metaball ?)
    - offscreen_rendering
    - pixelize
    - rectangle
    - sprite
    - textured_triangle
    - text
    - vector ? (like in paper)
- procedural
    - cloud
    - terrain
    - plasma
    - specular
- sound
    - mod_player
- transition: for fullscreen transitions
    - tbd
