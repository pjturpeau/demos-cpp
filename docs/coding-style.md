# Coding style

Here I address few topics I had to face during my coding activities (at least,
the one I took the time to formalize)

## Size reduction

todo... (avoid iostream, exceptions, ...)

## Enum vs Const vs #define

const must be used unless a good reason to not do so (because of a special
feature offered by the two other approaches).

A good source is the answer in
[static const vs #define by Tony Delroy](https://stackoverflow.com/questions/1637332/static-const-vs-define/3835772#3835772)

Also prefer `constexpr` as much as possible.

## CamelCase vs underscores separated

- Class name: `CamelCasePure`
- Instance name: `camelCaseImpure`
- Variables, attributes and operations: `lower_case_underscore_separated`

## Public vs private

Private attributes and operations start with an underscore
(e.g., `_private_operation()`, `_private_attribute`)

## Logging

Gracefull error management with error message vs throwing an exception has to be studied...

In release mode the `-mwindows` link option remove all console outputs (both stdin & stderr). As such`SDL_Log()` shall be used for all required CLI outputs (e.g., usage info)

- Use `APP_LOG_ERROR` (shortcut to `SDL_LogError`) for all runtime errors

- Use `APP_LOG_DEBUG` (shortcut to `SDL_LogDebug`) for all complimentary
information that could be displayed using the CLI `-debug` switch.

- `std::cerr/cout` shall not be used

## C++ Coding Best Practices

- [The contents of namespaces are not indented](https://www.jarchitect.com/QACenter/index.php?qa=116&qa_1=the-contents-of-namespaces-are-not-indented)
