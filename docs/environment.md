# Development environment

| Kind     | Details & Links
|----------|--------------------------------------------------
| OS       | [Windows with MinGW](https://sourceforge.net/projects/mingw-w64/)
| Repo     | [Portable Git](https://git-scm.com/download/win)
| Build    | [CMake](http://cmake.org/)
| Build    | [CppProjectTemplate](https://github.com/Barthelemy/CppProjectTemplate)
| IDE      | [VS Code](https://code.visualstudio.com/)
| Lib      | [SDL2](https://www.libsdl.org/)
| Lib      | [INCBIN](https://github.com/graphitemaster/incbin)
| Lib      | [ThrowAssert](https://github.com/Softwariness/ThrowAssert)
| Lang     | [C++11 (newbie)](http://en.cppreference.com/w/)
| Debug    | [CppCheck](http://cppcheck.sourceforge.net/)
| Debug    | [DrMemory](http://drmemory.org/)

## Windows platform dependencies

The code should rely solely on the SDL library so that it can be built on SDL
supported platforms.

Yet, if you still find abnormal dependencies to the windows platform, feel
free to provide feedback.

## Shell aliases for quick build

```console
alias cmk-release='BN=`basename $PWD`; if [ ${BN} = build -o ${BN} = debug ]; then cd ..; fi; rm -rf build/; mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" ..'

alias cmk-debug='BN=`basename $PWD`; if [ ${BN} = build -o ${BN} = debug ]; then cd ..; fi; rm -rf debug/; mkdir debug && cd debug && cmake -DCMAKE_BUILD_TYPE=Debug -G "Unix Makefiles" ..'
```
