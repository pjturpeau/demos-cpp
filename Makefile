default:
	[[ -d build ]] && rm -r build || true;
	mkdir build && cd build; cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" .. && make

debug:
	[[ -d debug ]] && rm -r debug || true;
	mkdir debug && cd debug; cmake -DCMAKE_BUILD_TYPE=Debug -G "Unix Makefiles" .. && make

clean:
	find . -type f -name "build" | xargs rm -rf
	find . -type f -name "debug" | xargs rm -rf
