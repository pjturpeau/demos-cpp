/*
 * ansi.hh
 * Copyleft (c) Pierre-Jean Turpeau 07/2018
 * <pierrejean AT turpeau DOT net>
 *
 */
 #ifndef BBL_ANS_ANSI_HH
 #define BBL_ANS_ANSI_HH

/*
 
TextMode inherit ANSIListener ?
 - x, y
 - fgTextColor, bgTextColor
 - attribute (high intensity, blinking, underline, ...)
 - clearColor = bgTextColor...


TextMode ( ChunkyMode, Font );
 => TextWidth = w / font.w;
 => TextHeight = H / font.h;
 
*/

class ansi_attributes
{
    public:
    
        static const int RESET = 0;

        static const int HIGH_INTENSITY   = 1;
        static const int NORMAL_INTENSITY = 22;

        static const int ITALIC = 3;
        
        static const int UNDERLINE        = 4;
        static const int DOUBLE_UNDERLINE = 21;
        static const int NO_UNDERLINE     = 24;
	
	    static const int BLINK      = 5; // less than 150 per minute
	    static const int FAST_BLINK = 6; // more than 150 per minute
	    static const int NO_BLINK   = 25;
	
	    static const int REVERSE    = 7;
	    static const int NO_REVERSE = 27;
	
	    static const int HIDE = 8;
	    static const int SHOW = 28;
};

class ansi_color
{	
    public:
	    static const int BLACK   = 0;
	    static const int RED     = 1;
	    static const int GREEN   = 2;
	    static const int YELLOW  = 3;
	    static const int BLUE    = 4;
	    static const int MAGENTA = 5;
	    static const int CYAN    = 6;
	    static const int WHITE   = 7;
};

#endif // BBL_ANS_ANSI_HH
