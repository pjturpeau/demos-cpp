/*
 * sdl2.hh
 * Copyleft (c) Pierre-Jean Turpeau 03/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_SDL2_HH
#define X_SDL2_HH

#include <SDL2/SDL.h>

#include <memory>

// c++ smart wrappers from https://swarminglogic.com/jotting/2015_05_smartwrappers
namespace sdl2
{
    struct SDL_Deleter {
        void operator()(SDL_Surface*  ptr) { if (ptr) SDL_FreeSurface(ptr); }
        void operator()(SDL_Texture*  ptr) { if (ptr) SDL_DestroyTexture(ptr); }
        void operator()(SDL_Renderer* ptr) { if (ptr) SDL_DestroyRenderer(ptr); }
        void operator()(SDL_Window*   ptr) { if (ptr) SDL_DestroyWindow(ptr); }
        void operator()(SDL_RWops*    ptr) { if (ptr) SDL_RWclose(ptr); }
    };
 
    // Unique Pointers
    using SurfacePtr  = std::unique_ptr<SDL_Surface,  SDL_Deleter>;
    using TexturePtr  = std::unique_ptr<SDL_Texture,  SDL_Deleter>;
    using RendererPtr = std::unique_ptr<SDL_Renderer, SDL_Deleter>;
    using WindowPtr   = std::unique_ptr<SDL_Window,   SDL_Deleter>;
    using RWopsPtr    = std::unique_ptr<SDL_RWops,    SDL_Deleter>;

    template<class T, class D = std::default_delete<T>>
    struct shared_ptr_with_deleter : public std::shared_ptr<T>
    {
        explicit shared_ptr_with_deleter(T* t = nullptr)
            : std::shared_ptr<T>(t, D()) {}

        void reset(T* t = nullptr) {
            std::shared_ptr<T>::reset(t, D());
        }
    };

    using SurfaceShPtr  = shared_ptr_with_deleter<SDL_Surface,  SDL_Deleter>;
    using TextureShPtr  = shared_ptr_with_deleter<SDL_Texture,  SDL_Deleter>;
    using RendererShPtr = shared_ptr_with_deleter<SDL_Renderer, SDL_Deleter>;
    using WindowShPtr   = shared_ptr_with_deleter<SDL_Window,   SDL_Deleter>;
    using RWopsShPtr    = shared_ptr_with_deleter<SDL_RWops,    SDL_Deleter>;
}

#endif // X_SDL2_HH
