/*
 * rgb.hh
 * Copyleft (c) Pierre-Jean Turpeau 03/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_RGB_HH
#define X_RGB_HH

namespace x {
typedef struct {
    union {
        uint16_t value;
        struct {
        #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
            unsigned b : 5;
            unsigned g : 6;
            unsigned r : 5;
        #else
            unsigned r : 5;
            unsigned g : 6;
            unsigned b : 5;
        #endif
        };
    };
}
rgb16_t;

typedef struct {
    union {
        uint32_t value;
        struct {
        #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
            unsigned b : 8;
            unsigned g : 8;
            unsigned r : 8;
            unsigned a : 8;
        #else
            unsigned a : 8;
            unsigned r : 8;
            unsigned g : 8;
            unsigned b : 8;
        #endif
        };
    };
}
argb32_t;

}
#endif // X_RGB_HH
