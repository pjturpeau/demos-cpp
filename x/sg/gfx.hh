/*
 * gfx.hh
 * Copyleft (c) Pierre-Jean Turpeau 03/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_SG_GFX_HH
#define X_SG_GFX_HH

#ifdef X_IMPLEMENTATION

#define X_TRANSPARENTSURFACE_IMPLEMENTATION
#define X_OPAQUESURFACE_IMPLEMENTATION

#endif // X_IMPLEMENTATION

#include <x/sg/gfx/TransparentSurface.hh>
#include <x/sg/gfx/OpaqueSurface.hh>

#endif // X_SG_GFX_HH
