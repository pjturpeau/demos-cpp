/*
 * OpaqueSurface.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_OPAQUESURFACE_HH
#define X_OPAQUESURFACE_HH

#include <x/gfx/PixelSurface.hh>

namespace x { namespace sg {

/**
 * Displays the content of an offscreen PixelSurface
 */
class OpaqueSurface : public Node
{
public:
    OpaqueSurface();

    void image(const PixelSurface& surface);
    void position(int x, int y);
    bool update_node(PixelSurface& surface, float delta_time_ms);

    int x = 0;
    int y = 0;

    PixelSurface surface;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_OPAQUESURFACE_IMPLEMENTATION

OpaqueSurface::OpaqueSurface() {}

void OpaqueSurface::image(const PixelSurface& surface) {
    this->surface = surface;
}

void OpaqueSurface::position(int x, int y) {
    this->x = x;
    this->y = y;
}

bool OpaqueSurface::update_node(PixelSurface& surface, float delta_time_ms) {
    this->surface.copy_to(surface, x, y);
    return true;
}

#endif // X_OPAQUESURFACE_IMPLEMENTATION
}}
#endif // X_OPAQUESURFACE_HH
