/*
 * TransparentSurface.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_TRANSPARENTSURFACE_HH
#define X_TRANSPARENTSURFACE_HH

#include <x/gfx/PixelSurface.hh>

namespace x { namespace sg {

class TransparentSurface : public Node
{
public:
    void image(const PixelSurface& surface);
    void position(int x, int y);
    void transparent_color(int index);

    int width();
    int height();

    /** return false to avoid traversal of childrens */
    virtual bool update_node(PixelSurface& to_surface, float delta_time_ms);

    int x = 0;
    int y = 0;
    int transp_color = 0xFFFFFF;
    PixelSurface surface;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_TRANSPARENTSURFACE_IMPLEMENTATION 

void TransparentSurface::image(const PixelSurface& surface) {
    this->surface = surface;
}

void TransparentSurface::position(int x, int y) {
    this->x = x;
    this->y = y;
}

void TransparentSurface::transparent_color(int index) {
    transp_color = index;
}

int TransparentSurface::width() {
    return surface.w;
}

int TransparentSurface::height() {
    return surface.h;
}

/** return false to avoid traversal of childrens */
bool TransparentSurface::update_node(PixelSurface& to_surface,
                                        float delta_time_ms) {
    this->surface.copy_to(to_surface, x, y, transp_color);
    return true;
};

#endif // X_TRANSPARENTSURFACE_IMPLEMENTATION
}}
#endif // X_TRANSPARENTSURFACE_HH
