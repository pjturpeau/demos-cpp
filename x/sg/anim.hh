/*
 * anim.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_SG_ANIM_HH
#define X_SG_ANIM_HH

#ifdef X_IMPLEMENTATION

#endif // X_IMPLEMENTATION

#include <x/sg/anim/Animation.hh>
#include <x/sg/anim/NumberAnimation.hh>
#include <x/sg/anim/PauseAnimation.hh>
#include <x/sg/anim/SequentialAnimation.hh>
#include <x/sg/anim/PaletteAnimation.hh>
#include <x/sg/anim/LambdaAction.hh>
#include <x/sg/anim/SineAnimation.hh>

#endif // X_SG_ANIM_HH
