/*
 * CyclicAction.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_CYCLICACTION_HH
#define X_CYCLICACTION_HH

#include <x/sg/core/Node.hh>
#include <functional>
 
namespace x { namespace sg {

/** Graphical action cyclically triggered by the rendering pipepline at each frame */
class CyclicAction : public Node
{
public:
    /** define the graphical action for this node (as a function) */
    void action(std::function<void(PixelSurface&, float)> action);

    /** triggered by the rendering pipeline */
    bool update_node(PixelSurface& surface, float delta_time_ms);

private:
    // default lambda is an empty function
    std::function<void(PixelSurface&, float)> _action
        = [](PixelSurface&, float) {};
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_CYCLICACTION_IMPLEMENTATION

void CyclicAction::action(std::function<void(PixelSurface&, float)> action) {
    this->_action = action;
}

bool CyclicAction::update_node(PixelSurface& surface, float delta_time_ms) {
    _action(surface, delta_time_ms);
    return true;
}

#endif // X_CYCLICACTION_IMPLEMENTATION
}}
#endif // X_CYCLICACTION_HH
