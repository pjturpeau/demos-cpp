/*
 * MutexNode.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_MUTEXNODE_HH
#define X_MUTEXNODE_HH

namespace x { namespace sg {

/**
 * Ensure that only one child is visible at a time.
 * By default, all child are hidden when added to the node.
 */
class MutexNode : public Node
{
public:
    bool update_node(PixelSurface&, float);

    int visible_child();

    /**
     * Select which child to be made visible
     * @param index    use 1 for the first child.
     *                 0 means all child are set invisible.
     */
    void visible_child(int index);

    /**
     * Cycle visibility to the next children.
     * If no children has the visibility, or if the last one has the
     * visibilty, the first child become visible.
     */
    void cycle_visibility();

    /** overloaded operation to ensure that child are hidden by default */
    void add_child(Node& node);
    
private:
    int visible_index = 0;
    int number_of_child = 0;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_MUTEXNODE_IMPLEMENTATION

bool MutexNode::update_node(PixelSurface&, float) {
    return true;
}

int MutexNode::visible_child() {
    return visible_index;
}

/**
 * Select which child to be made visible
 * @param index    use 1 for the first child.
 *                 0 means all child are set invisible.
 */
void MutexNode::visible_child(int index) {
    visible_index = 0;
    int count = 0;
    Node* current = _first_child;
    while( current ) {
        count++;
        if( count == index) {
            visible_index = index;
            current->set_visible(true);
        }
        else
            current->set_visible(false);

        current = current->_next_sibling;
    }
}

/** Cycle visibility to the next children.
     * If no children has the visibility, or if the last one has the
     * visibilty, the first child become visible.
     */
void MutexNode::cycle_visibility() {
    visible_index ++;
    if(visible_index > number_of_child)
        visible_index = 1;
    visible_child(visible_index);
}

/** overloaded operation to ensure that child are hidden by default */
void MutexNode::add_child(Node& node) {
    Node::add_child(node);
    node.set_visible(false);
    number_of_child++;
}

#endif // X_MUTEXNODE_IMPLEMENTATION
}}
#endif // X_MUTEXNODE_HH
