/*
 * SceneGraph.hh
 * Copyleft (c) Pierre-Jean Turpeau 10/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_SCENEGRAPH_HH
#define X_SCENEGRAPH_HH

namespace x { namespace sg {

class SceneGraph : public ChunkyModeLoop
{
public:
    SceneGraph();
    virtual ~SceneGraph();

    void fullscreen(bool do_fullscreen);
    void resolution(int width, int height);
    void zoom(int zoom_factor);
    void title(const char * name);

    void read_command_line(int argc, char ** argv,
        std::function<void(cstrptr_t)> additional_usage = [](cstrptr_t) {});

    void set_root(Node& node);

    int width();
    int height();
    
    /** Call this to launch the scenegraph app. return average fps */
    float execute();

    /** overload the default (empty) key_down call-back */
    void on_key_down(std::function<void(SDL_Keysym& keysym)> lambda);

    // interfaces from ChunkyModeLoop =====================================

    /** call-back called by video_mode.execute() */
    void loop_callback(PixelSurface& surface, float delta_time_ms);

    /** call-back called by video_mode.execute() when a key is pressed */ 
    void key_down(SDL_Keysym& keysym); 

private:
    Node* root = nullptr;
    ChunkyModeAttributes attributes;

    // default to an empty function
    std::function<void(SDL_Keysym& keysym)>
        key_down_action = [] (SDL_Keysym& keysym) {};
};

///////////////////////////////////////////////////////////////////////////////

#ifdef X_SCENEGRAPH_IMPLEMENTATION

SceneGraph::SceneGraph() {}
SceneGraph::~SceneGraph() {}

void SceneGraph::fullscreen(bool do_fullscreen) {
    attributes.fullscreen = do_fullscreen;
}

void SceneGraph::resolution(int width, int height) {
    attributes.width = width;
    attributes.height = height;
}

void SceneGraph::zoom(int zoom_factor) {
    attributes.window_pixel_size = zoom_factor;
}

void SceneGraph::title(const char * name) {
    attributes.title(name);
}

void SceneGraph::read_command_line(int argc, char ** argv,
    std::function<void(cstrptr_t)> additional_usage)
{
    attributes.read_command_line(argc, argv, additional_usage);
}

void SceneGraph::set_root(Node& node) {
    root = &node;
}

int SceneGraph::width() {
    return attributes.width;
}

int SceneGraph::height() {
    return attributes.height;
}

/** Call this to launch the scenegraph app. return average fps */
float SceneGraph::execute() {
    assert(root != nullptr); // execute: no root node

    ChunkyMode::instance().initialize(attributes);
    return ChunkyMode::instance().execute(*this);
}

/** overload the default (empty) key_down call-back */
void SceneGraph::on_key_down(std::function<void(SDL_Keysym& keysym)> lambda) {
    key_down_action = lambda;
}

// interfaces from ChunkyModeLoop =====================================

/** call-back called by video_mode.execute() */
void SceneGraph::loop_callback(PixelSurface& surface, float delta_time_ms) {
    if( root ) {
        // TODO: if(clear_screen) surface->clear(clear_color);
        root->execute(surface, delta_time_ms);
    }
}

/** call-back called by video_mode.execute() when a key is pressed */ 
void SceneGraph::key_down(SDL_Keysym& keysym) {
    key_down_action(keysym);
}

#endif // X_SCENEGRAPH_IMPLEMENTATION
}}
#endif // X_SCENEGRAPH_HH
