/*
 * Node.hh
 * Copyleft (c) Pierre-Jean Turpeau 10/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_NODE_HH
#define X_NODE_HH

#include <x/gfx/ChunkyMode.hh>

namespace x { namespace sg {

// allows binding for immediate 32 bits values
typedef union {
    struct {
        int* from_i;
        int* to_i;
    };
    struct {
        float* from_f;
        float* to_f;
    };
} binding_def_t;

// beware, a good source of memory problems :/
struct Binding
{
    Binding(int& from, int& to);
    void update();

    binding_def_t def;
};

// Node class
// ==========

class Node
{
public:
    Node();
    virtual ~Node();

    virtual void set_visible(bool visible);

    virtual void add_sibling(Node& node);

    virtual void add_child(Node& node);

    /**
     * destination lifeycle has to be managed carefully! 
     * do not bind a source property of another instance!
     *
     * one_node.bind_to(one_node.from_property, other_node.to_property);
     */
    virtual Node& bind_to(int& from_this_property, int& to_destination);

    /**
     * It shall always return true.
     * Return false to avoid traversal of childrens
     */
    virtual bool update_node(PixelSurface& surface, float delta_time_ms)  = 0;

    /**
     * recursive deep-first left-to-right traversal.
     * It shall not be overridden.
     */
    void execute(PixelSurface& surface, float delta_time_ms);

    void execute_bindings();

    bool visible = true;

    Node* _parent = nullptr;      // TODO: might be useless
    Node* _first_child = nullptr;
    Node* _next_sibling = nullptr;

    std::vector<Binding> bindings;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_NODE_IMPLEMENTATION

// Binding class
// =============

Binding::Binding(int& from, int& to) {
    def.from_i = &from;
    def.to_i   = &to;
}

void Binding::update() {
    *(def.to_i) = *(def.from_i);
}

// Node class
// ==========

Node::Node() {}

Node::~Node() {}

void Node::set_visible(bool visible) {
    this->visible = visible;
}

void Node::add_sibling(Node& node) {
    Node* aNode = this->_next_sibling;

    if( aNode == nullptr ) {
        this->_next_sibling = &node;
    } else {  
        while( aNode->_next_sibling != nullptr )
            aNode = aNode->_next_sibling;
            
        aNode->_next_sibling = &node;
    }
    node._parent = _parent;
}

void Node::add_child(Node& node) {
    if( _first_child == nullptr )
        _first_child = &node;
    else
    {
        _first_child->add_sibling(node);
    }
}

/**
 * destination lifeycle has to be managed carefully! 
 * do not bind a source property of another instance!
 *
 * one_node.bind_to(one_node.from_property, other_node.to_property);
 */
Node& Node::bind_to(int& from_this_property, int& to_destination) {
    Binding b(from_this_property, to_destination);
    bindings.push_back(b);
    return *this;
}

/**
 * recursive deep-first left-to-right traversal.
 * It shall not be overridden.
 */
void Node::execute(PixelSurface& surface, float delta_time_ms) {
    if( visible == true ) {
        if( update_node(surface, delta_time_ms) == true ) {
            execute_bindings(); // update bound properties
            if( _first_child != nullptr )
                _first_child->execute(surface, delta_time_ms);
        }
    }
    if( _next_sibling != nullptr )
        _next_sibling->execute(surface, delta_time_ms);
}

void Node::execute_bindings() {
    for(auto &binding: bindings) {
        binding.update();
    }
}

#endif // X_NODE_IMPLEMENTATION
}}
#endif // X_NODE_HH
