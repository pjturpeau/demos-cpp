/*
 * NumberAnimation.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_NUMBERANIMATION_HH
#define X_NUMBERANIMATION_HH

#include <x/sg/anim/Animation.hh>
#include <x/math/Easing.hh>

namespace x { namespace sg {

template <typename T>
class NumberAnimation : public Animation
{
public:

    template<typename E>
    using easing_func_t = E(*)(E from, E delta, float time, float duration);

    NumberAnimation() {}
    virtual ~NumberAnimation() {}

    void from(T pfrom) { from_value = pfrom; }

    void to(T pto) { to_value = pto; }

    void duration(float duration_ms) { this->duration_ms = duration_ms; }

    void easing(easing_func_t<T> ease) {
        this->_easing_func = ease;
    }

    void start() {
        throw_assert(duration_ms != 0, "null duration (division by zero)");

        Animation::start();
        elapsed_time_ms = 0;
        value = from_value;
    }

    void loop(bool do_loop) {
        Animation::loop(do_loop);
    }

    /** bind the 'value' property. */        
    void bind_to(T& to_destination) {
        Node::bind_to(value, to_destination);
    }

    virtual bool update_node(PixelSurface& surface, float delta_time_ms) {
        if(running == true) {
            elapsed_time_ms += delta_time_ms;
            if(elapsed_time_ms >= duration_ms) {
                elapsed_time_ms = duration_ms; // clamp
                stop();
            }

            value = _easing_func(from_value, to_value - from_value, elapsed_time_ms, duration_ms);

            if( running == false && do_loop == true ) {
                start();
            }
        }
        return true;
    }

public:
    float duration_ms = 0;
    float elapsed_time_ms = 0;

    T value = 0;
    T from_value = 0;
    T to_value = 0;

private:
    easing_func_t<T> _easing_func = ease_linear<T>;
};

typedef NumberAnimation<float> FloatAnimation;
typedef NumberAnimation<int> IntegerAnimation;

}}
#endif // X_NUMBERANIMATION_HH
