/*
 * Animation.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_ANIMATION_HH
#define X_ANIMATION_HH

#include <x/sg/core/Node.hh>

namespace x { namespace sg {

class Animation : public Node
{
public:
    Animation() {}

    virtual void start() {
        running = true;
    }

    virtual void stop() {
        running = false;
    }

    bool is_running() {
        return running;
    }

    virtual void loop(bool do_loop) {
        this->do_loop = do_loop;
    }

    bool running = false;
    bool do_loop = false;
};

}}
#endif // X_ANIMATION_HH
