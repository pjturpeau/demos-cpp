/*
 * LambdaAction.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_LAMBDAACTION_HH
#define X_LAMBDAACTION_HH

#include <x/sg/anim/Animation.hh>
 
namespace x { namespace sg {

class LambdaAction : public Animation
{
public:
    LambdaAction(std::function<void()> action = []() {}) :
        action(action)
    {}

    void lambda(std::function<void()> action) {
        this->action = action;
    }

    bool update_node(PixelSurface& surface, float delta_time_ms) {
        if( running == true) {
            action();
            stop();
        }
        return true;
    }

    // default lambda is an empty function
    std::function<void()> action = []() {};
};

}}
#endif // X_LAMBDAACTION_HH
