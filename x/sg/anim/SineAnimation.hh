/*
 * SineAnimation.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_SINEANIMATION_HH
#define X_SINEANIMATION_HH

#include <x/math/Trigo.hh>

namespace x { namespace sg {

template<typename T, int BITS>
class SineAnimation : public Animation
{
public:
    /** value = center + amplitude * sin( time * time_speed ); */
    SineAnimation() {}

    void time_speed(float time_speed) {
        this->timespeed = time_speed;
    }
    
    /** bind the 'value' property. */        
    void bind_to(T& to_destination) {
        Node::bind_to(value, to_destination);
    }
    
    virtual bool update_node(PixelSurface& surface, float delta_time_ms) {
        if(running == true) {
            elapsed_time += delta_time_ms * timespeed;
            value = trigo.sin(uint32_t(elapsed_time));
            Node::execute_bindings();
        }
        return true;
    }

    Trigo<T,BITS> trigo;

private:
    T value = 0;
    T amplitude = 1;
    T center = 0;

    float elapsed_time = 0;
    float timespeed = 0.1f;

};

typedef SineAnimation<int,12> IntSineAnimation;

}}
#endif // X_NUMBERANIMATION_HH
