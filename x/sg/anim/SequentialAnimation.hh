/*
 * SequentialAnimation.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_SEQUENTIALANIMATION_HH
#define X_SEQUENTIALANIMATION_HH

#include <x/sg/core/Node.hh>
#include <x/sg/anim/Animation.hh>

namespace x { namespace sg {
    
class SequentialAnimation : public Animation
{
public:
    SequentialAnimation() : _current(nullptr) {}

    void start() {
        Animation::start();
        _current = static_cast<Animation*>(_first_child);
        if( _current != nullptr ) {
            _current->start();
            Animation* next = static_cast<Animation*>(_current->_next_sibling);
            while( next ) {
                next->stop();
                next = static_cast<Animation*>(next->_next_sibling);
            }
        }
        else
            stop();
    }

    virtual void add_child(Animation& node) {
        Node& tmp = static_cast<Node&>(node);
        Node::add_child(tmp);
    }

    virtual bool update_node(PixelSurface& surface, float delta_time_ms) {
        if( running == true ) {
            _current->update_node(surface, delta_time_ms);
            _current->execute_bindings();
            if( _current->running == false) {
                // animation has terminated, switch to the next one
                _current = static_cast<Animation*>(_current->_next_sibling);
                if( _current != nullptr ) {
                    _current->start();
                } else {
                    if( do_loop == true )
                        start();
                    else
                        stop(); // no more animation in the sequence
                }
            }
        }
        return false; // break the standard traversal
    }

private:
    void add_child(Node& node) {
        throw std::invalid_argument("You can only add node of type bbl::Animation");
    }

    Animation* _current;
};

}}
#endif // X_SEQUENTIALANIMATION_HH
