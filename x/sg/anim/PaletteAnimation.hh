/*
 * PaletteAnimation.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_PALETTEANIMATION_HH
#define X_PALETTEANIMATION_HH

#include <x/gfx/ColorPalette.hh>
 
namespace x { namespace sg {

class PaletteAnimation : public Animation
{
public:
    PaletteAnimation() {}

    void destination_palette(ColorPalette& destination) {
        this->destination = &destination;
    }

    void from_palette(ColorPalette& from) {
        this->from = &from;
    }

    void to_palette(ColorPalette& to) {
        this->to = &to;
    }
    void duration(float duration_ms) {
        this->duration_ms = duration_ms;
    }

    void start() {
        throw_assert(destination, "start: invalid 'destination' ptr");
        throw_assert(from, "start: invalid 'from' ptr");
        throw_assert(to, "start: invalid 'to' ptr");

        Animation::start();
        elapsed_time_ms = 0;
        *destination = *from;
    }

    bool update_node(PixelSurface& surface, float delta_time_ms) {
        if(running == true) {

            elapsed_time_ms += delta_time_ms;

            if(elapsed_time_ms >= duration_ms) {
                elapsed_time_ms = duration_ms; // clamp
                stop();
            }

            float factor = elapsed_time_ms / duration_ms;

            destination->interpolate(*from, *to, factor);
        }
        return true;
    }

public:
    float duration_ms       = 0;
    float elapsed_time_ms   = 0;    

    ColorPalette *destination = nullptr;
    ColorPalette *from        = nullptr;
    ColorPalette *to          = nullptr;
};

}}
#endif // X_PALETTEANIMATION_HH
