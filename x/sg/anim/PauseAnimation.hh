/*
 * PauseAnimation.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_PAUSEANIMATION_HH
#define X_PAUSEANIMATION_HH
 
namespace x { namespace sg {

class PauseAnimation : public Animation
{
public:
    PauseAnimation()
        : duration_ms(0), elapsed_time_ms(0) {}

    void start() {
        Animation::start();
        elapsed_time_ms = 0;
    }

    void duration(float milliseconds) {
        duration_ms = milliseconds;
    }

    bool update_node(PixelSurface& surface, float delta_time_ms) {
        if( running == true) {
            elapsed_time_ms += delta_time_ms;
            if(elapsed_time_ms >= duration_ms) {
                elapsed_time_ms = duration_ms; // clamp
                stop();
            }
        }
        return true;
    }

    float duration_ms;
    float elapsed_time_ms;
};

}}
#endif // X_PAUSEANIMATION_HH
