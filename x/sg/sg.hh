/*
 * sg.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_SG_SG_HH
#define X_SG_SG_HH

#ifdef X_IMPLEMENTATION

#define X_SCENEGRAPH_IMPLEMENTATION
#define X_NODE_IMPLEMENTATION
#define X_CYCLICACTION_IMPLEMENTATION
#define X_MUTEXNODE_IMPLEMENTATION

#define X_TRANSPARENTSURFACE_IMPLEMENTATION
#define X_OPAQUESURFACE_IMPLEMENTATION

#endif // X_IMPLEMENTATION

#include <x/sg/core/Node.hh>
#include <x/sg/core/SceneGraph.hh>
#include <x/sg/core/MutexNode.hh>
#include <x/sg/core/CyclicAction.hh>

#endif // X_SG_SG_HH
