/*
 * x.hh
 * Copyleft (c) Pierre-Jean Turpeau 03/2018
 * <pierrejean AT turpeau DOT net>
 *
 * the father of all things...
 *
 * use '#define X_IMPLEMENTATION = 1' before including this file in your
 * main cpp file so that the compiler has access to the source code
 * implementation.
 */
#ifndef X_X_HH
#define X_X_HH

#include <stdint.h> // uint8_t, uint16_t, uint32_t
#include <assert.h>

// from http://sol.gfxile.net/interpolation/
#define SMOOTHSTEP(x)   ((x) * (x) * (3 - 2 * (x)))
#define SMOOTHERSTEP(x) ((x) * (x) * (x) * ((x) * ((x) * 6 - 15) + 10))

// because of c++11
#ifndef M_PI
constexpr double M_PI   = 3.14159265358979323846;
#endif

#ifndef M_PI_2
constexpr double M_PI_2 = 1.57079632679489661923;
#endif

#ifndef M_TAU
constexpr double M_TAU  = 6.28318530717958647692;
#endif

typedef const char * cstrptr_t;

#define APP_LOG_ERROR(...) SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, __VA_ARGS__)
#define APP_LOG_DEBUG(...) SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, __VA_ARGS__)

// Implementation directives
// =========================

#ifdef X_IMPLEMENTATION

// gfx
#define X_BITMAP_IMPLEMENTATION
#define X_BITMAPMANAGER_IMPLEMENTATION
#define X_BITMAPFONT_IMPLEMENTATION
#define X_CHUNKYMODE_IMPLEMENTATION
#define X_CHUNKYMODEATTRIBUTES_IMPLEMENTATION
#define X_COLORPALETTE_IMPLEMENTATION
#define X_PIXELSURFACE_IMPLEMENTATION

// math
#define X_EASING_IMPLEMENTATION
#define X_FASTMATHS_IMPLEMENTATION

// fx
#define X_WHITENOISEFX_IMPLEMENTATION
#define X_ROTOZOOMFX_IMPLEMENTATION
#define X_SURFACEFX_IMPLEMENTATION
#define X_UVMAPFX_IMPLEMENTATION

// tools
#define X_CHRONO_IMPLEMENTATION

#include <x/gfx/Bitmap.hh>
#include <x/gfx/BitmapManager.hh>
#include <x/gfx/BitmapFont.hh>
#include <x/gfx/ChunkyModeAttributes.hh>
#include <x/gfx/ChunkyMode.hh>
#include <x/gfx/ColorPalette.hh>
#include <x/gfx/PixelSurface.hh>

#include <x/math/Easing.hh>
#include <x/math/fastmaths.hh>

#include <x/fx/WhiteNoiseFx.hh>
#include <x/fx/RotozoomFx.hh>

#include <x/tools/Chrono.hh>

#endif // X_IMPLEMENTATION

#endif // X_X_HH
