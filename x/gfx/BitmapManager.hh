/*
 * BitmapManager.hh
 * Copyleft (c) Pierre-Jean Turpeau 10/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_BITMAPMANAGER_HH
#define X_BITMAPMANAGER_HH

#include <map>
#include <stdexcept>

#include <ThrowAssert/ThrowAssert.hpp>

#include <x/types/sdl2.hh>

#include <x/x.hh>
#include <x/gfx/Bitmap.hh>

namespace x {

/** The BitmapManager owns the surfaces and is in charge of deleting them when
 * cleared */
class BitmapManager
{
public:
    
    /** returns a singleton */
    static BitmapManager& instance();
   
    /** deletes all loaded bitmaps */
    void clear();

    const Bitmap& load_bmp(const char *name, const uint8_t *bmp_data, int bmp_size);

    // retrieve a previously loaded bitmap
    const Bitmap& get(const char *name);

    static void save_bmp(const char *name, uint8_t *pixels, int w, int h,
        const ColorPalette& palette);

private:
    std::map<std::string, BitmapShPtr> _map;

    sdl2::SurfaceShPtr load_bmp(const uint8_t *bmp_data, int bmp_size);
    void _initialize_palette(ColorPalette& palette, const SDL_Palette *sdl_palette);
    static void _copy_palette(const ColorPalette& from, sdl2::SurfaceShPtr& surface);
};

// global aliases for the singleton
BitmapManager& Bitmaps();
const Bitmap& Bitmaps(const char *);

///////////////////////////////////////////////////////////////////////////////
#ifdef X_BITMAPMANAGER_IMPLEMENTATION

BitmapManager& Bitmaps() {
    return BitmapManager::instance();
}

const Bitmap& Bitmaps(const char* name) {
    return BitmapManager::instance().get(name);
}

/** returns a singleton */
BitmapManager& BitmapManager::instance() {
    static BitmapManager singleton;
    return singleton;
}
    
/** deletes all loaded bitmaps */
void BitmapManager::clear() {
    _map.clear();
}

const Bitmap& BitmapManager::load_bmp(const char *name, const uint8_t *bmp_data,
        int bmp_size) {
    throw_assert(_map.count(name) == 0, "load_bmp: name already used");

    sdl2::SurfaceShPtr bmp = load_bmp(bmp_data, bmp_size);

    throw_assert(SDL_ISPIXELFORMAT_INDEXED(bmp->format->format),
        "load_bmp: invalid pixel format");
    throw_assert(bmp->format->BitsPerPixel == 8,
        "load_bmp: invalid pixel format");
        
    BitmapShPtr bitmap = std::make_shared<Bitmap>();

    bitmap->surface.resize(bmp->w, bmp->h);
    memcpy(bitmap->surface.pixels, bmp->pixels, bmp->w*bmp->h);

    _initialize_palette(bitmap->palette, bmp->format->palette);

    _map.insert(
        std::pair<std::string,BitmapShPtr>(name, bitmap)
    );

    return *(bitmap);
}

// retrieve a previously loaded bitmap
const Bitmap& BitmapManager::get(const char *name) {
    assert( _map.count(name) ); // bitmap not found

    std::map<std::string, BitmapShPtr>::iterator it = _map.find(name);
    if( it != _map.end() )
        return *(it->second);
    else
        throw std::invalid_argument("bitmap not found");
}

// load a new bitmap from buffered data
sdl2::SurfaceShPtr BitmapManager::load_bmp(const uint8_t *bmp_data, int bmp_size) {
    sdl2::RWopsPtr rw(SDL_RWFromConstMem((void *)bmp_data, bmp_size));
    throw_assert(rw, "load_bmp: SDL_RWFromConstMem:" << SDL_GetError());

    // we suppose of a 8-bit BMP, no convert yet (to be studied)
    sdl2::SurfaceShPtr surface(SDL_LoadBMP_RW(rw.get(), 1));
    throw_assert(surface, "load_bmp: SDL_LoadBMP_RW:" << SDL_GetError());

    APP_LOG_DEBUG("BMP Loaded: w=%d h=%d "\
        "pitch=%d indexed=%d bits/p=%d bytes/p=%d",
        surface->w,
        surface->h,
        surface->pitch,
        SDL_ISPIXELFORMAT_INDEXED(surface->format->format),
        surface->format->BitsPerPixel,
        surface->format->BytesPerPixel);

    return surface;
}

void BitmapManager::save_bmp(const char *name, uint8_t *pixels, int w, int h,
        const ColorPalette& palette) {
    sdl2::SurfaceShPtr surface(SDL_CreateRGBSurfaceFrom((void*)pixels, w, h, 8, w, 0, 0, 0, 0));

    if( !surface )
        APP_LOG_ERROR("save_bmp: SDL_CreateRGBSurfaceFrom: %s", SDL_GetError());
    else {
        _copy_palette(palette, surface);

        if( SDL_SaveBMP(surface.get(), name) )
            APP_LOG_ERROR("save_bmp: SDL_SaveBMP: %s", SDL_GetError());
    }
}

void BitmapManager::_initialize_palette(ColorPalette& palette, const SDL_Palette *sdl_palette) {
    throw_assert(sdl_palette, "_initialize_palette: invalid sdl_palette");
    for(int i = 0; i < sdl_palette->ncolors; i++) {
        palette.set_color( i, sdl_palette->colors[i].r,
            sdl_palette->colors[i].g, sdl_palette->colors[i].b);
    }
}

    // copy the palette to the surface
void BitmapManager::_copy_palette(const ColorPalette& from,
                                    sdl2::SurfaceShPtr& surface) {
        if( SDL_ISPIXELFORMAT_INDEXED(surface->format->format)
        && surface->format->BitsPerPixel == 8 )
    {
        SDL_Palette* palette = surface->format->palette;

        for(int i = 0; i < palette->ncolors; i++) {
            palette->colors[i].r = from.colors[i].r << 3;
            palette->colors[i].g = from.colors[i].g << 2;
            palette->colors[i].b = from.colors[i].b << 3;
            palette->colors[i].a = 0xFF;
        }
    }
}

#endif // X_BITMAPMANAGER_IMPLEMENTATION
}
#endif // X_BITMAPMANAGER_HH
