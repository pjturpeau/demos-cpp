/*
 * Bitmap.hh
 * Copyleft (c) Pierre-Jean Turpeau 03/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_BITMAP_HH
#define X_BITMAP_HH

#include <map>
#include <stdexcept>

#include <x/x.hh>
#include <x/gfx/ColorPalette.hh>
#include <x/gfx/PixelSurface.hh>

namespace x {

class Bitmap {
public:
    Bitmap();
    Bitmap(const Bitmap& other);
    Bitmap& operator=(const Bitmap& other);
    operator const PixelSurface& () const; 

    PixelSurface surface;
    ColorPalette palette;
};

using BitmapShPtr = std::shared_ptr<Bitmap>;

///////////////////////////////////////////////////////////////////////////////
#ifdef X_BITMAP_IMPLEMENTATION

Bitmap::Bitmap() {}

Bitmap::Bitmap(const Bitmap& other) {
    *this = other;
}

Bitmap& Bitmap::operator=(const Bitmap& other) {
    surface = other.surface;
    palette = other.palette;
    return *this;
}

Bitmap::operator const PixelSurface& () const {
    return this->surface;
}

#endif // X_BITMAP_IMPLEMENTATION
}
#endif // X_BITMAP_HH
