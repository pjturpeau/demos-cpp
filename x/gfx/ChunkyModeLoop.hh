/*
 * ChunkyModeLoop.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef BBL_GFX_CHUNKYMODELOOP_HH
#define BBL_GFX_CHUNKYMODELOOP_HH

#include <x/x.hh>

namespace x {

/** Abstract class used to execute the main loop of a video mode */
class ChunkyModeLoop
{
public:
    virtual ~ChunkyModeLoop() {}

    virtual void key_down(SDL_Keysym& keysym) {}

    /** The delta time parameter allows to update behavior according to the
      * time elapsed between two frames.
      * The video_mode backbuffer can be cleared through the clear()
      * operation */
    virtual void loop_callback(PixelSurface& surface, float delta_time_ms) = 0;
};

}
#endif // BBL_GFX_CHUNKYMODELOOP_HH
