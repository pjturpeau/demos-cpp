/*
 * PixelSurface.hh
 * Copyleft (c) Pierre-Jean Turpeau 10/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef BBL_GFX_PIXELSURFACE_HH
#define BBL_GFX_PIXELSURFACE_HH

#include <assert.h>
#include <stdint.h>

// TODO: remove from here (SDL_Rect)
#include <x/types/sdl2.hh>

#include <x/gfx/ColorPalette.hh>

namespace x {

class PixelSurface
{
public: 
    PixelSurface();
    PixelSurface(int width, int height);
    PixelSurface(const PixelSurface& other);

    virtual ~PixelSurface();

    PixelSurface& operator=(const PixelSurface& other);

    /** TODO: check the generated code */
    uint8_t& operator[](uint32_t index);

    void clear(uint8_t color = 0);
    void resize(int width, int height);

    /** no boundaries checking */
    void set(int32_t x, int32_t y, uint8_t color);

    /** no boundaries checking */
    uint8_t get(int x, int y) const;

    /** with boundaries checking */
    void set_safe(int x, int y, uint8_t color);

    void draw_rectangle(int x1, int y1, int x2, int y2, uint8_t color);
    void draw_line(int x1, int y1, int x2, int y2, uint8_t color);

    /** simple helper function to display the color palette */
    void draw_palette(ColorPalette& palette);

    void copy_to(PixelSurface& destination, int x, int y,
                    uint8_t transparent_index) const;

    void copy_to(PixelSurface& destination, int x, int y) const;

    void copy_area_to(
            PixelSurface& destination, int x, int y, SDL_Rect& from,
            uint8_t transparent_index) const;

    void vertical_mirror();
    void horizontal_mirror();

    int w  = 0;
    int h  = 0;
    uint8_t *pixels = nullptr;        
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_PIXELSURFACE_IMPLEMENTATION

PixelSurface::PixelSurface() {}

PixelSurface::~PixelSurface() {
    if( pixels )
        delete[] pixels;
}

PixelSurface::PixelSurface(int width, int height) {
    resize(width, height);
}

PixelSurface::PixelSurface(const PixelSurface& other) {
    *this = other;
}

PixelSurface& PixelSurface::operator=(const PixelSurface& other) {
    resize(other.w, other.h);
    memcpy(pixels, other.pixels, w * h * sizeof(uint8_t));
    return *this;
}

/** TODO: check the generated code */
uint8_t& PixelSurface::operator[](uint32_t index) {
    assert(index < w * h);
    return pixels[index];
}

void PixelSurface::clear(uint8_t color) {
    uint32_t c = (color << 24) ^ (color << 16) ^ (color << 8) ^ color;
    SDL_memset4( pixels, c, (w * h * sizeof(uint8_t))>>2);
}

void PixelSurface::resize(int width, int height) {
    // invalid surface size or not a multiple of 4
    assert( (width * height > 0) && ((width * height) % 4 == 0));

    w = width;
    h = height;

    if(pixels) delete[] pixels;

    pixels = new uint8_t[w * h * sizeof(uint8_t)];
    throw_assert(pixels, "pixels allocation error");
}

void PixelSurface::set_safe(int32_t x, int32_t y, uint8_t color) {
    if( x >= 0 && y >= 0 && x < w && y < h )
        pixels[ y * w + x ] = color;
}

void PixelSurface::set(int x, int y, uint8_t color) {
    assert( x >= 0 && y >= 0 && x < w && y < h);
    pixels[ y * w + x ] = color;
}

uint8_t PixelSurface::get(int x, int y) const {
    assert( x >= 0 && y >= 0 && x < w && y < h);
    return pixels[y * w + x ];
}

void PixelSurface::draw_rectangle(int x1, int y1, int x2, int y2, uint8_t color) {
    // TODO: clip to surface
    int s;
    if( x2 < x1 ) { s=x1; x1=x2; x2=s; }
    if( y2 < y1 ) { s=y1; y1=y2; y2=s; }
    for(int x = x1; x < x2; x++) {
        for(int y = y1; y < y2; y++) {
            set_safe(x, y, color);
        }
    }
}

void PixelSurface::draw_line(int x1, int y1, int x2, int y2, uint8_t color) {
    int dx =  std::abs (x2 - x1), sx = x1 < x2 ? 1 : -1;
    int dy = -std::abs (y2 - y1), sy = y1 < y2 ? 1 : -1;
    int err = dx + dy; /* error value e_xy */

    for (;;) {  /* loop */
        set_safe(x1, y1, color);
        if (x1 == x2 && y1 == y2) break;
        int e2 = 2 * err;
        if (e2 >= dy) { err += dy; x1 += sx; } /* e_xy+e_x > 0 */
        if (e2 <= dx) { err += dx; y1 += sy; } /* e_xy+e_y < 0 */
    }
}

/** simple helper function to display the color palette */
void PixelSurface::draw_palette(ColorPalette& palette) {
    constexpr int ssize = 8;

    for(int i = 0; i < ColorPalette::SIZE; i++)
    {
        int x = (i * ssize) % w;
        int y = (i * ssize) / w * ssize;
        draw_rectangle(x, y, x + ssize - 1, y + ssize - 1, i);
    }
}

void PixelSurface::copy_area_to(
            PixelSurface& destination, int x, int y, SDL_Rect& from,
            uint8_t transparent_index) const {
    uint8_t* d = destination.pixels + x + destination.w * y;
    uint8_t* s = pixels + from.x + w * from.y;

    for( int j = 0; j < from.h; j++ ) {
        uint8_t* src = s;
        for(int i = 0; i < from.w; i++ ) {
            uint8_t col = *(src++);
            if( col != transparent_index )
                d[i] = col;
        }
        s += w;
        d += destination.w;
    }
}

// TODO: manage clipping !!!!
void PixelSurface::copy_to(PixelSurface& destination, int x, int y,
                            uint8_t transparent_index) const {
    uint8_t* d = destination.pixels + x + destination.w * y;
    uint8_t* s = pixels;

    for( int j = 0; j < h; j++ ) {
        for(int i = 0; i < w; i++ ) {
            uint8_t col = *(s++);
            if( col != transparent_index )
                d[i] = col;
        }
        d += destination.w;
    }
}

// TODO: manage clipping and 
void PixelSurface::copy_to(PixelSurface& destination, int x, int y) const {
    if( x == 0 && y == 0 && destination.w == w && destination.h == h ) {
        SDL_memcpy(destination.pixels, pixels, w*h*sizeof(uint8_t));
    } else if( x >= 0 && x+w <= destination.w && y >= 0 && y+h <= destination.h) {
        uint8_t* dest_pixs = destination.pixels + x + y * destination.w;
        uint8_t* src_pixs = pixels;

        for( int j = 0; j < h; j++ ) {
            SDL_memcpy(dest_pixs, src_pixs, w);
            src_pixs += w;
            dest_pixs += destination.w;
        }
    }
}

void PixelSurface::vertical_mirror() {
    if( pixels ) {
        uint8_t *pix = pixels;
        for(int j = 0; j < h; j++) {
            for(int i = 0; i < w/2; i++) {
                uint8_t tmp = pix[w - 1 - i];
                pix[w - 1 - i] = pix[i];
                pix[i] = tmp; 
            }
            pix += w;
        }
    }
}

void PixelSurface::horizontal_mirror() {
    if( pixels ) {
        uint8_t *pix = pixels;
        uint8_t *pix2 = pixels + (h -1) * w;
        for(int j = 0; j < h/2; j++) {
            for(int i = 0; i < w; i++) {
                uint8_t tmp = pix2[i];
                pix2[i] = pix[i];
                pix[i] = tmp; 
            }
            pix += w;
            pix2 -= w;
        }
    }
}

#endif // X_PIXELSURFACE_IMPLEMENTATION
} // bbl::
#endif // PIXELSURFACE_HH
