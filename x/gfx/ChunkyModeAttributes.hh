/*
 * ChunkyModeAttributes.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_CHUNKYMODEATTRIBUTES_HH
#define X_CHUNKYMODEATTRIBUTES_HH

#include <cstring>
#include <iostream>
#include <functional>

#include <libgen.h>

#include <x/x.hh>

namespace x {

class ChunkyModeAttributes
{
public:
    /**
     * -nosync to disable synchronization on vertical refresh
     * -fullscreen to force fullscreen display  
     * -window to force window display
     * 
     * the additional_usage parameter allows to display additional usage
     * informations.
     *
     * exit program if usage is displayed (-h)
     */
    ChunkyModeAttributes();

    ChunkyModeAttributes(const ChunkyModeAttributes& copy);

    void title(const char * name);
    void usage(char const* const* argv);
    void resolution(int w, int h);

    void read_command_line(int argc, char const* const* argv,
        std::function<void(cstrptr_t)>
            additional_usage = [](cstrptr_t) {});

    int width  = 320;
    int height = 240;
    bool fullscreen = false;    // start in window mode
    bool with_vsync = true;     // use vsync
    int window_pixel_size = 2;
    SDL_LogPriority log_priority = SDL_LOG_PRIORITY_INFO;

    static const int TITLE_SIZE = 256;
    char title_str[TITLE_SIZE];

private:
    std::string _basename(const char *file_path) {
        std::string tmp_path(file_path);
        return tmp_path.substr(tmp_path.find_last_of("\\/") + 1);
    }

    std::function<void(cstrptr_t)>
        _additional_usage = [](cstrptr_t) {};
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_CHUNKYMODEATTRIBUTES_IMPLEMENTATION 

ChunkyModeAttributes::ChunkyModeAttributes() {
    title("-empty-");
}

ChunkyModeAttributes::ChunkyModeAttributes(const ChunkyModeAttributes& copy) {
    width = copy.width;
    height = copy.height;
    fullscreen = copy.fullscreen;
    with_vsync = copy.with_vsync;
    window_pixel_size = copy.window_pixel_size;
    log_priority = copy.log_priority;
    ::memcpy(title_str, copy.title_str, TITLE_SIZE);
}

void ChunkyModeAttributes::title(const char * name) {
    ::strncpy(title_str, name, TITLE_SIZE);
    title_str[TITLE_SIZE-1] = '\0';
}

void ChunkyModeAttributes::usage(char const* const* argv) {
    SDL_Log("ChunkyMode options:");
    SDL_Log("\t-novsync      disable synchronization to vertical retrace");
    SDL_Log("\t-window       force windowing display");
    SDL_Log("\t-zoom <n>     window mode pixel size (default: 2)");
    SDL_Log("\t-fullscreen   force fullscreen display");
    SDL_Log("\t-debug        force debug level infos\n\n");

    SDL_Log("ChunkyMode key actions:");
    SDL_Log("\t[c]                to display the color map");
    SDL_Log("\t[p]                to pause the animation");
    SDL_Log("\t[ESC]              to exit");
    SDL_Log("\t[ALT] + [RETURN]   to switch between window/fullscreen\n\n");

    char prog[256];
    ::strncpy(prog, argv[0], 255);
    _additional_usage(basename(prog));

    SDL_Quit();
    exit(0);
}

void ChunkyModeAttributes::resolution(int w, int h) {
    width = w;
    height = h;
}

void ChunkyModeAttributes::read_command_line(int argc, char const* const* argv,
    std::function<void(cstrptr_t)> additional_usage)
{
    _additional_usage = additional_usage;
    for(int i = 0; i < argc; i++)
    {
        std::string str( argv[i] );

        if( ! str.compare("-novsync") )
        {
            APP_LOG_DEBUG("ChunkyMode: no vsync");
            with_vsync = false;
        }
        else if ( ! str.compare("-window") )
        {
            APP_LOG_DEBUG("ChunkyMode: window mode");
            fullscreen = false;
        }
        else if ( ! str.compare("-debug") ) {
            APP_LOG_DEBUG("ChunkyMode: window mode");
            log_priority = SDL_LOG_PRIORITY_DEBUG;
        }
        else if ( ! str.compare("-zoom") )
        {
            if( i < argc-1 )
            {
                try {
                    std::string valstr(argv[i+1]);
                    window_pixel_size = stoi(valstr);
                    APP_LOG_DEBUG("ChunkyMode: set window mode pixel size" \
                        " to %d", window_pixel_size);
                } catch(...) {
                    APP_LOG_ERROR("invalid argument: not a number or out of range");
                }
            }
            else
            {
                APP_LOG_ERROR("-zoom: missing argument");;
            }
        }
        else if ( ! str.compare("-fullscreen") )
        {
            APP_LOG_DEBUG("ChunkyMode: fullscreen mode");
            fullscreen = true;
        }
        else if (  ! str.compare("-h")
                || ! str.compare("-?")
                || ! str.compare("-help")
                || ! str.compare("--help") ) {
            usage(argv);
        }
    }

    SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, log_priority);
}

#endif // X_CHUNKYMODEATTRIBUTES_IMPLEMENTATION
}
#endif // X_CHUNKYMODEATTRIBUTES_HH
