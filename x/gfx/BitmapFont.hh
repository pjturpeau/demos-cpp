/*
 * BitmapFont.hh
 * Copyleft (c) Pierre-Jean Turpeau 10/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_BITMAPFONT_HH
#define X_BITMAPFONT_HH

#include <x/x.hh>
#include <x/gfx/PixelSurface.hh>

namespace x {

// character descriptor
typedef struct
{
    int x;
    int y;
    int w;
    int h;
}
char_def_t;

/**
 * A bitmap font is created from a bitmap in which several pixel content help to
 * give the details of the font:
 *  - the first pixel value gives the transparent background color
 *  - the second pixel value gives the cells width
 *  - the thrid pixel value gives the cells height
 *  - the fourth pixel value gives the ASCII code of the first cell the bitmap
 */
class BitmapFont
{
public:
    BitmapFont();

    void spacing(int pixels);
    void font(const Bitmap& font);
    void draw_text(PixelSurface& to, int x, int y, const char* str);
    void draw_char(PixelSurface& to, int x, int y, char_def_t& def);

private:
    static constexpr int NB_CHARS = 256;

    const Bitmap* _font;
    int _line_height;
    int _space_width;
    int _spacing = 1;
    char_def_t _chars[NB_CHARS];
    uint8_t _bg_color;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_BITMAPFONT_IMPLEMENTATION

BitmapFont::BitmapFont() {}

void BitmapFont::spacing(int pixels) {
    _spacing = pixels;
}

/**
 * Configure the instance using the given bitmap:
 *  - precompute all characters position and size in the bitmap
 *  - compute the line height
 *  - compute the space width
 */
void BitmapFont::font(const Bitmap& font) {
    this->_font = &font;

    SDL_memset(_chars, 0, NB_CHARS * sizeof(char_def_t));

    // first pixel is background color
    _bg_color = font.surface.get(0, 0);

    // second pixel is cell width
    int cell_w = font.surface.get(1, 0);
    assert(cell_w);

    // third pixel is cell height
    int cell_h = font.surface.get(2, 0);
    assert(cell_h);

    // fourth pixel is first ascii code
    int start_char = font.surface.get(3, 0);

    int cell_cols = font.surface.w / cell_w;
    int cell_rows = font.surface.h / cell_h;

    int top_line = cell_h;
    int base_line = 0;

    int cur_char = start_char;

    for(int j = 0; j < cell_rows; j++) {
        for(int i = 0; i < cell_cols; i++) {
            
            // setup initial char pos and size 
            _chars[cur_char].x = i * cell_w;
            _chars[cur_char].y = j * cell_h;
            _chars[cur_char].w = cell_w;
            _chars[cur_char].h = cell_h;

            // compute the char left position
            for(int x = 0; x < cell_w; x++) {
                for(int y = 0; y < cell_h; y++) {

                    uint8_t col =
                        font.surface.get(i * cell_w + x, j * cell_h + y);
                    
                    if( col != _bg_color ) {
                        _chars[cur_char].x += x; // store the position
                        x = cell_w; // break the outer loop
                        break; // break the inner loop
                    }
                }
            }

            // compute the char right limit (width)
            for(int x = cell_w - 1; x > 0; x--) {
                for(int y = 0; y < cell_h; y++) {
                    int px = i * cell_w + x;
                    uint8_t col =
                        font.surface.get(px, j * cell_h + y);
                    
                    if( col != _bg_color ) {
                        _chars[cur_char].w = px + 1 - _chars[cur_char].x;
                        x = -1; // break the outer loop
                        break; // break the inner loop
                    }
                }
            }

            // compute top line for all chars
            for( int y = 0; y < cell_h; y++ ) {
                for( int x = 0; x < cell_w; x++ ) {

                    uint8_t col =
                        font.surface.get(i * cell_w + x, j * cell_h + y);

                    if( col != _bg_color ) {
                        if(y < top_line)
                            top_line = y;
                        y = cell_h;
                        break;
                    }
                }
            }

            // compute the base line
            if( cur_char == 'Z' ) {
                for(int y = cell_h - 1; y >= 0; y--) {
                    for(int x = 0; x < cell_w; x++) {

                        uint8_t col =
                            font.surface.get(i * cell_w + x, j * cell_h + y);

                        if( col != _bg_color ) {
                            base_line = y;
                            y = -1;
                            break;
                        }
                    }
                }
            }

            cur_char++;
            
            // when the first ascii cell in the bitmap is not 0
            if( cur_char >= 256 ) {
                j = cell_rows; // break the outerloop
                break; // break the innerloop
            }
        }
    }

    _space_width = cell_w >> 1;
    _line_height = base_line - top_line + 1;

    APP_LOG_DEBUG("font: bg_color=%d, w=%d, h=%d, start_ascii=0x%X",
        _bg_color, cell_w, cell_h, start_char);
    APP_LOG_DEBUG("font: space width=%d, baseline=%d, topline=%d, line height=%d",
        _space_width, base_line, top_line, _line_height);

    // squeeze useless top pixels
    for( int i = 0; i < NB_CHARS; ++i ) {
        _chars[i].y += top_line;
        _chars[i].h -= top_line;
    }
}

/** Draw a transparent multi-line (\n) string */ 
void BitmapFont::draw_text(PixelSurface& to, int x, int y, const char* str) {
    int draw_x = x;
    int draw_y = y;

    for( int c = *str; c != '\0'; c = *(++str)) {

        switch(c) {
            case ' ':
                draw_x += _space_width;
                break;
            case '\n':
                draw_x = x;
                draw_y += _line_height + _spacing;
                break;
            default:
                char_def_t& def = _chars[c];
                draw_char(to, draw_x, draw_y, def);
                draw_x += def.w + _spacing;
                break;
        }
    }
}

/** draw a transparent character */
void BitmapFont::draw_char(PixelSurface& to, int x, int y, char_def_t& def) {
    SDL_Rect r;
    r.x = def.x;
    r.y = def.y;
    r.w = def.w;
    r.h = def.h;
   _font->surface.copy_area_to(to, x, y, r, _bg_color);
}

#endif // X_BITMAPFONT_IMPLEMENTATION
}
#endif // X_BITMAPFONT_HH
