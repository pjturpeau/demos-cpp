/*
 * ColorPalette.hh
 * Copyleft (c) Pierre-Jean Turpeau 10/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_COLORPALETTE_HH
#define X_COLORPALETTE_HH

#include <vector>
#include <iostream>

#include <ThrowAssert/ThrowAssert.hpp>

#include <x/x.hh>
#include <x/types/rgb.hh>

namespace x {

class ColorPalette
{
public:
    // default to pink
    ColorPalette(uint32_t color = 0xFF00DE);
    ColorPalette(uint8_t from_index, uint8_t to_index,
                    uint32_t from_color, uint32_t to_color);

    ColorPalette(const ColorPalette &other);

    ColorPalette& operator=(const ColorPalette& other);

    /** activate the palette in the video */
    void activate() const;
    
    /** clear to default pink color */
    void clear(uint32_t color = 0xFF00DE);

    void set_color(int color_index, uint32_t rgb_color);

    /** rgb as [0..255] values */
    void set_color(int color_index, int r, int g, int b);

    /** rgb as [0..1] values */
    void set_color(int color_index, float r, float g, float b);

    /** Takes the first nb_useful_colors and darken them to black
         * according to the palette size and remaining space.
         * It can be used for shadows. */
    void darken_to_black(int nb_useful_colors);

    /** Generate gradient. The alpha component in input colors is not used */ 
    void linear_gradient( uint8_t from_index, uint32_t from_color,
        uint8_t to_index, uint32_t to_color);

    static const ColorPalette& black_to_white();

    /** The resulting palette is an interpolation between from and to
         * according to the given factor */ 
    void interpolate(const ColorPalette& from, const ColorPalette& to, float factor);

    static constexpr int SIZE = 256;

    rgb16_t colors[SIZE];

private: //================================================================

    static ColorPalette black_to_white_gradient;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_COLORPALETTE_IMPLEMENTATION

ColorPalette ColorPalette::black_to_white_gradient = [] {
    ColorPalette palette;
    palette.linear_gradient(0, 0x000000, 255, 0xFFFFFF);
    return palette;
}();

const ColorPalette& ColorPalette::black_to_white() {
    return ColorPalette::black_to_white_gradient;
}

ColorPalette::ColorPalette(uint32_t color) {
    clear(color);
}

ColorPalette::ColorPalette(
    uint8_t from_index, uint8_t to_index,
    uint32_t from_color, uint32_t to_color )
{
    linear_gradient(from_index, to_index, from_color, to_color);
}

ColorPalette::ColorPalette(const ColorPalette &other) {
    *this = other;
}

ColorPalette& ColorPalette::operator=(const ColorPalette& other) {
    if( this != &other ) {
        memcpy(colors, other.colors, SIZE * sizeof(rgb16_t));
    }
    return *this;
}

// forward declarations
class ColorPalette;
extern void set_video_palette(const ColorPalette& palette);

void ColorPalette::activate() const {
    set_video_palette(*this);
}

void ColorPalette::clear(uint32_t color) {
    for( int i = 0; i < SIZE; i++)
        set_color(i, color);     
}

void ColorPalette::set_color(int color_index, uint32_t rgb_color) {
    assert(color_index >= 0 && color_index < SIZE );

    argb32_t argb;
    argb.value = rgb_color;

    colors[color_index].r = (argb.r >> 3);
    colors[color_index].g = (argb.g >> 2);
    colors[color_index].b = (argb.b >> 3);
}

/** rgb as [0..255] values */
void ColorPalette::set_color(int color_index, int r, int g, int b) {
    assert(color_index >= 0 && color_index < SIZE );
    
    colors[color_index].r = (r >> 3);
    colors[color_index].g = (g >> 2);
    colors[color_index].b = (b >> 3);
}

/** rgb as [0..1] float values */ 
void ColorPalette::set_color(int color_index, float r, float g, float b) {
    assert(color_index >= 0 && color_index < SIZE );

    colors[color_index].r = r * 31;
    colors[color_index].g = g * 63;
    colors[color_index].b = b * 31;
}

/** Takes the first nb_usefull_colors and darken them to black
    * according to the palette size and remaining space */
void ColorPalette::darken_to_black(int nb_usefull_colors) {
    // nb_color shall be a multiple of the palette size
    assert( (SIZE % nb_usefull_colors) == 0);

    int gradient_steps = SIZE / nb_usefull_colors;

    for(int i = 1; i < gradient_steps; i++) {
        for( int j = 0; j < nb_usefull_colors; j++) {
            float factor = float(gradient_steps - i) / float(gradient_steps);
            colors[i*nb_usefull_colors+j].r = colors[j].r * factor;
            colors[i*nb_usefull_colors+j].g = colors[j].g * factor;
            colors[i*nb_usefull_colors+j].b = colors[j].b * factor;
        }
    }
}

/** Generate gradient. The alpha component in input colors is not used */ 
void ColorPalette::linear_gradient( uint8_t from_index, uint32_t from_color,
    uint8_t to_index, uint32_t to_color)
{
    // linear_gradient: invalid from/to indexes (>=SIZE)
    assert(from_index < SIZE && to_index < SIZE);

    argb32_t from, to;

    if( from_index < to_index ) {
        from.value = from_color;
        to.value = to_color;
    } else {
        from.value = to_color;
        to.value = from_color;
        uint8_t tmp = from_index;
        from_index = to_index;
        to_index = tmp;
    }

    float dist = std::abs(to_index - from_index);
    throw_assert(dist, "linear_gradient: invalid from/to indexes (dist==0)");

    int incr = from_index < to_index ? 1 : -1;
    for(int i = from_index; i <= to_index; i += incr ) {
        set_color( i, 
            from.r + (uint8_t)((float) ((i - from_index) * (to.r - from.r)) / dist ), 
            from.g + (uint8_t)((float) ((i - from_index) * (to.g - from.g)) / dist ),
            from.b + (uint8_t)((float) ((i - from_index) * (to.b - from.b)) / dist )
        );
    }
}

/** The resulting palette is an interpolation between from and to
    * according to the given factor */ 
void ColorPalette::interpolate(const ColorPalette& from, const ColorPalette& to, float factor) {
    for(int i = 0; i < ColorPalette::SIZE; i++) {
        int r = (int)from.colors[i].r
            + ((int)to.colors[i].r
                - (int)from.colors[i].r) * factor;
        int g = (int)from.colors[i].g
            + ((int)to.colors[i].g
                - (int)from.colors[i].g) * factor;
        int b = (int)from.colors[i].b
            + ((int)to.colors[i].b
                - (int)from.colors[i].b) * factor;

        colors[i].r = r & 0x1F;
        colors[i].g = g & 0x3F;
        colors[i].b = b & 0x1F;
    }
}

#endif // X_COLORPALETTE_IMPLEMENTATION
}
#endif // X_COLORPALETTE_HH
