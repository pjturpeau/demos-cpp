/*
 * ChunkyMode.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2017
 * <pierrejean AT turpeau DOT net>
 *
 * Simple chunky mode class with:
 *  - fullscreen switch support
 *  - basic palette management
 *  - 8BPP BMP loading
 */
#ifndef X_CHUNKYMODE_HH
#define X_CHUNKYMODE_HH

// #include <string>
// #include <iostream>
// #include <functional>

#include <x/types/sdl2.hh>

#include <x/x.hh>
#include <x/gfx/ColorPalette.hh>
#include <x/gfx/PixelSurface.hh>
#include <x/gfx/ChunkyModeLoop.hh>
#include <x/gfx/ChunkyModeAttributes.hh>
#include <x/tools/Chrono.hh>

namespace x {

class ChunkyMode
{
public:    
    static ChunkyMode& instance();

    // TODO: to be removed
    ChunkyMode( int width, int height, const char* title,
        ChunkyModeAttributes& attributes);

    virtual ~ChunkyMode();

    /** initialize before execute */
    void initialize(ChunkyModeAttributes& attributes);

    /** return the average FPS */
    float execute(ChunkyModeLoop& main_loop);

    void set_window_title(const char* title);

    // public data
    // ===========

    PixelSurface    backbuffer;
    ColorPalette    palette;
    
    ChunkyModeAttributes attributes;

private:
    ChunkyMode() {}
    ChunkyMode& operator= (const ChunkyMode&)   = delete;
    ChunkyMode (const ChunkyMode&)              = delete;

    void _update_video();
    void _blit_screen16(PixelSurface& source, uint16_t* destination);
    int _toggle_fullscreen();

    SDL_Window*   _window = nullptr;
    SDL_Renderer* _renderer = nullptr;
    SDL_Texture*  _screen_texture = nullptr;
};

/** alternative helper to access the singleton */
ChunkyMode& Video();    

///////////////////////////////////////////////////////////////////////////////
#ifdef X_CHUNKYMODE_IMPLEMENTATION

#define CHUNKYMODE_REFRESH_RATE     10
#define CHUNKYMODE_REFRESH_PERIOD   (1000.f/CHUNKYMODE_REFRESH_RATE)

ChunkyMode& ChunkyMode::instance() {
        static ChunkyMode singleton;
        return singleton;
}

ChunkyMode::ChunkyMode(int width, int height, const char* title,
    ChunkyModeAttributes& attributes)
{
    attributes.width = width;
    attributes.height = height;
    attributes.title(title);
    initialize(attributes);
}

ChunkyMode::~ChunkyMode() {
    SDL_DestroyTexture(_screen_texture);
    SDL_DestroyRenderer(_renderer);
    SDL_DestroyWindow(_window);
}

void ChunkyMode::initialize(ChunkyModeAttributes& attribs) 
{
    attributes = attribs;

    // this is because of the SDL_memset4 use in the clear_video() operation
    if( attributes.width * attributes.height % 4 ) {
        APP_LOG_ERROR("ChunkyMode: video mode size has to be a multiple of 4");
        SDL_Quit();
        exit(1);
    }

    backbuffer.resize(attributes.width, attributes.height);

    if( SDL_Init(SDL_INIT_VIDEO /*| SDL_INIT_TIMER | SDL_INIT_EVENTS*/
        | SDL_INIT_AUDIO) != 0)
    {
        APP_LOG_ERROR("ChunkyMode: Unable to initialize SDL: %s", SDL_GetError());
        SDL_Quit();
        exit(1);
    }

    uint32_t flags = SDL_WINDOW_SHOWN;

    if( attributes.fullscreen )
    {
        flags |= SDL_WINDOW_FULLSCREEN;
        _window = SDL_CreateWindow( attributes.title_str,
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 0, 0, flags);
        SDL_ShowCursor(SDL_DISABLE);
    }
    else
    {
        _window = SDL_CreateWindow( attributes.title_str,
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            attributes.width  * attributes.window_pixel_size,
            attributes.height * attributes.window_pixel_size, flags);
        SDL_ShowCursor(SDL_ENABLE);
    }

    flags = SDL_RENDERER_ACCELERATED;
    if( attributes.with_vsync )
        flags |= SDL_RENDERER_PRESENTVSYNC;

    _renderer = SDL_CreateRenderer(_window, -1, flags);
    _screen_texture = SDL_CreateTexture(_renderer, SDL_PIXELFORMAT_RGB565,
        SDL_TEXTUREACCESS_STREAMING, attributes.width, attributes.height);

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
    SDL_RenderSetLogicalSize(_renderer, attributes.width, attributes.height);

    backbuffer.clear();

    APP_LOG_DEBUG("ChunkyMode: resolution is %dx%d", attributes.width, attributes.height);
}

// time management inspired by:
// http://devcry.heiho.net/html/2015/20150211-rock-solid-frame-rates.html
//
// for precise physics in games, you should move to:
// https://gafferongames.com/post/fix_your_timestep/
//
float ChunkyMode::execute(ChunkyModeLoop& main_loop) {
    SDL_Event event;
    char done = 0;
    char palette = 0;
    char pause = 0;

    Chrono outer_time, inner_time;

    int number_of_frames = 0;

    while (!done)
    {
        number_of_frames++;

        if (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_ESCAPE:
                            done = 1;
                            break;

                        case SDLK_RETURN:
                            if (event.key.keysym.mod & KMOD_ALT) {
                                // wait for current callback to stop
                                SDL_Delay(CHUNKYMODE_REFRESH_PERIOD);
                                _toggle_fullscreen();
                            }
                            break;

                        // show color palette
                        case SDLK_c:
                            palette = ~palette;
                            if( !palette )
                                backbuffer.clear();
                            break;

                        // pause
                        case SDLK_p:
                            pause = ~pause;
                            if( !pause )
                                inner_time.restart();
                            break;

                        // delegate
                        default:
                            main_loop.key_down(event.key.keysym);
                            break;
                    }
                    break;

                case SDL_QUIT:
                    done = 1;
                    break;

            }
        }

        if(!pause) {
            main_loop.loop_callback( backbuffer, 
                inner_time.elapsed_ms_and_restart() );
        }

        if(palette) {
            backbuffer.draw_palette(this->palette);
        }

        _update_video();
    }

    return ((float) number_of_frames) / outer_time.elapsed_sec();
}

void ChunkyMode::set_window_title(const char* title) {
    SDL_SetWindowTitle(_window, title);
}

/////////////////////
///// private
/////////////////////

/** 8-bit chunky to 16-bit texture */
void ChunkyMode::_blit_screen16(PixelSurface& source, uint16_t* destination)
{
    uint16_t *dest = destination;
    uint8_t *src = source.pixels;
    for(int i = source.w * source.h; i > 0; i--) {
        *(dest++) = palette.colors[*(src++)].value;
    }
}

/** blit back buffer pixels to the screen */
void ChunkyMode::_update_video() {
    uint16_t* screen_pixels;
    int32_t pitch;

    if( ! SDL_LockTexture(_screen_texture, nullptr, (void **)& screen_pixels,
        &pitch) )
    {
        if (pitch == backbuffer.w << 1)
        {
            _blit_screen16(backbuffer, screen_pixels);
        }
/*            else if (pitch == WINDOW_WIDTH * 4)
            _blit_screen32();*/
        else
        {
            APP_LOG_ERROR("ChunkyMode: Unsupported pitch: %d (width %d)\n",
                pitch, backbuffer.w);
            SDL_Quit();
            exit(1);
        }


        SDL_UnlockTexture(_screen_texture);
        SDL_RenderCopy(_renderer, _screen_texture, nullptr, nullptr);
        SDL_RenderPresent(_renderer);
    }
    else
        APP_LOG_ERROR("ChunkyMode: Unable to lock screen texture");
}

// thanks to monkey0506 on libsdl.org forum, highly reworked though
int ChunkyMode::_toggle_fullscreen() {
    int result = 0;

    Uint32 flags = (SDL_GetWindowFlags(_window) ^ SDL_WINDOW_FULLSCREEN);
    if (SDL_SetWindowFullscreen(_window, flags) < 0)
    {
        APP_LOG_ERROR("Toggling fullscreen mode failed: %s", SDL_GetError());
        return -1;
    }

    if ((flags & SDL_WINDOW_FULLSCREEN) != 0)
    {
        SDL_ShowCursor(SDL_DISABLE);
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");
        result = 1;
    }
    else
    {
        SDL_ShowCursor(SDL_ENABLE);
        SDL_SetWindowSize(
            _window, backbuffer.w * attributes.window_pixel_size,
            backbuffer.h * attributes.window_pixel_size );
        SDL_SetWindowPosition(_window, SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED);
        result = 0;
    }

    flags = SDL_RENDERER_ACCELERATED;
    if( attributes.with_vsync )
        flags |= SDL_RENDERER_PRESENTVSYNC;

    SDL_DestroyTexture(_screen_texture);
    SDL_DestroyRenderer(_renderer);
    _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED
        | SDL_RENDERER_PRESENTVSYNC);
    _screen_texture = SDL_CreateTexture(_renderer, SDL_PIXELFORMAT_RGB565,
        SDL_TEXTUREACCESS_STREAMING, backbuffer.w, backbuffer.h);
    SDL_RenderSetLogicalSize(_renderer, backbuffer.w, backbuffer.h);

    return result;
}

/** alternative helper to access the singleton */
ChunkyMode& Video() {
    return ChunkyMode::instance();
}

/** used by the ColorPalette class (to avoid cyclic dependencies) */
void set_video_palette(const ColorPalette& palette) {
    Video().palette = palette;
}

#endif // X_CHUNKYMODE_IMPLEMENTATION
}
#endif // X_CHUNKYMODE_HH
