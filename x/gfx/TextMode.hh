/*
 * TextMode.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_TEXMODE_HH
#define X_TEXMODE_HH

/*

Current strategy as of 2019-02-19:

- pure ANSI text video mode
- brut rendering of the full content into the video framebuffer
- management of blinking elements: one timer
- font selection
- TBC : put char with automatic vertical scrolling
- need to add textmode compatibility in ChunkyMode (640x480 display)
    - recreate the framebuffer and the screen texture with the correct resolution
    - tbc in fullscreen...


----------
PAST THINKING
=============


TODO : TextMode or ANSI Mode ???

do this store color infos???

The best would be to have a TRUE ansi compatible text video mode, with
autoscroll according to a given bauds...

Question: immediate mode (i.e., ANSI interpretation at each cycle)
or retained mode (i.e., caching into the framebuffer) ???

To manage the drawing, a font utility will be needed.

Performances testing will be needed to see if it's better to manage char color
in real-time or at the global font texture level.
Font utility will certainly need to manage, with/without colors, at chars or 
global level, ... Think about managing only 4 bytes words aligned chars...

*/

namespace bbl
{
    class TextMode
    {
    public:

        ColorPalette ansi_palette() {
            ColorPalette r;
            // TODO: tbd
            return r;
        }

        void render(PixelSurface& surface) {

        }

        uint16_t* chars = nullptr;

    private:
    };
} // bb::

#ifdef X_TEXTMODE_IMPLEMENTATION

#endif // X_TEXTMODE_IMPLEMENTATION

#endif // X_TEXMODE_HH