/*
 * Matrix.hh
 * Copyleft (c) Pierre-Jean Turpeau 03/2018
 * <pierrejean AT turpeau DOT net>
 *
 * Inspired by: http://www.reedbeta.com/blog/on-vector-math-libraries/
 *
 */
#ifndef X_MATRIX_H
#define X_MATRIX_H

#include <cmath>
#include <x/math/Vector.hh>

namespace x {
        
    template <typename T, int C, int R>
    struct Matrix {
        union {
            T data[C][R];
            Vector<T,C> rows[R]; // TODO: not sure it works, might be useful for further complex operations
        };
    };

    template <typename T, int C, int R>
    void mat_identity(Matrix<T,C,R>& m) {
        for(int j = 0; j < R; j++)
            for(int i = 0; i < C; i++)
                m.data[i][j] = i == j ? 1 : 0;
    }

    template <typename T, int C, int R>
    Vector<T,R> operator *(Matrix<T,C,R>& m, Vector<T,C>& v) {
        Vector<T,R> r;
        for(int j = 0; j < R; j++) {
            r.data[j] = 0;
            for(int i = 0; i < C; i++)
                r.data[j] += m.data[i][j] * v.data[i];
        }
        return r;
    }

    //=========================================================================
    // Specialized types
    //=========================================================================

    template <typename T> struct Matrix<T,3,3> {
        union {
            T data[3][3];
            struct {
                float m00, m01, m02;
                float m10, m11, m12;
                float m20, m21, m22;
            };
        };
    };

    typedef Matrix<float,3,3> Mat3f;

    // Sligthly faster than using the general purpose operator*
    template <typename T>
    Vector<T,3> operator *(Matrix<T,3,3>& m, Vector<T,3>& v) {
        Vector<T,3> r;
        r.x = m.m00 * v.x + m.m10 * v.y + m.m20 * v.z;
        r.y = m.m01 * v.x + m.m11 * v.y + m.m21 * v.z;
        r.z = m.m02 * v.x + m.m12 * v.y + m.m22 * v.z;
        return r;
    }

    /**
     *           1        0        0
     * Rx(a) =   0      cos(a)   -sin(a)
     *           0      sin(a)    cos(a)
     *
     *
     *          cos(a)    0      sin(a)
     * Ry(a) =   0        1        0
     *         -sin(a)    0      cos(a)
     *
     *
     *         cos(a)   -sin(a)    0
     * Rz(a) = sin(a)    cos(a)    0
     *           0        0        1
     *
     *
     * ROT = Ry * Rx * Rz;
     */
    template <typename T>
    Matrix<T,3,3>& mat_rotation(Matrix<T,3,3>& m, float rad_x, float rad_y,
        float rad_z)
    {
        float csx = std::cos(rad_x);
        float snx = std::sin(rad_x);

        m.m00 = 1; m.m01 =   0; m.m02 =   0;
        m.m10 = 0; m.m11 = csx; m.m12 = -snx;
        m.m20 = 0; m.m21 = snx; m.m22 =  csx;
        //TODO: to complete
        return m;
    }



    template <typename T> struct Matrix<T,2,2> {
        union {
            T data[2][2];
            struct {
                float m00, m01;
                float m10, m11;
            };
        };
    };


    typedef Matrix<float,2,2> Mat2f;

    // Sligthly faster than using the general purpose operator*
    template <typename T>
    Vector<T,2> operator *(Matrix<T,2,2>& m, Vector<T,2>& v) {
        Vector<T,2> r;
        r.x = m.m00 * v.x + m.m10 * v.y;
        r.y = m.m01 * v.x + m.m11 * v.y;
        return r;
    }

    /**
     *         cos(a)   -sin(a)
     * Rz(a) = sin(a)    cos(a)
     */
    template <typename T>
    Matrix<T,2,2>& mat_rotation(Matrix<T,2,2>&m, float rad_z) {
        float csz = std::cos(rad_z);
        float snz = std::sin(rad_z);

        m.m00 = csz;  m.m01 = -snz;
        m.m10 = snz;  m.m11 =  csz;
        return m;
    }
}

#endif // X_MATRIX_H
