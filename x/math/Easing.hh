/*
 * Easing.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_EASING_HH
#define X_EASING_HH

namespace x {
    
template<typename T>
T ease_linear(T from, T delta, float time, float duration);

template<typename T>
T ease_in_quad(T from, T delta, float time, float duration);

template<typename T>
T ease_out_quad(T from, T delta, float time, float duration);

template<typename T>
T ease_in_cubic(T from, T delta, float time, float duration);

template<typename T>
T ease_in_quart(T from, T delta, float time, float duration);

template<typename T>
T ease_out_quart(T from, T delta, float time, float duration);

template<typename T>
T ease_out_bounce(T from, T delta, float time, float duration);

///////////////////////////////////////////////////////////////////////////////

#ifdef X_EASING_IMPLEMENTATION

template<typename T>
T ease_linear(T from, T delta, float time, float duration) {
    return T(delta * (time/duration) + from);
}

template<typename T>
T ease_in_quad(T from, T delta, float time, float duration) {
    time /= duration;
    return T(delta * time * time + from);
}
template<typename T>
T ease_out_quad(T from, T delta, float time, float duration) {
    time /= duration;
    return T(-delta * time * (time - 2) + from);
}

template<typename T>
T ease_in_cubic(T from, T delta, float time, float duration) {
    time /= duration;
    return T(delta * time * time * time + from);
}

template<typename T>
T ease_in_quart(T from, T delta, float time, float duration) {
    time /= duration;
    return delta * time * time * time * time + from;
}

template<typename T>
T ease_out_quart(T from, T delta, float time, float duration) {
    time /= duration;
    time --;
    return -delta * (time * time * time * time - 1) + from;
}

template<typename T>
T ease_out_bounce(T from, T delta, float time, float duration) {
    time /= duration;

    if(time < (1/2.75))  {
        return T(delta * (7.5625 * time * time) + from);
    } else if(time < (2/2.75)) {
        time -= (1.5/2.75);
        return T(delta*(7.5625 * time * time + 0.75) + from);
    } else if(time < (2.5/2.75)) {
        time -= (2.25/2.75);
        return T(delta*(7.5625 * time * time + 0.9375) + from);
    } else {
        time -= (2.625/2.75);
        return T(delta*(7.5625 * time * time + 0.984375) + from);
    }
}

#endif // X_EASING_IMPLEMENTATION
}
#endif // X_EASING_HH
