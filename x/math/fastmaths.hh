/*
 * fastmaths.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_FASTMATHS_H
#define X_FASTMATHS_H

#include <x/x.hh>

namespace x {

static constexpr int SQRT_MAGIC_F = 0x5f3759df;

// good approximation for values < 289
extern int sqrt(unsigned int x);

// quake sqrt
extern float sqrt(float x);

//http://pubs.opengroup.org/onlinepubs/009695399/functions/atan2.html
extern float atan2(float x, float y) ;

///////////////////////////////////////////////////////////////////////////////
#ifdef X_FASTMATHS_IMPLEMENTATION

int sqrt(unsigned int x) {
    // adjust bias
    x  += 127 << 23;
    // approximation of square root
    x >>= 1;

    return x;
}

// quake sqrt
float sqrt(float x) {
    const float xhalf = 0.5f*x;

    union // get bits for floating value
    {
        float x;
        int i;
    } u;

    u.x = x;
    u.i = SQRT_MAGIC_F - (u.i >> 1);     // gives initial guess y0
    return x*u.x*(1.5f - xhalf*u.x*u.x); // Newton step, repeating increases accuracy
}

//http://pubs.opengroup.org/onlinepubs/009695399/functions/atan2.html
//Volkan SALMA
float atan2(float x, float y) {
    const float ONEQTR_PI = M_PI / 4.0;
    const float THRQTR_PI = 3.0 * M_PI / 4.0;
    float r, angle;
    float abs_y = std::abs(y) + 1e-10f; // kludge to prevent 0/0 condition
    if ( x < 0.0f )
    {
        r = (x + abs_y) / (abs_y - x);
        angle = THRQTR_PI;
    }
    else
    {
        r = (x - abs_y) / (x + abs_y);
        angle = ONEQTR_PI;
    }
    angle += (0.1963f * r * r - 0.9817f) * r;
    if ( y < 0.0f )
        return( -angle );     // negate if in quad III or IV
    else
        return( angle );
}
#endif // X_FASTMATHS_IMPLEMENTATION
}
#endif // X_FASTMATHS_H
