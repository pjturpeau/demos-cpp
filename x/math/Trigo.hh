/*
 * Trigo.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2018
 * <pierrejean AT turpeau DOT net>
 *
 * Pre-caculated sine and cosine with fixed point angles (BITS) and a
 * resolution factor to be used for integer types again to have fixed point
 * values.
 */
#ifndef X_TRIGO_HH
#define X_TRIGO_HH

#include <math.h>

namespace x {

// T to be int or float, BITS for the angle resolution
template<typename T, int BITS>
class Trigo {
public:
    Trigo() {
        configure(0, 1);
    }
    
    /** simple equation: v = center + amplitude * sin(angle) */
    void configure(T center, T amplitude) {
        for(uint32_t i = 0; i < RANGE; i++) {
            _sin[i] = center + amplitude * ::sin(i * M_TAU / RANGE);
        }
    }

    /**
     * configure with amplitude oscillations:
     * v = center + amplitude * sin(angle / divider) * sin(angle)
     */
    void configure(T center, T amplitude, float divider) {
        for(uint32_t i = 0; i < RANGE; i++) {
            float radian = i * M_TAU / RANGE;
            float h = ::sin( radian * divider );
            _sin[i] = T(center + amplitude * h * ::sin(radian));
        }
    }

    /**
     * configure with both sin and cos:
     * v = center + amplitude * (cos_amplitude * cos(cos_shift+angle*cos_speed)
     *     + sin_amplitude * sin(sin_shift+angle*sin_speed))
     */
    void configure(T center, T amplitude,
        float cos_amplitude, float cos_speed, float cos_shift, 
        float sin_amplitude, float sin_speed, float sin_shift )
    {
        for(uint32_t i = 0; i < RANGE; i++) {
            float radian = i * M_TAU / RANGE;

            _sin[i] = T(center + amplitude *
                (cos_amplitude*::cos(cos_speed*radian+cos_shift) +
                    sin_amplitude*::sin(sin_speed*radian+sin_shift)));
        }
    }                

    T sin(uint32_t angle)  {
        return _sin[angle & AMASK];
    }

    T cos(uint32_t angle) {
        return _sin[(angle + PI_2) & AMASK];
    }

    // Range for integer angle value 
    static constexpr uint32_t RANGE = (1<<BITS);

    // Mask for integer angle value
    static constexpr uint32_t AMASK = RANGE-1;

    // PI and PI/2 values according to range
    static constexpr uint32_t PI    = RANGE/2;
    static constexpr uint32_t PI_2  = RANGE/4;

private:
    T _sin[RANGE];
};

typedef Trigo<int, 12> IntegerTrigo;
typedef Trigo<float, 12> FloatTrigo;

}
#endif // X_TRIGO_HH
