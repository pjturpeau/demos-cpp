/*
 * Vector.hh
 * Copyleft (c) Pierre-Jean Turpeau 03/2018
 * <pierrejean AT turpeau DOT net>
 *
 * Inspired by: http://www.reedbeta.com/blog/on-vector-math-libraries/
 *
 */
#ifndef X_VECTOR_HH
#define X_VECTOR_HH

namespace x {

    template <typename T, int N> struct Vector
    {
        T data[N];
    };

    template<typename T, int N> Vector<T,N> operator+=(Vector<T,N>& v,
        Vector<T,N>& a)
    {
        for(int i = 0; i < N; i++)
            v.data[i] += a.data[i];
        return v;
    }

    template<typename T, int N> Vector<T,N> operator+(Vector<T,N> v,
        Vector<T,N>& a)
    {
        Vector<T,N> r(v);
        r += a;
        return r;
    }

    template<typename T, int N> Vector<T,N>&
    operator*=(Vector<T,N> &v, float a) {
        for( auto &val: v.data)
            val *= a;
        return v;
    }

    template<typename T, int N> Vector<T,N> operator*(Vector<T,N> v, float a) {
        Vector<T,N> r(v);
        r *= a;
        return r;
    }

    template<typename T, int N> Vector<T,N>& vec_zero(Vector<T,N>& v) {
        for(auto &vv : v.data)
            vv = 0;
        return v;
    }

    template<typename T, int N> T vec_length(Vector<T,N>& v) {
        T result = 0;
        for(auto vv : v.data)
            result += vv*vv;
        return result;
    }

    template<typename T, int N> Vector<T,N>& vec_normalize(Vector<T,N>& v) {
        T l = vec_length(v);
        if(l == 0)
            return vec_zero(v);
        else {
            l = 1 / l;
            for( auto &vv: v.data)
                vv *= l;
        }
        return v;
    }

    //=========================================================================
    // Specialized types
    //=========================================================================

    template <typename T> struct Vector<T,2> {
        union {
            T data[2];
            struct { T x, y; };
            struct { T u, v; };
        };

        Vector() {}

        Vector<T,2>(Vector<T,2>& v) {
            x = v.x;
            y = v.y;
        }

        Vector<T,2>(T x, T y) {
            this->x = x;
            this->y = y;
        }

        Vector<T,2>& operator=(const Vector<T,2>& v) {
            if(&v != this) {
                this->x = v.x;
                this->y = v.y;
            }
            return *this;
        }
    };

    template <typename T> struct Vector<T,3> {
        union {
            T data[3];
            struct { T x, y, z; };
            struct { T r, g, b; };
            struct { T u, v, w; };
        };

        Vector() {}

        Vector<T,3>(Vector<T,3>& v) {
            x = v.x;
            y = v.y;
            z = v.z;
        }

        Vector<T,3>(T x, T y, T z) {
            this->x = x;
            this->y = y;
            this->z = z;
        }
    };

    template <typename T> struct Vector<T,4> {
        union {
            T data[4];
            struct { T x, y, z, w; };
        };
    };

    using Vec2f = Vector<float,2>;
    using Vec3f = Vector<float,3>;
    using Vec3i = Vector<int,3>;
}

#endif // X_VECTOR_HH
