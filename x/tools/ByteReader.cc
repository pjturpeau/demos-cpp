/*
 * ByteReader.cc
 * Copyleft (c) Pierre-Jean Turpeau 11/2017
 * <pierrejean AT turpeau DOT net>
 */
#include "ByteReader.hh"

#include <string.h>

#ifdef LINUX
#include <netinet/in.h>
#elif WIN32
#include <winsock.h>
#endif // WIN32

namespace x {
        
    ByteReader::ByteReader() : ByteReader(NULL, 0) {}

    ByteReader::ByteReader(uint8_t *data_in, uint32_t data_length)
        : _data(data_in), _length(data_length), _read_offset(0)
    {
        if( _data == NULL )
        {
            error_number = ERR_NOT_INITIALIZED;
        }
        else
        {
            error_number = ERR_NONE;
        }
    }

    ByteReader::~ByteReader() {}

    void ByteReader::read_string(char* str_out, uint32_t maxlen) {
        if( _read_offset + maxlen <= _length )
        {
            memcpy(str_out, _data + _read_offset, maxlen);
            str_out[maxlen-1] = 0;
            _read_offset += maxlen;
        }
        else
        {
            error_number = ERR_INVALID_OFFSET;
            str_out[0] = 0;
        }
    }

    uint32_t ByteReader::read_u32() {
        if( _data && _read_offset + sizeof(uint32_t) <= _length)
        {
            void *address = _data + _read_offset;
            _read_offset += sizeof(uint32_t);
            return ntohl(*((uint32_t *) address));
        }
        else
        {
            error_number = ERR_INVALID_OFFSET;
            return 0;
        }
    }

    uint16_t ByteReader::read_u16() {
        if( _data && _read_offset + sizeof(uint16_t) <= _length )
        {
            void *address = _data + _read_offset;
            _read_offset += sizeof(uint16_t);
            return ntohs(*((uint16_t *) address));
        }
        else
        {
            error_number = ERR_INVALID_OFFSET;
            return 0;
        }
    }

    uint8_t ByteReader::read_u8() {
        if( _data && _read_offset + 1 <= _length )
        {
            void *address = _data + _read_offset;
            _read_offset += sizeof(uint8_t);
            return *((uint8_t *) address);
        }
        else
        {
            error_number = ERR_INVALID_OFFSET;
            return 0;
        }
    }

    void ByteReader::read_bytes(uint8_t* out, uint32_t length) {
        if( _data && _read_offset + length <= _length )
        {
            void *address = _data + _read_offset;
            memcpy(out, address, length);
            _read_offset += length;
        }
        else
        {
            error_number = ERR_INVALID_OFFSET;
        }
    }
}
