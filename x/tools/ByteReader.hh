/*
 * ByteReader.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2017
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_BYTEREADER_H
#define X_BYTEREADER_H

#include <stdint.h>

namespace x
{
    class ByteReader
    {
    public:

        ByteReader();
        ByteReader(uint8_t *data_in, uint32_t data_length);
        virtual ~ByteReader();

        inline void seek(uint32_t offset) { _read_offset = offset; }
        inline void skip(uint32_t offset) { _read_offset += offset; }
        inline uint32_t tell() { return _read_offset; }

        void read_string(char *str_out, uint32_t maxlen);
        uint32_t read_u32();
        uint16_t read_u16();
        uint8_t  read_u8();

        void read_bytes(uint8_t *out, uint32_t length);

        // public properties
        static constexpr uint32_t ERR_NONE  = 0x00;
        static constexpr uint32_t ERR_INVALID_OFFSET = 0x10;
        static constexpr uint32_t ERR_NOT_INITIALIZED = 0x20;

        uint32_t error_number;

    private:
        uint8_t *_data;
        uint32_t _length;
        uint32_t _read_offset;
    };
}

#endif // X_BYTEREADER_H
