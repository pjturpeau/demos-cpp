/*
 * Chrono.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_CHRONO_HH
#define X_CHRONO_HH

#include <stdint.h>
#include <SDL2/SDL.h>

namespace x {

class Chrono
{
public:

    Chrono();
    Chrono(const Chrono& other);
    Chrono& operator=(const Chrono& other);

    void restart();
    float elapsed_ms_and_restart();
    float elapsed_ms();
    float elapsed_sec();

private:
    uint64_t _start_time;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_CHRONO_IMPLEMENTATION

Chrono::Chrono() {
    _start_time = SDL_GetPerformanceCounter();
}

Chrono::Chrono(const Chrono& other) : _start_time(other._start_time) {}

Chrono& Chrono::operator=(const Chrono& other) {
    _start_time = other._start_time;
    return *this;
}

void Chrono::restart() {
    _start_time = SDL_GetPerformanceCounter();
}

float Chrono::elapsed_ms_and_restart() {
    uint64_t NOW = SDL_GetPerformanceCounter();
    uint64_t elapsed = NOW - _start_time;
    float result = (float) elapsed * 1000.0f / SDL_GetPerformanceFrequency();
    _start_time = NOW;
    return result;
}

float Chrono::elapsed_ms() {
    uint64_t elapsed = SDL_GetPerformanceCounter() - _start_time;
    return (float)(elapsed * 1000.0f / SDL_GetPerformanceFrequency());
}

float Chrono::elapsed_sec() {
    uint64_t elapsed = SDL_GetPerformanceCounter() - _start_time;
    return (float) elapsed / SDL_GetPerformanceFrequency();
}

#endif // X_CHRONO_IMPLEMENTATION
}
#endif // X_CHRONO_HH
