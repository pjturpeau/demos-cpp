/*
 * DotTunnelFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_DOTTUNNELFX_HH
#define X_DOTTUNNELFX_HH

#include <x/x.hh>
#include <x/gfx/ColorPalette.hh>
#include <x/gfx/PixelSurface.hh>
#include <x/math/Vector.hh>

#include <math.h>

namespace x {

template <int WIDTH, int HEIGHT>
class DotTunnelFx
{
public:

    DotTunnelFx()
    {
        // compute points relative positions for one circle
        for(int i = 0; i < NB_POINTS_IN_CIRCLE*CIRCLE_EXTRA_RESOLUTION; i++)
        {
            // compute radian angle
            double angle = 2.0 * M_PI /
                (NB_POINTS_IN_CIRCLE * CIRCLE_EXTRA_RESOLUTION) * i; 
            
            circle_px[i] = CIRCLE_RADIUS * std::cos(angle);
            circle_py[i] = CIRCLE_RADIUS * std::sin(angle);
        }

        // compute Z positions for each circle
        for(int j = 0; j < NB_CIRCLES; j++)
        {
            tunnel[j].x = 0;

            // put initial circles out of the field of view to make them
            // appear gracefully
            tunnel[j].y = TOTAL_DEPTH * focal_distance;
            tunnel[j].z = TOTAL_DEPTH * j / NB_CIRCLES;
        }
    }

    /** typical default palette */
    ColorPalette yellow_and_blue_palette() {
        ColorPalette palette;
        palette.linear_gradient(  0, 0x0, 127, 0x00FFFF00);
        palette.linear_gradient(128, 0x0, 255, 0x000020FF);
        return palette;
    }

    void update(float delta_time_ms)
    {
        animation_angle += 0.04;

        for (int i = 0; i < NB_CIRCLES; i++)
        {
            tunnel[i].z -= 4;

            // throw the near most circle to the far most Z
            if( tunnel[i].z < 4 )
            {
                tunnel[i].z += TOTAL_DEPTH ;

                tunnel[i].x = WIDTH * 96/128*cos(animation_angle-0.25) +
                    WIDTH * 32/128 * sin(animation_angle*2);

                tunnel[i].y = HEIGHT*48/128*sin(animation_angle+0.25) -
                    HEIGHT * 64/128 * cos(animation_angle+0.75);

                // once a circle has been thrown to the back, special action
                // is needed to correctly manage the tunnel array limit.
                far_most_circle++;
                if( far_most_circle >= NB_CIRCLES )
                {
                    far_most_circle = 0;
                }
            }
        }
    }

    void draw(PixelSurface& surface)
    {
        // need to be fine tuned according to: depth, radius, etc. */
        float b = animation_angle+1.25;

        int x = WIDTH *96/128*cos(b-0.25) + WIDTH *32/128*sin(b*2);
        int y = HEIGHT*48/128*sin(b+0.25) - HEIGHT*64/128*cos(b+0.75);

        // painter algorithm: start by the far most circle
        for(int i = far_most_circle-1; i >= 0; i--)
        {
            _draw_circle(surface, tunnel[i].x-x, tunnel[i].y-y, tunnel[i].z, i);
        }
        for(int i = NB_CIRCLES-1; i > far_most_circle - 1 ; i--)
        {
            _draw_circle(surface, tunnel[i].x-x, tunnel[i].y-y, tunnel[i].z, i);
        }
    }

private: //////////////////////////////////////////////////////// PRIVATE

    void _draw_circle( PixelSurface& surface, int32_t x, int32_t y, int32_t z, int32_t circle_index)
    {
        int color;

        // reminder:
        //  - x_proj = x * focal_distance / z
        //  - y_proj = x * focal_distance / z
        float fdz = focal_distance / z; // do the divide once

        int advance_angle = animation_angle * 64;

        for(int i = 0; i < NB_POINTS_IN_CIRCLE; i++)
        {
            // compute the point index according to the angle advance to
            // simulate a rotation of the circle
            int idx = (advance_angle + i * CIRCLE_EXTRA_RESOLUTION)
                        // modulo the circle
                        % (NB_POINTS_IN_CIRCLE*CIRCLE_EXTRA_RESOLUTION);

            if( (i + circle_index) % 4 > 1 )
                color = 256 - z * 128 / TOTAL_DEPTH;
            else
                color = 128 - z * 128 / TOTAL_DEPTH;

            // projection of the circle point (cx,cy) relative to its
            // (x,y,z) center in the 3D space and translate the result
            // according to the screen center
            int px = (x + circle_px[idx])*fdz + WIDTH/2;
            int py = (y + circle_py[idx])*fdz + HEIGHT/2;

            surface.set_safe( px,   py,   color );
            surface.set_safe( px+1, py,   color );
            surface.set_safe( px,   py+1, color );
            surface.set_safe( px+1, py+1, color );
        }
    }

    // PRIVATE PROPERTIES ==================================================

    static constexpr int NB_CIRCLES = 64;
    static constexpr int NB_POINTS_IN_CIRCLE = 48;
    static constexpr int CIRCLE_EXTRA_RESOLUTION = 8; // used to simulate rotation

    static constexpr int CIRCLE_RADIUS = 128;
    static constexpr int TOTAL_DEPTH = 512; // Z value for the far most circle

    /* xproj = x * fd / z */
    static constexpr double focal_distance = 64; /* fd = 1 / tan(fov/2) */

    /* tables for points in one circle */
    int circle_px[NB_POINTS_IN_CIRCLE * CIRCLE_EXTRA_RESOLUTION];
    int circle_py[NB_POINTS_IN_CIRCLE * CIRCLE_EXTRA_RESOLUTION];

    float animation_angle = 0;

    Vec3i tunnel[NB_CIRCLES];

    // the index of the far most circle in the tunnel array
    // the one from which to start drawing (painter algorithm)
    int far_most_circle = 0;
};

}
#endif // X_DOTTUNNELFX_HH
