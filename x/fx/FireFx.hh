/*
 * FireFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_FIREFX_HH
#define X_FIREFX_HH

#include <stdlib.h> // rand(), srand()
#include <time.h>   // time()

#include <x/gfx/ColorPalette.hh>

namespace x {

/**
 * MAX_COLOR_INDEX allows to generate the fire using less colors and keep room
 * for few others to display text. Tested also with 64 which works with a
 * smaller fire result.
 */
template <int MAX_COLOR_INDEX>
class FireFx
{
public:
    static constexpr int MAX_8BITS_INDEX = 255;

    // interpolation macro
    #define INTERP(V) (V * MAX_COLOR_INDEX / MAX_8BITS_INDEX)

    FireFx() {
        static_assert( (MAX_COLOR_INDEX >= 0) && (MAX_COLOR_INDEX <= MAX_8BITS_INDEX),
            "bad MAX_COLOR_INDEX value");

        srand(time(NULL));
    }

    /** returns a yellow-to-blue palette */
    ColorPalette yellow_to_blue_fire() {
        ColorPalette palette;
        palette.linear_gradient( INTERP(  0), 0x000000, INTERP( 16), 0x021B3B);
        palette.linear_gradient( INTERP( 16), 0x021B3B, INTERP( 32), 0xFF1C1C);
        palette.linear_gradient( INTERP( 32), 0xFF1C1C, INTERP( 96), 0xFF8C00);
        palette.linear_gradient( INTERP( 96), 0xFF8C00, INTERP(128), 0xFFFFA0);
        palette.linear_gradient( INTERP(128), 0xFFFFA0, INTERP(255), 0xFFFFA0);
        return palette;
    }

    void draw(PixelSurface& s) {
        for(int i = 0; i < s.w; i++ ) {
            s.set_safe(i, s.h - 1, rand() % 16 > 8 ? MAX_COLOR_INDEX : 0 );
            // shall be done outside of the visible area
        }

        for( int j = 1; j < s.h; j++) {
            for(int i = 0; i < s.w; i++) {

                uint32_t value = 
                    s.pixels[i-1 + j*s.w] + s.pixels[i+1+j*s.w] +
                    s.pixels[i+j*s.w]     + s.pixels[i + (j-1)*s.w];

                value >>= 2;

                // if(value > 0) value--;   // a little bit more dynamic at the top
                if(value > 0 && value < 96) value--;

                s.pixels[i+(j-1)*s.w] = value;
            }
        }
    }

    ColorPalette palette;
};

}
#endif // X_FIREFX_HH
