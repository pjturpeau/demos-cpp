/*
 * MoireFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 03/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_MOIREFX_HH
#define X_MOIREFX_HH

#include <x/gfx/ColorPalette.hh>
#include <math.h>

namespace x {

class MoireFx
{
public:

    MoireFx() {}

    void update(PixelSurface& s, float dt) {
        static float elapsed_time = 0;

        elapsed_time += dt * 0.002;

        cx1 = sin(elapsed_time / 2) * s.w / 3 + s.w / 2;
        cy1 = sin(elapsed_time / 4) * s.h / 3 + s.h / 2;
        cx2 = cos(elapsed_time / 2) * s.w / 3 + s.w / 2;
        cy2 = cos(elapsed_time / 1) * s.h / 3 + s.h / 2;
    }


    void draw(PixelSurface& s, bool flat = true) {
        for( int j = 0; j < s.h; j++) {

            int dy1 = (j - cy1);
            dy1 *= dy1;

            int dy2 = (j - cy2);
            dy2 *= dy2;

            for(int i = 0; i < s.w; i++) {

                int dx1 = (i - cx1);
                dx1 *= dx1;

                int dx2 = (i - cx2);
                dx2 *= dx2;

                int value;

                if( flat ) {
                    dx1 = ::sqrt(dx1 + dy1);
                    dx2 = ::sqrt(dx2 + dy2);
                    value = ((dx1 ^ dx2) >> 4 ) & 1;
                } else {        
                    // interesting results...
                    float one = ::sqrt(dx1 + dy1);
                    float two = ::sqrt(dx2 + dy2);
                    value = 8 + 8 * (sin(one / 4) + sin(two / 3)) / 2;
                }

                s.set(i, j, value);
            }
        }
    }

    ColorPalette palette(bool flat = true) {
        ColorPalette p;

        if( flat ) {
            p.set_color(0, 0, 0, 0);
            p.set_color(1, 0xFFFFFF);
        }  else
            p.linear_gradient(0, 0x000000, 16, 0xFFFFFF);

        return p;
    } 

    int cx1, cy1;
    int cx2, cy2;
};

}
#endif // X_MOIREFX_HH
