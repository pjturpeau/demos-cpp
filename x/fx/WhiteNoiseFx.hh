/*
 * WhiteNoise.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_WHITENOISEFX_HH
#define X_WHITENOISEFX_HH

#include <ctime>

#include <x/gfx/PixelSurface.hh>

namespace x {

/** work best with monochrome palette */
class WhiteNoiseFx
{
public:
    /** Fill the surface width random points */
    void seed_noise(PixelSurface& surface);
    void draw(PixelSurface& surface);
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_WHITENOISEFX_IMPLEMENTATION

/** Fill the surface width random points */
void WhiteNoiseFx::seed_noise(PixelSurface& surface) {
    ::srand(::time(nullptr));
    for (int i = 0; i < surface.w * surface.h; i++)
        surface.pixels[i] = ((::rand() & 1) ? 255 : 0);
}

void WhiteNoiseFx::draw(PixelSurface& surface) {
    for (int Y = 0; Y < surface.h; Y++)
    {
        uint8_t *Row1;

        if( Y == 0 )
            Row1 = surface.pixels + surface.w * (surface.h - 1);
        else
            Row1 = surface.pixels + surface.w * (Y - 1);
            
        uint8_t *Row2 = surface.pixels + surface.w * Y;

        uint8_t *Row3 = surface.pixels + surface.w * (Y + 1) % (surface.w*surface.h);

        *Row1++ = 0;
        *Row2++ = 0;
        *Row3++ = 0;

        for (int X = 1; X < surface.w - 1; X++)
        {
            int Color = (*Row1 + *Row3 + *(Row2 - 1) + *(Row1 + 1)) >> 1;

            if (Color < 0)
                Color = 0;

            *Row2 = Color;

            Row1++;
            Row2++;
            Row3++;
        }

        *Row1++ = 0;
        *Row2++ = 0;
        *Row3++ = 0;
    }
}

#endif // X_WHITENOISEFX_IMPLEMENTATION
}
#endif // X_WHITENOISEFX_HH
