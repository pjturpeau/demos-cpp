/*
 * UVMapFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_UVMAPFX_HH
#define X_UVMAPFX_HH

#include <x/gfx/PixelSurface.hh>
#include <x/math/Vector.hh>
#include <math.h>

namespace x {

template <int WIDTH, int HEIGHT>
class UVMapFx
{
public:

    ~UVMapFx();

    void texture(const PixelSurface& texture);

    void shadow_colors(int nb_colors);
    int shadow_levels();

    void generate_tunnel();
    void generate_waves();
    void generate_warping();
    void generate_warping_near();
    void generate_warping_far();

    void update_tunnel(float delta_time_ms);

    /** draw pixels using u,v,w maps */
    void draw(PixelSurface& surface);

    /** draw the distance map (u) */
    void render_u_map(PixelSurface& surface);

    /** draw the angle map (v) */
    void render_v_map(PixelSurface& surface);
    
    /** draw the shadow map (w) */
    void render_w_map(PixelSurface& surface);

    // PUBLIC properties ==================================================
    static constexpr int TEXTURE_SIZE = 256;

    float start_u = 0;
    float start_v = 0;

private:
    // number of usefull colors in the _texture palette
    int _shadow_colors = 256;

    // number of possible gradient levels in the palette
    int _shadow_levels = 1;

    const PixelSurface* _texture = nullptr;

    using Vec3u8 = Vector<uint8_t,3>;
    Vec3u8 *uv_map = nullptr;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_UVMAPFX_IMPLEMENTATION

template <int WIDTH, int HEIGHT>
UVMapFx<WIDTH,HEIGHT>::~UVMapFx() {
    if(uv_map) delete [] uv_map;
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::texture(const PixelSurface& texture) {
    assert(texture.w == TEXTURE_SIZE);
    assert(texture.h == TEXTURE_SIZE); // bad _texture

    this->_texture = &texture;
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::shadow_colors(int nb_colors) {
    assert(nb_colors > 0); // bad shadow_colors parameter

    _shadow_colors = nb_colors;
    _shadow_levels = ColorPalette::SIZE / _shadow_colors;
}

template <int WIDTH, int HEIGHT>
int UVMapFx<WIDTH,HEIGHT>::shadow_levels() {
    return _shadow_levels;
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::generate_tunnel() {
    constexpr float repeat_ratio = 32.0f;
    constexpr float angle_ratio  = 1.0f;

    if(uv_map)
        delete [] uv_map;

    uv_map
        = new Vec3u8[WIDTH * HEIGHT];

    for(int y = 0; y < HEIGHT; y++) {
        for(int x = 0; x < WIDTH; x++) {

            float xx = x - WIDTH / 2.0f;
            float yy = y - HEIGHT / 2.0f;

            Vec3u8 uvw;
    
            // compute distance
            uvw.u = int(repeat_ratio * TEXTURE_SIZE
            / ::sqrt(xx*xx + yy*yy)) % TEXTURE_SIZE;

            // uvw.u = int(repeat_ratio * TEXTURE_SIZE
            // / ::sqrt(xx*xx + yy*yy) * ::sin(4 * M_PI * (x+y) / (HEIGHT+WIDTH))) % TEXTURE_SIZE;

            // compute angle
            uvw.v = uint32_t(TEXTURE_SIZE
                * ::atan2(xx, yy) / M_PI * angle_ratio);

            // compute the shadow levels map
            if( xx*xx + yy*yy < 2 )
                // clamp to the maximum shadow level in the center
                uvw.w = _shadow_levels - 1; 
            else
                // compute the shadow levels
                uvw.w = int(HEIGHT / ::sqrt(xx*xx + yy*yy));

            if(uvw.w >= _shadow_levels)
                // clamp to the maximum shadow level
                uvw.w = _shadow_levels-1; 

            // store
            uv_map[x + y * WIDTH] = uvw;
        }
    }
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::generate_waves()
{
    if(uv_map) delete [] uv_map;
    uv_map = new Vec3u8[WIDTH * HEIGHT];
    
    constexpr float x_center = WIDTH / 2.0f;
    constexpr float y_center = HEIGHT / 2.0f;

    constexpr float angle_base = TEXTURE_SIZE / 4.0f;
    constexpr float angle_factor = angle_base / M_PI;
    
    // amplitude of the oscillation
    constexpr float dist_oscillation_amplitude = TEXTURE_SIZE / 4.0f;

    // distance oscillation period factor
    constexpr float dist_oscillation_period_factor = M_PI / dist_oscillation_amplitude;

    int map_offset = 0;

    for(int y = 0; y < HEIGHT; y++)
    {
        for(int x = 0; x < WIDTH; x++) {
            float xx = x - x_center;
            float yy = y - y_center;

            Vec3u8 uvw;

            // angle
            uvw.u = angle_base + atan2(xx, yy) * angle_factor;

            // distance
            uvw.v = sqrt(xx*xx + yy*yy);
            uvw.v += sin(uvw.v * dist_oscillation_period_factor)
                        * dist_oscillation_amplitude;

            uvw.w = 0;
            uv_map[map_offset++] = uvw;
        }
    }
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::generate_warping()
{
    if(uv_map) delete [] uv_map;
    uv_map = new Vec3u8[WIDTH * HEIGHT];

    float x_center = WIDTH / 2.0f;
    float y_center = HEIGHT / 2.0f;

    int map_offset = 0;

    for(int y = 0; y < HEIGHT; y++) {
        for (int x = 0; x < WIDTH; x++) {

            float xx = x - x_center;
            float yy = y - y_center;

            float d  = 50000 / (xx*xx + yy*yy + 40000);

            Vec3u8 uvw;

            uvw.u = uint8_t(xx * d);
            uvw.v = uint8_t(yy * d);
            uvw.w = 0;

            uv_map[map_offset++] = uvw;
        }
    }
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::generate_warping_near()
{
    if(uv_map) delete [] uv_map;
    uv_map = new Vec3u8[WIDTH * HEIGHT];

    float x_center = WIDTH / 2.0f;
    float y_center = HEIGHT / 2.0f;

    int map_offset = 0;

    for(int y = 0; y < HEIGHT; y++) {
        for (int x = 0; x < WIDTH; x++) {

            float xx = x - x_center;
            float yy = y - y_center;

            float d  = 5000 / (xx*xx + yy*yy);

            Vec3u8 uvw;

            uvw.u = uint8_t(xx * d);
            uvw.v = uint8_t(yy * d);

            // compute the shadow levels map
            if( xx*xx + yy*yy < 2 )
                // clamp to the maximum shadow level in the center
                uvw.w = _shadow_levels - 1; 
            else
                // compute the shadow levels
                uvw.w = int(HEIGHT / ::sqrt(xx*xx + yy*yy));

            if(uvw.w >= _shadow_levels)
                // clamp to the maximum shadow level
                uvw.w = _shadow_levels-1; 

            uv_map[map_offset++] = uvw;
        }
    }
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::generate_warping_far()
{
    if(uv_map) delete [] uv_map;
    uv_map = new Vec3u8[WIDTH * HEIGHT];

    float x_center = WIDTH / 2.0f;
    float y_center = HEIGHT / 2.0f;

    int map_offset = 0;

    for(int y = 0; y < HEIGHT; y++) {
        for (int x = 0; x < WIDTH; x++) {

            float xx = x - x_center;
            float yy = y - y_center;

            float d  = 25000 / (xx*xx + yy*yy);

            Vec3u8 uvw;
            
            uvw.u = uint8_t(xx * d);
            uvw.v = uint8_t(yy * d);
            uvw.w = 0;

            uv_map[map_offset++] = uvw;
        }
    }
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::update_tunnel(float delta_time_ms) {
    start_u += delta_time_ms * 0.05;
    start_v += delta_time_ms * 0.05;
    return;
    start_u += delta_time_ms * 0.2;
    start_v += delta_time_ms * 0.125;            
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::draw(PixelSurface& surface) {
    assert(surface.w == WIDTH);
    assert(surface.h == HEIGHT);
    assert(_texture);
    assert(uv_map);

    for( int j=0; j < WIDTH * HEIGHT; j++)
    {
        Vec3u8 uvw = uv_map[j];

        int s_u = (int) start_u;
        int s_v = (int) start_v;

        uint16_t texture_offset
            = (s_u + uvw.u + ((uvw.v + s_v) << 8)) & 0xFFFF;

        surface.pixels[j] =
            _texture->pixels[texture_offset] + uvw.w * _shadow_colors;
    }
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::render_u_map(PixelSurface& surface) {
    assert(surface.w == WIDTH);
    assert(surface.h == HEIGHT); // bad surface

    for( int j=0; j < WIDTH * HEIGHT; j++)
        surface.pixels[j] = uv_map[j].u;
}

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::render_v_map(PixelSurface& surface) {
    assert(surface.w == WIDTH);
    assert(surface.h == HEIGHT); // bad surface

    for( int j=0; j < WIDTH * HEIGHT; j++)
        surface.pixels[j] = uv_map[j].v;
}       

template <int WIDTH, int HEIGHT>
void UVMapFx<WIDTH,HEIGHT>::render_w_map(PixelSurface& surface) {
    assert(surface.w == WIDTH);
    assert(surface.h == HEIGHT); // bad surface

    for( int j=0; j < WIDTH * HEIGHT; j++)
        surface.pixels[j] = uv_map[j].w;
}
#endif //X_UVMAPFX_IMPLEMENTATION
}
#endif // X_UVMAPFX_HH
