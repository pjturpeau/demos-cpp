/*
 * BumpFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_BUMPFX_HH
#define X_BUMPFX_HH

#include <assert.h>

#include <x/gfx/PixelSurface.hh>

namespace x {

class BumpFx
{
public:
    void background(const PixelSurface& background) {
        _bg = &background;

        // in case nobody bind x,y to any movement
        light_x = background.w / 2;
        light_y = background.h / 2;            
    }

    // TODO: multi-lights?
    // TODO: generate...
    void light(const PixelSurface& light) {
        assert(light.w == LIGHT_SIZE);
        assert(light.h == LIGHT_SIZE);
        _light = &light;
    }

    void draw(PixelSurface& surface) {
        assert(_bg);
        assert(_light);
        assert(_bg->w == surface.w);
        assert(_bg->h == surface.h);

        // light top left coordinate
        int topleft_x = light_x - LIGHT_CENTER; 
        int topleft_y = light_y - LIGHT_CENTER;

        for(int j = 1; j < surface.h - 1; j++ ) {
            int bump_y = j - topleft_y;

            for( int i = 1; i < surface.w-1; i++ ) {
                int bump_x = i - topleft_x;

                int colx = bump_x
                    + _bg->pixels[i - 1 + j * _bg->w]
                    - _bg->pixels[i + 1 + j * _bg->w];

                int coly = bump_y
                    + _bg->pixels[i + (j-1) * _bg->w]
                    - _bg->pixels[i + (j+1) * _bg->w];

                if( colx >= 0 && colx < LIGHT_SIZE && coly>= 0 && coly < LIGHT_SIZE ) {
                    int col = _light->pixels[colx + (coly<<8)];
                    surface.set(i, j, col);
                } else
                    surface.set(i, j, 0);
            }
        }
    }

    int light_x = 0; // light x center
    int light_y = 0; // light y center

private:
    static constexpr int LIGHT_SIZE   = 256;
    static constexpr int LIGHT_CENTER = LIGHT_SIZE / 2;  

    const PixelSurface *_bg = nullptr;
    const PixelSurface *_light = nullptr;
};

}
#endif // X_FIREFX_HH
