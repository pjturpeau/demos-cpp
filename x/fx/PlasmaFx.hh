/*
 * PlasmaFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
 #ifndef X_PLASMAFX_HH
 #define X_PLASMAFX_HH

#include <x/gfx/ColorPalette.hh>
#include <x/math/Trigo.hh>
#include <x/math/fastmaths.hh>

namespace x {

class PlasmaFx
{
public:
    PlasmaFx() {
        trigo.configure(0, RES);
        elapsed_time = 0.0f;
    }

    ColorPalette default_palette() {
        ColorPalette palette;
        for(int i = 0; i < ColorPalette::SIZE; i++) {
            float r = 1.0;
            float g = ::sin(i*M_PI/256.0);
            float b = 0.5+0.5*::sin(i*M_PI/128.0);
            palette.set_color(i, r, g, b);
        }
        return palette;
    }

    void update(float delta_time_ms) {
        elapsed_time += delta_time_ms * 0.001f;
    }

    void render_complexe(PixelSurface& surface)
    {
        for(int j = 0; j < surface.h; j++)
        {
            float y = -0.5f + 1.0f * j / surface.h;

            for(int i = 0; i < surface.w; i++)
            {
                float x = -0.5f + 1.0f * i / surface.w;

                float cx = x + 0.5f * ::sin(elapsed_time*(1/5.f));
                float cy = y + 0.5f * ::cos(elapsed_time*(1/8.f));

                float c =
                    // moving vertical stripes
                    ::sin(x*10+elapsed_time) +

                        // rotating and zooming stripes
                        ::sin(10*(x*::sin(elapsed_time*0.5f) +
                            y*::cos(elapsed_time*0.33f))+elapsed_time) +

                        // moving circular stripes
                        ::sin(x::sqrt(100*(cx*cx + cy*cy)+1)+elapsed_time);

                int col = (int)(128 + 128 * c) & 0xFF;

                surface.set(i, j, col);
            }
        }
    }

    // a simplified sine only version, no divide, no sqrt
    void render_sine_only(PixelSurface& surface)
    {
        float c;
        int col;

        float t1 = 10.0F*sin(elapsed_time*0.5f);
        float t2 = 15.0f*sin(elapsed_time*0.25f);
        float t3 = 5.0f*sin(elapsed_time);
        float t4 = sin(elapsed_time*1.5f);

        for(int j = 0; j < surface.h; j++)
        {
            float p3 = t3;
            float p4 = t4;

            for(int i = 0; i < surface.w; i++)
            {
                c = sin(t1) + sin(t2 + p4*0.5f) + sin(p3+p4*0.25f) + sin(p4);

                col = (int)(128 + 128 * c)&0xFF;
                surface.set(i, j, col);

                p3 += 0.01f;
                p4 += 0.02f;
            }

            t1 += 0.02f;
            t2 += 0.03f;
        }
    }

    // FIXED POINT VERSION ================================================

    void update_fixed_point(float delta_time_ms) {
        elapsed_time += delta_time_ms * 0.1f;            
    }

    // Possibly the fastest.
    // One alternative could be the use of Trigf instead of Trigoi.
    // precalculated sin() and fixed point
    void render_fixed_point(PixelSurface& surface)
    {           
        uint32_t t = (uint32_t)(elapsed_time);

        int t1 = 10 * trigo.sin(t>>1);
        int t2 = 15 * trigo.sin(t>>2);
        int t3 = 5 * trigo.sin(t);
        int t4 = trigo.sin(t<<1);

        for(int j = 0; j < surface.h; j++)
        {
            int p3 = t3;
            int p4 = t4;

            for(int i = 0; i < surface.w; i++)
            {
                int c = trigo.sin(t1) + trigo.sin(t2 + (p4>>1)) +
                    trigo.sin(p3+(p4>>4)) + trigo.sin(p4);

                int col = (128 + (c >> (BITS-7))) & 0x0FF;

                surface.set(i, j, col);

                p3 += (0.004 * IntegerTrigo::PI);
                p4 += (0.007 * IntegerTrigo::PI);
            }

            t1 += (0.007 * IntegerTrigo::PI);
            t2 += ( 0.01 * IntegerTrigo::PI);
        }
    }

private:
    static constexpr int BITS = 12;
    static constexpr int RES  = 1<<BITS;

    float elapsed_time;
    x::IntegerTrigo trigo;
};

}
#endif // X_PLASMAFX_HH
 