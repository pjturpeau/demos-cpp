/*
 * RotozoomFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_ROTOZOOMFX_HH
#define X_ROTOZOOMFX_HH

#include <math.h>
 
#include <x/x.hh>
#include <x/gfx/PixelSurface.hh>


/**
## Projection of the framebuffer rectangle in the texture coordinate system

As the projection is orthogonal (rectangle), we can consider only three (3)
point of the framebuffer rectangle (here, the top left, top right and bottom
left points).

```
A = ( -framebuffer.width / 2 ; -framebuffer.height / 2 )
B = (  framebuffer.width / 2 ; -framebuffer.height / 2 )
C = ( -framebuffer.width / 2 ;  framebuffer.height / 2 )

P =  (P.x , P.y) = initial position in the texture coordinate system
```

The 2D rotation matrix around the Z axis is:

```
            cos(angle)   -sin(angle)
Rz(angle) = sin(angle)    cos(angle)
```

Once zoomed, translated and rotated we have

```
Ap = (scale * (A + P)) * Rz(angle)
Bp = (scale * (B + P)) * Rz(angle)
Cp = (scale * (C + P)) * Rz(angle)
```

Which gives

```
Ap.x =  (scale * (A.x + P.x)) * cos(angle) + (P.y + scale * (A.y + P.y)) * sin(angle)
Ap.y = -(scale * (A.x + P.x)) * sin(angle) + (P.y + scale * (A.y + P.y)) * cos(angle)

Bp.x =  (scale * (B.x + P.x)) * cos(angle) + (P.y + scale * (B.y + P.y)) * sin(angle)
Bp.y = -(scale * (B.x + P.x)) * sin(angle) + (P.y + scale * (B.y + P.y)) * cos(angle)

Cp.x =  (scale * (C.x + P.x)) * cos(angle) + (P.y + scale * (C.y + P.y)) * sin(angle)
Cp.y = -(scale * (C.x + P.x)) * sin(angle) + (P.y + scale * (C.y + P.y)) * cos(angle)
```

## dx & dy computation for the slope along the X axis of the framebuffer

The framebuffer horizontal scanline is going from point _Ap_ to point _Bp_ in
the texture coordinate system.

Thus, to compute the _dx & dy_ for the slope along the X axis of the
framebuffer we need:

```
dX = Bp - Ap = (Bp.x - Ap.x ; Bp.y - Ap.y)

dX.x =   (scale * (B.x + P.x)) * cos(angle) + (P.y + scale * (B.y + P.y)) * sin(angle)
       - (scale * (A.x + P.x)) * cos(angle) - (P.y + scale * (A.y + P.y)) * sin(angle)

dX.y = - (scale * (B.x + P.x)) * sin(angle) + (P.y + scale * (B.y + P.y)) * cos(angle)
       + (scale * (A.x + P.x)) * sin(angle) - (P.y + scale * (A.y + P.y)) * cos(angle)
```

As we have an orthogonal projection _A.y = B.y_, we can simplify:

```
dX.x =  (scale * (B.x + P.x)) * cos(angle) - (scale * (A.x + P.x)) * cos(angle)
dX.y = -(scale * (B.x + P.x)) * sin(angle) + (scale * (A.x + P.x)) * sin(angle)
```

Since _A.x = -B.x_, we can still simplify (and eliminate P.x):

```
dX.x =  2 * scale * B.x * cos(angle)
dX.y = -2 * scale * B.x * sin(angle)
```

As _B.x = framebuffer.width / 2_

```
dX.x =  scale * framebuffer.width * cos(angle)
dX.y = -scale * framebuffer.width * sin(angle)
```

In the end, we can now compute _dx_ and _dy_ according to the horizontal
scanline size:

```
dx_dX = dX.x / framebuffer.width;
dy_dX = dX.y / framebuffer.width;
```

## dx & dy computation for the slope along the Y axis of the framebuffer

After each drawn scanline, we need to go from point _Ap_ to point _Cp_ in the
texture coordinate system.

Thus, to compute the _dx & dy_ for the slope along the Y axis of the
framebuffer we need:

```
dY = Cp - Ap = (Cp.x - Ap.x ; Cp.y - Ap.y)

dY.x =   (scale * (C.x + P.x)) * cos(angle) + (P.y + scale * (C.y + P.y)) * sin(angle)
       - (scale * (A.x + P.x)) * cos(angle) - (P.y + scale * (A.y + P.y)) * sin(angle)

dY.y = - (scale * (C.x + P.x)) * sin(angle) + (P.y + scale * (C.y + P.y)) * cos(angle)
       + (scale * (A.x + P.x)) * sin(angle) - (P.y + scale * (A.y + P.y)) * cos(angle)
```

As we have an orthogonal projection _A.x = C.x_, we can simplify:

```
dY.x = -(P.y + scale * (C.y + P.y)) * sin(angle) - (P.y + scale * (A.y + P.y)) * sin(angle)

dY.y =  (P.y + scale * (C.y + P.y)) * cos(angle) - (P.y + scale * (A.y + P.y)) * cos(angle)
```

Since _A.y = -C.x_, we can still simplify (and eliminate P.y):

```
dY.x = 2 * scale * C.y * sin(angle)
dY.y = 2 * scale * C.y * cos(angle)
```

As _C.y = framebuffer.height / 2_

```
dY.x = scale * framebuffer.height * sin(angle)
dY.y = scale * framebuffer.height * cos(angle)
```

In the end, we can now compute _dx_ and _dy_ according to the number of lines
in the framebuffer:

```
dx_dY = dY.x / framebuffer.height;
dy_dY = dY.y / framebuffer.height;
```
*/
namespace x {

class RotozoomFx
{
public:
    void time_speed(float time_speed);

    /** texture has to be 256x256x8 bits */
    void set_texture(const PixelSurface& texture);

    void default_update(float delta_time_ms);

    bool draw(PixelSurface& surface);

    // x,y position in the texture coordinate system
    int position_x = 0;
    int position_y = 0;

    float angle = 0.F;
    float scale = 1.f;
    float timespeed = 0.001f;

private:
    static constexpr int TEXTURE_SIZE = 256;
    const PixelSurface* texture;
};

///////////////////////////////////////////////////////////////////////////////
#ifdef X_ROTOZOOMFX_IMPLEMENTATION

void RotozoomFx::time_speed(float time_speed) {
    this->timespeed = time_speed;
}

/** texture has to be 256x256x8 bits */
void RotozoomFx::set_texture(const PixelSurface& texture) {
    assert(texture.w == TEXTURE_SIZE);
    assert(texture.h == TEXTURE_SIZE);
    this->texture = &texture;
}

void RotozoomFx::default_update(float delta_time_ms) {
    angle += delta_time_ms * timespeed;

    scale = 4 + 1.5 * cos(angle*0.75) - 0.5 * sin (angle*1.25);
    
    // position of the center of the framebuffer in the texture
    // coordinate system
    position_x = (TEXTURE_SIZE>>1) * cos(angle * 0.75);
    position_y = (TEXTURE_SIZE>>1) * sin(angle * 2);
}

bool RotozoomFx::draw(PixelSurface& surface) {
    int Ax = (1<<16) * scale * (position_x - (surface.w>>1)) * cos(angle)
            + scale * (position_y - (surface.h>>1)) * sin(angle);

    int Ay = (1<<16) * -scale * (position_x - (surface.w>>1)) * sin(angle)
            + scale * (position_y - (surface.h>>1)) * cos(angle);

    int dx_dX = (1<<16) * scale * surface.w * cos(angle) / surface.w;
    int dy_dX = (1<<16) * -scale * surface.w * sin(angle) / surface.w;

    int dx_dY =  (1<<16) * scale * surface.h * sin(angle) / surface.h;
    int dy_dY =  (1<<16) * scale * surface.h * cos(angle) / surface.h;

    int dest_offset = 0;

    for(int j = surface.h; j > 0; j--)
    {
        int x = Ax;
        int y = Ay;

        for(int i = surface.w; i > 0; i--)
        {
            surface.pixels[dest_offset++] =
                texture->pixels[((x>>16)&0xFF) + ((y>>8)&0xFF00)];

            x += dx_dX;
            y += dy_dX;
        }
        Ax += dx_dY;
        Ay += dy_dY;
    }
    return true;
}

#endif // X_ROTOZOOMFX_IMPLEMENTATION
}
#endif // X_ROTOZOOMFX_HH
