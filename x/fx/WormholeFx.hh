/*
 * WormholeFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 01/2019
 * <pierrejean AT turpeau DOT net>
 *
 * Code inspired by: Calogiuri Enzo Antonio [Insolit Dust] 
 */
#ifndef X_WORMHOLEFX_HH
#define X_WORMHOLEFX_HH

#include <x/x.hh>
#include <x/gfx/PixelSurface.hh>
#include <x/gfx/Bitmap.hh>

namespace x {

// width & height of the destination surface
template <int WIDTH, int HEIGHT>
class WormholeFx
{
public:
    WormholeFx(const Bitmap& map, const Bitmap& texture) :
        map(&map), texture(&texture)
    {
        assert(texture->w == 16 && texture->h == 16);
    }

    ~WormholeFx() {
    }

    void update(float delta_time_ms) {
        static float time = 0;

        time += delta_time_ms;

        startu += delta_time_ms * 30 * cos(time/3024);
        startv += TEXTURE_WIDTH * delta_time_ms * 2 * sin(time/4024);
    }

    void draw(PixelSurface& surface) {
        uint8_t * texture_pixels = (uint8_t *)(texture->surface.pixels);
        uint8_t * map_data = (uint8_t *)(map->surface.pixels);

        for(int i = 0; i < WIDTH * HEIGHT; i++) {
            // for 16x16 texture sized only
            surface.pixels[i] = texture_pixels[
                ((startu >> 8) + (startv >> 8)  * texture->surface.w +
                map_data[i]) & 0xFF];

            // for greater texture size, could be replaced by the following
            // code, _wormhole shall be of type uint32_t[] though:
            // surface.pixels[i] = texture_pixels[
            //     (uint32_t(startu) + uint32_t(startv * texture->w) +
            //     _wormhole[i]) % (texture->w * texture->h)];
        }
    }

    void draw_map(PixelSurface& surface) {
        uint8_t * map_data = (uint8_t *)(map->surface.pixels);
        for(int i = 0; i < WIDTH * HEIGHT; i++) {
            surface.pixels[i] = map_data[i];
        }
    }        

    /** generate the wormhole map and save it to a bmp file */
    static void generate_and_save_map(
            int centerx, int centery,
            int spokes, int divs, int stretch,
            int spokes_divisor,
            int divs_divisor,
            bool vertical = true )
    {
        constexpr int texture_size = 16;

        SDL_Log("Gen: %dx%d (%d,%d) spokes=%d,%d divs=%d,%d " \
            "strech=%d %s", WIDTH, HEIGHT, centerx, centery, spokes,
            spokes_divisor, divs, divs_divisor, stretch,
            vertical ? "vertical" : "horizontal");

        uint8_t* map = new uint8_t[WIDTH * HEIGHT];

        assert(map != nullptr);

        for(int j = 1; j < divs + 1; j++)
        {
            float z = -1.0f + (::log(2.0f * j / divs)); // TODO: use optimized log

            float div_calc_x = float(WIDTH  * j / divs);
            float div_calc_y = float(HEIGHT * j / divs);

            ::printf("%d%%\r", j * 100 / divs);

            for(int i = 0; i < spokes; i++)
            {
                float angle = 2.0f * M_PI * i / spokes;
                
                // todo: use precalc sin
                float x = div_calc_x * ::cos(angle);
                float y = div_calc_y * ::sin(angle);
                
                // this creates the downward curve in the center
                if( vertical ) 
                    y -= stretch * z;
                else
                    x -= stretch * z;

                x += centerx;
                y += centery;
                
                if( (x >= 0) && (x < WIDTH) && (y >= 0) && (y < HEIGHT))
                {
                    uint8_t color =
                        uint8_t( ((i/spokes_divisor) % texture_size) + 
                            texture_size * ( (j/divs_divisor) % texture_size));

                    map[int(x) + int(y) * WIDTH] = color;
                }
            }
        }

        char filename[256];

        ::snprintf(filename, 256, "map-%dx%d-%dx%d-%dx%d-%d-%s.bmp",
                WIDTH, HEIGHT, spokes, spokes_divisor, divs, divs_divisor,
                stretch, vertical ? "v" : "h" );

        x::BitmapManager::save_bmp(filename, map, WIDTH, HEIGHT,
            x::ColorPalette::black_to_white());

        SDL_Log("%s built!", filename);
    }

    const x::Bitmap* map;
    const x::Bitmap* texture;
    uint32_t startu, startv;

private:

    // the more you stretch, the more you need spokes to avoid glitches
    static constexpr int STRETCH = 50; 

    static constexpr int SPOKES = 2400;
    static constexpr int DIVS = SPOKES;

    static constexpr int XCENTER = WIDTH/2 - WIDTH/4;
    static constexpr int YCENTER = HEIGHT/2;

    static constexpr int TEXTURE_WIDTH = 16;
};

}
#endif // X_WORMHOLE_HH
