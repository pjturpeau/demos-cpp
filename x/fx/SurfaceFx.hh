/*
 * SurfaceFx.hh
 * Copyleft (c) Pierre-Jean Turpeau 11/2018
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_SURFACEFX_HH
#define X_SURFACEFX_HH

#include <x/x.hh>

namespace x {

/*
 * From Surface to Surface:
 * SinX()
 * SinY()
 * NoiseX()
 * DotShowing() // replace pixels by dots/square/diamonds changing size
 */

class SurfaceFx
{
public:
    void from(const PixelSurface& image);

    // void sin_y() {}
    // void noise_x() {}

    void pixelate(PixelSurface& to_surface, int to_x, int to_y, float factor);

private:        
    const PixelSurface *_from = nullptr;
};
///////////////////////////////////////////////////////////////////////////////
#ifdef X_SURFACEFX_IMPLEMENTATION

void SurfaceFx::from(const PixelSurface& image) {
    _from = &image;
}

void SurfaceFx::pixelate(PixelSurface& to_surface, int to_x, int to_y, float factor) {
    float inverse_factor = 1.0f / factor;
    float pos_adjustment = factor * 0.5f;

    for(int j = 0; j < _from->h; j++) {
        int yy = int(int((j + pos_adjustment) * inverse_factor) * factor) * _from->w;
        for(int i = 0; i < _from->w; i++ ) {
            int xx = ((int)((i + pos_adjustment) * inverse_factor)) * factor;
            to_surface.set(to_x + i, to_y + j, _from->pixels[xx + yy]);
        }
    }
}

#endif // X_SURFACEFX_IMPLEMENTATION
}
#endif // X_SURFACEFX_HH
