# Demos snippets in C++

A personal repository for old-school trash stuffs that I code as a hobby on my
spare time during commuting.

The main idea is to code few stuffs that I didn't have the opportunity to code
back in the late 1990 as an humble assembly coder, in a very simple C++11 subset.

Here, I'm focusing on 8-bits pixels technics and old-school effects & tricks.

See [turpeau.net/babyloon/](http://www.turpeau.net/babyloon/) for past
*productions*.

## The X library part

In `x/` you'll find my little C++ demo framework currently based on SDL2 as the
display backend (mainly backbuffer blitting to the video, windows/fullscreen
switch and sfx).

It's named after the [Mode X](https://en.wikipedia.org/wiki/Mode_X) graphics
display mode of the IBM VGA hardware which brought the 320x240 square pixels
resolution instead of the standard VGA Mode 0x13 (320x200). Apart from the
naming, it doesn't support planar mode.

**CAUTION**: use it at your own risk! As I'm now an occasional coder, I can be
pretty versatile about coding style, naming convention, design decisions,
etc... The maturity level is still low, and big changes can happen at any time,
very suddenly, after weeks or months of inactivity.

## Extra documentations

- [Development environment](docs/environment.md)
- [Coding style](docs/coding-style.md)
- [Scene-graph](docs/scenegraph.md)
- [to do list](docs/todo.md)

## Execution

You can switch between fullscreen and windowing mode through the
`<ALT>-<TAB>` key stroke.  
You can exit thanks to the `<ESC>` key.  
The `<c>` key toggles the display of the color palette.  
The `<p>` key pauses the animation.  
Few CLI options are available to force window/full screen mode, to disable
vsync, or ask for a specific FX feature. Call with `-h` for details.

## Interesting technical references

- [Interpolation Tricks](http://sol.gfxile.net/interpolation/)
- [Timer steps](https://gafferongames.com/post/fix_your_timestep/)
- [FPS measurement technics](http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement)
- [Easing equations](http://robertpenner.com/easing/)

## Interesting minimalist frameworks

- [Immediate 2d](https://github.com/npiegdon/immediate2d)
- [mini framebuffer](https://github.com/emoon/minifb)

`-pj`

## Screenshots

![Raycasting tunnel](images/raycasting-tunnel.png)
![Modplayer](images/modplayer.png)
![Dot Tunnel](images/dot-tunnel.png)
![RotoZoom](images/rotozoom.png)
![Plasma](images/plasma.png)
![UV Tunnel Mapping](images/uv-mapping-1.png)
![UV Warp Mapping](images/uv-mapping-2.png)
![Wormhole](images/wormhole.png)
![Bump 2D](images/bump.png)
![Fire](images/fire.png)
![Moiré](images/moire.png)
